package com.my.commands;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Used to change or add locale parameter in session to string representing locale parameter in request.
 * Redirects to page representing currentPage parameter in request.
 */
public class ChangeLocaleCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        String locale = request.getParameter("locale");
        locale = locale.replace("en", "");
        request.getSession().setAttribute("locale", locale);
        response.sendRedirect(request.getParameter("currentPage"));
    }
}
