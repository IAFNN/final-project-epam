package com.my.commands;

import com.my.DTO.OfferDTO;
import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import com.my.service.OfferService;
import com.my.service.ReportService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
/**
 * Used to get information about some number of offers, which is defined in NUMBER_OF_OFFERS_PER_PAGE
 * constant in code and with offset specified by numberOfCurrentPage parameter in request.
 * Only offers with correct target user type and login are retrieved.
 * Redirects to offers page.
 */
public class OffersDetailsCommand extends FrontCommand{
    private static final int NUMBER_OF_OFFERS_PER_PAGE = 5;
    @Override
    public void process() throws ServletException, IOException {
        List<OfferDTO> offerList = new ArrayList<>();
        Object offset = request.getParameter("numberOfCurrentPage");
        int numberOfOffers;
        if(offset == null){
            numberOfOffers = OfferService.getAll(0, NUMBER_OF_OFFERS_PER_PAGE, offerList);
        }else {
            numberOfOffers = OfferService.getAll(Integer.parseInt(request.getParameter("numberOfCurrentPage")) * NUMBER_OF_OFFERS_PER_PAGE, NUMBER_OF_OFFERS_PER_PAGE, offerList);
        }
        request.getSession().setAttribute("offerList", offerList);
        request.getSession().setAttribute("numberOfOffersPages", Math.ceil(((double)numberOfOffers) / NUMBER_OF_OFFERS_PER_PAGE));
        UserType userType = UserType.valueOf(String.valueOf(request.getSession().getAttribute("userType")).toUpperCase());
        if(userType == UserType.MODERATOR) {
            request.getSession().setAttribute("offerList", offerList.stream().filter(offer -> offer.getTargetUserType() == UserType.MODERATOR).collect(Collectors.toList()));
        }else{
                String speakerLogin = String.valueOf(request.getSession().getAttribute("login"));
                request.getSession().setAttribute("offerList", offerList.stream().filter(offer -> {
                    try {
                        return offer.getTargetUserType() == UserType.SPEAKER && ReportService.get(offer.getReportID()).getSpeakerLogin().equals(speakerLogin);
                    } catch (IncorrectDataException e) {
                        Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
                        request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.unknownError"));
                        try {
                            response.sendRedirect("frontControllerServlet?command=GetStartPage");
                        } catch (IOException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                    return false;
                }).collect(Collectors.toList()));
        }
        servletContext.getRequestDispatcher("/offersDetails.jsp").forward(request, response);
    }
}
