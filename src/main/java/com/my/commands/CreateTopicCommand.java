package com.my.commands;

import com.my.DTO.TopicDTO;
import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.service.TopicService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;
/**
 * Used to create topic with topic name according to topicName parameter in request and event corresponding to eventID parameter in request and save to data source.
 * Redirects to start page.
 */
public class CreateTopicCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        try {
            String topicName = request.getParameter("topicName");
            if(!topicName.matches(".+")){
                Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
                request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.invalidTopicName"));
                response.sendRedirect("frontControllerServlet?command=GetStartPage");
                return;
            }
            String eventIDString = request.getParameter("eventID");
            int eventID = Integer.parseInt(eventIDString.substring(0, eventIDString.indexOf("|") - 1));
            TopicDTO topicDTO = TopicService.save(topicName);
            EventService.setEventToTopic(EventService.get(eventID), topicDTO);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectEventID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
