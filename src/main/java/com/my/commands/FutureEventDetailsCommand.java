package com.my.commands;

import com.my.DTO.EventDTO;
import com.my.service.EventService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Used to get information about some number of future events, which is defined in NUMBER_OF_FUTURE_EVENTS_PER_PAGE
 * constant in code and with offset specified by numberOfCurrentPage parameter in request,
 * sorted according to sortBy parameter in request in order specified in orderFuture attribute in session.
 * Redirects to future events page.
 */
public class FutureEventDetailsCommand extends FrontCommand {
    private static final int NUMBER_OF_FUTURE_EVENTS_PER_PAGE = 5;
    @Override
    public void process() throws ServletException, IOException {
        List<EventDTO> eventList = new ArrayList<>();
        Object offset = request.getParameter("numberOfCurrentPage");
        int numberOfFutureEvents;
        boolean order;
        String sortByParameter = request.getParameter("sortBy");
        String orderParameter = String.valueOf(request.getSession().getAttribute("orderFuture"));
        if(request.getParameter("sortBy") == null){
            sortByParameter = String.valueOf(request.getSession().getAttribute("sortByFuture"));
        }else{
            request.getSession().setAttribute("orderFuture", !orderParameter.matches("true"));
            request.getSession().setAttribute("sortByFuture", request.getParameter("sortBy"));
        }
        if(sortByParameter == null || sortByParameter.matches("null")){
            sortByParameter = null;
        }
        orderParameter = String.valueOf(request.getSession().getAttribute("orderFuture"));
        order = orderParameter.matches("true");
        if(offset == null){
            numberOfFutureEvents = EventService.getAll(0, NUMBER_OF_FUTURE_EVENTS_PER_PAGE, eventList, false,
                    sortByParameter, order);
        }else {
            numberOfFutureEvents = EventService.getAll(Integer.parseInt(request.getParameter("numberOfCurrentPage")) * NUMBER_OF_FUTURE_EVENTS_PER_PAGE, NUMBER_OF_FUTURE_EVENTS_PER_PAGE, eventList,
                    false, sortByParameter, order);
        }
        request.getSession().setAttribute("futureEventList", eventList);
        request.getSession().setAttribute("numberOfFutureEventsPages", Math.ceil(((double)numberOfFutureEvents) / NUMBER_OF_FUTURE_EVENTS_PER_PAGE));
        servletContext.getRequestDispatcher("/eventDetails.jsp").forward(request, response);
    }
}
