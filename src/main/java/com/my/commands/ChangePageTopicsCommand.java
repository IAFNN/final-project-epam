package com.my.commands;

import javax.servlet.ServletException;
import java.io.IOException;
/**
 * Used to change page of topics table on start page according to numberOfCurrentPage request parameter
 */
public class ChangePageTopicsCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        CommandUtils.updateTopicsDataInSession(request.getSession(), Integer.parseInt(request.getParameter("numberOfCurrentPage")), 5);
        response.sendRedirect("start.jsp");
    }
}
