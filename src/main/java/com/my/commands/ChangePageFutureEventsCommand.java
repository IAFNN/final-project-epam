package com.my.commands;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Used to change page of future events table on start page according to numberOfCurrentPage request parameter
 */
public class ChangePageFutureEventsCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        CommandUtils.updateFutureEventsDataInSession(request.getSession(), Integer.parseInt(request.getParameter("numberOfCurrentPage")), 5);
        response.sendRedirect("start.jsp");
    }
}
