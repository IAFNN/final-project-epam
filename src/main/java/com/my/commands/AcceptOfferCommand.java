package com.my.commands;

import com.my.exception.IncorrectDataException;
import com.my.service.OfferService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;

/**
 * Used to accept offer with id according to id request parameter.
 * Redirects to start page.
 */
public class AcceptOfferCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        int offerID = Integer.parseInt(request.getParameter("id"));
        try {
            OfferService.doRequest(offerID);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectOfferID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
