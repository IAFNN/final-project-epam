package com.my.commands;

import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;
/**
 * Used to delete from data source event corresponding to id parameter in request.
 * Redirects to start page.
 */
public class DeleteCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        try {
            EventService.delete(id);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectEventID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
