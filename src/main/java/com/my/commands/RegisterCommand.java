package com.my.commands;

import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import com.my.service.UserService;
import com.my.util.Utils;
import com.my.util.WebUtils;

import javax.servlet.ServletException;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;

/**
 * Used to register new user specified by login, name, password and userType parameters in request.
 * Saves information about user type and login of new user to attributes in session.
 * Redirects to start page.
 */
public class RegisterCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        try {
            String login = request.getParameter("login");
            String name = request.getParameter("name");
            String password = request.getParameter("password");
            if(!(name.matches(".{6,45}") && login.matches(".{6,45}") && password.matches("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{1,45})$"))){
                throw new IllegalArgumentException();
            }
            UserType userType = UserType.valueOf(request.getParameter("userType").toUpperCase());
            try {
                String salt = Utils.generateSalt();
                password = Utils.hashPassword(password, salt);
                UserService.save(login, name, password, userType, salt);
                request.getSession().setAttribute("userType", userType);
                request.getSession().setAttribute("login", login);
                CommandUtils.initializeDataOnStartPage(request.getSession());
                response.sendRedirect("frontControllerServlet?command=GetStartPage");
            } catch (IncorrectDataException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
                request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.userWithThisLoginAlreadyExists"));
                response.sendRedirect("register.jsp");
            }
        }catch (IllegalArgumentException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.invalidRegisterData"));
            response.sendRedirect("register.jsp");
        }
    }
}
