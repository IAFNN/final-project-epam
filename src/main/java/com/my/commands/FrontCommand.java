package com.my.commands;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Abstract class to extend for all Commands
 */
public abstract class FrontCommand {
    protected ServletContext servletContext;
    protected HttpServletRequest request;
    protected HttpServletResponse response;

    /**
     * Used to initialize data about request that will be processed
     * @param servletContext ServletContext of current application
     * @param request HttpServletRequest to be processed
     * @param response HttpServletResponse that will be sent after processing request
     */
    public void init(ServletContext servletContext, HttpServletRequest request, HttpServletResponse response) {
        this.servletContext = servletContext;
        this.request = request;
        this.response = response;
    }

    /**
     * Method that must be overridden in Command to process request
     */
    public abstract void process() throws ServletException, IOException;
}
