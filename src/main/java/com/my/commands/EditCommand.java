package com.my.commands;

import com.my.DTO.AbstractUserDTO;
import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.util.MailUtil;
import com.my.util.Utils;
import com.my.util.WebUtils;
import jakarta.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;
/**
 * Used to edit information in data source about event corresponding to id parameter in request.
 * Name, time and location can be changed by passing them as request parameters, if nothing is passed information won't be changed.
 * Sends notification via email about change in data to all users and speakers registered in this event.
 * Redirects to start page.
 */
public class EditCommand extends FrontCommand {
    private static final Logger logger = LoggerFactory.getLogger(EditCommand.class);

    @Override
    public void process() throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String time = request.getParameter("time");
        String location = request.getParameter("location");
        if(name.matches(".+") && time.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}") && location.matches(".+")){
            time += ":00";
            try {
                String finalTime = time;
                new Thread(() -> {
                    try {
                        EventDTO event = EventService.get(id);
                        for (AbstractUserDTO user : EventService.getUsersByEvent(event)) {
                            MailUtil.sendEmail(user.getLogin(), event.getName(), Utils.buildMessageText(String.valueOf(id), name, finalTime, location));
                        }
                        for (ReportDTO report : EventService.getReportsByEvent(event)){
                            MailUtil.sendEmail(report.getSpeakerLogin(), event.getName(), Utils.buildMessageText(String.valueOf(id), name, finalTime, location));
                        }
                        logger.info("Emails sent");
                    } catch (IncorrectDataException | MessagingException e) {
                        e.printStackTrace();
                    }
                }).start();
                EventService.update(id, name, time, location);
                response.sendRedirect("frontControllerServlet?command=GetStartPage");
            }catch (IncorrectDataException e){
                Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
                request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectEventID"));
                response.sendRedirect("frontControllerServlet?command=GetStartPage");
            }
        }else{
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.invalidEventsInput"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
