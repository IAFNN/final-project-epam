package com.my.commands;


import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import com.my.service.OfferService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;
/**
 * Used to create offer for creating report.
 * Offer is specified by topicID and eventID request parameters which represent topic and event for new report.
 * As speaker login current login of user, which is specified in session attribute, is used.
 * Redirects to start page.
 */
public class OfferCreateReportCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        String topicIDString = request.getParameter("topicID");
        String eventIDString = request.getParameter("eventID");
        int topicID = Integer.parseInt(topicIDString.substring(0, topicIDString.indexOf("|") - 1));
        int eventID = Integer.parseInt(eventIDString.substring(0, eventIDString.indexOf("|") - 1));
        String login = String.valueOf(request.getSession().getAttribute("login"));
        try {
            OfferService.save(UserType.MODERATOR, "Create", topicID, login, eventID, -1);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectReportIDOrTopicID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
