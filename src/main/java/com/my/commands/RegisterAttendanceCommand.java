package com.my.commands;

import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.service.UserService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;

/**
 * Used to register attendance of user specified by login parameter in request on event corresponding to eventID parameter
 * in request.
 * Redirects to start page.
 */
public class RegisterAttendanceCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        try {
            EventService.registerAttendance(UserService.get(request.getParameter("login")),
                    EventService.get(Integer.parseInt(request.getParameter("eventID"))));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectUserLoginOrEventID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
