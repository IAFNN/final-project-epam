package com.my.commands;

import com.my.DTO.AbstractUserDTO;

import javax.servlet.ServletException;

import com.my.exception.IncorrectDataException;
import com.my.service.UserService;
import com.my.util.Utils;
import com.my.util.WebUtils;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;

/**
 * Used to check if login and password in request parameter match to any login and password in data source.
 * If authorization is successful adds to session attributes representing user type, login and need for auto authorization
 * in next attempts of accessing application.
 * Redirects to start page if authorization was successful, otherwise redirects to login page.
 */
public class LoginCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        try {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            String userPassword = UserService.getPassword(login);
            String salt = UserService.getSalt(login);
            if (userPassword != null && userPassword.equals(Utils.hashPassword(password, salt))) {
                AbstractUserDTO userDTO = UserService.get(login);
                request.getSession().setAttribute("userType", userDTO.getUserType());
                request.getSession().setAttribute("login", login);
                if (request.getParameter("rememberMe") != null) {
                    request.getSession().setAttribute("autoAuthorize", true);
                }
                response.sendRedirect("frontControllerServlet?command=GetStartPage");
            } else {
                Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
                request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectLoginOrPassword"));
                response.sendRedirect("index.jsp");
            }
        }catch (IncorrectDataException | InvalidKeySpecException | NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }
}
