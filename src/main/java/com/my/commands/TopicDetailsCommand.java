package com.my.commands;

import com.my.DTO.EventDTO;
import com.my.DTO.TopicDTO;
import com.my.service.EventService;
import com.my.service.TopicService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Used to get information about some number of topics, which is defined in NUMBER_OF_TOPICS_PER_PAGE
 * constant in code and with offset specified by numberOfCurrentPage parameter in request.
 * Redirects to topics page.
 */
public class TopicDetailsCommand extends FrontCommand{
    private static final int NUMBER_OF_TOPICS_PER_PAGE = 5;
    @Override
    public void process() throws ServletException, IOException {
        List<TopicDTO> topicList = new ArrayList<>();
        List<EventDTO> eventList = EventService.getAll();
        Object offset = request.getParameter("numberOfCurrentPage");
        int numberOfTopics;
        if(offset == null){
            numberOfTopics = TopicService.getAll(0, NUMBER_OF_TOPICS_PER_PAGE, topicList);
        }else {
            numberOfTopics = TopicService.getAll(Integer.parseInt(request.getParameter("numberOfCurrentPage")) * NUMBER_OF_TOPICS_PER_PAGE, NUMBER_OF_TOPICS_PER_PAGE, topicList);
        }
        request.getSession().setAttribute("eventList", eventList);
        request.getSession().setAttribute("topicList", topicList);
        request.getSession().setAttribute("numberOfTopicPages", Math.ceil(((double)numberOfTopics) / NUMBER_OF_TOPICS_PER_PAGE));
        servletContext.getRequestDispatcher("/topicsDetails.jsp").forward(request, response);
    }
}
