package com.my.commands;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Used to delete information about user type, login and auto authorization in session attributes.
 * Redirects to login page.
 */
public class SignOutCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        request.getSession().removeAttribute("userType");
        request.getSession().removeAttribute("login");
        request.getSession().removeAttribute("autoAuthorize");
        response.sendRedirect("index.jsp");
    }
}
