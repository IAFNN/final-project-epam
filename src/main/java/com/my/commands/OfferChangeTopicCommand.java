package com.my.commands;

import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import com.my.service.OfferService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;

/**
 * Used to create offer for changing topic in report corresponding to id parameter in request.
 * Offer is specified by topicID request parameter which represents new topic for report.
 * Redirects to start page.
 */
public class OfferChangeTopicCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        int reportID = Integer.parseInt(request.getParameter("id"));
        String topicIDString = request.getParameter("topicID");
        int topicID = Integer.parseInt(topicIDString.substring(0, topicIDString.indexOf("|") - 1));
        try {
            OfferService.save(UserType.SPEAKER, "Edit", topicID, null, -1, reportID);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectTopicIDOrEventID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
