package com.my.commands;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Used to update data in session which is displayed on start page.
 * Redirects to start page.
 */
public class GetStartPageCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        CommandUtils.initializeDataOnStartPage(request.getSession());
        servletContext.getRequestDispatcher("/start.jsp").forward(request, response);
    }

}
