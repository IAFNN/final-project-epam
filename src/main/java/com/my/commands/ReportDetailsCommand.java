package com.my.commands;

import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.DTO.TopicDTO;
import com.my.entity.Topic;
import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.service.ReportService;
import com.my.service.TopicService;
import com.my.service.UserService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Used to get information about some number of reports, which is defined in NUMBER_OF_REPORTS_PER_PAGE
 * constant in code and with offset specified by numberOfCurrentPage parameter in request.
 * Redirects to reports page.
 */
public class ReportDetailsCommand extends FrontCommand {
    private static final int NUMBER_OF_REPORTS_PER_PAGE = 5;
    @Override
    public void process() throws ServletException, IOException {
        List<ReportDTO> reportList = new ArrayList<>();
        List<ReportDTO> myReportList = new ArrayList<>();
        List<TopicDTO> topicList = TopicService.getAll();
        List<EventDTO> eventList = EventService.getAll();
        Object offset = request.getParameter("numberOfCurrentPage");
        int numberOfReports;
        int numberOfMyReports = 0;
        if(offset == null){
            numberOfReports = ReportService.getAll(0, NUMBER_OF_REPORTS_PER_PAGE, reportList);
            try {
                numberOfMyReports = ReportService.getMyAll(0, NUMBER_OF_REPORTS_PER_PAGE, myReportList, UserService.get(
                        String.valueOf(request.getSession().getAttribute("login"))
                ));
            } catch (IncorrectDataException e) {
                throw new RuntimeException(e);
            }
        }else {
            numberOfReports = ReportService.getAll(Integer.parseInt(request.getParameter("numberOfCurrentPage")) * NUMBER_OF_REPORTS_PER_PAGE, NUMBER_OF_REPORTS_PER_PAGE, reportList);
            try {
                numberOfMyReports = ReportService.getMyAll(Integer.parseInt(request.getParameter("numberOfCurrentPage")) * NUMBER_OF_REPORTS_PER_PAGE, NUMBER_OF_REPORTS_PER_PAGE,
                        myReportList, UserService.get(String.valueOf(request.getSession().getAttribute("login"))));
            } catch (IncorrectDataException e) {
                throw new RuntimeException(e);
            }
        }
        request.getSession().setAttribute("reportList", reportList);
        request.getSession().setAttribute("myReportList", myReportList);
        request.getSession().setAttribute("topicList", topicList);
        request.getSession().setAttribute("eventList", eventList);
        request.getSession().setAttribute("numberOfReportsPages", Math.ceil(((double)numberOfReports) / NUMBER_OF_REPORTS_PER_PAGE));
        request.getSession().setAttribute("numberOfMyReportsPages", Math.ceil(((double)numberOfMyReports) / NUMBER_OF_REPORTS_PER_PAGE));
        servletContext.getRequestDispatcher("/reportsDetails.jsp").forward(request, response);
    }
}
