package com.my.commands;

import com.my.service.EventService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;

/**
 * Used to create event according to name, time and location parameters in request and save to data source.
 * Redirects to start page.
 */
public class CreateCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        String name = request.getParameter("name");
        String time = request.getParameter("time");
        String location = request.getParameter("location");
        if(name.matches(".+") && time.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}") && location.matches(".+")) {
            EventService.save(name, time, location);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }else{
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.invalidEventsInput"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
