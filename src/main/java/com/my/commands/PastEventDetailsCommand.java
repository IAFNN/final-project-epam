package com.my.commands;

import com.my.DTO.EventDTO;
import com.my.service.EventService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Used to get information about some number of past events, which is defined in NUMBER_OF_PAST_EVENTS_PER_PAGE
 * constant in code, sorted according to sortBy parameter in request in order specified in order attribute in session.
 * Redirects to future events page.
 */
public class PastEventDetailsCommand extends FrontCommand {
    private static final int NUMBER_OF_PAST_EVENTS_PER_PAGE = 5;
    @Override
    public void process() throws ServletException, IOException {
        List<EventDTO> eventList = new ArrayList<>();
        Object offset = request.getParameter("numberOfCurrentPage");
        int numberOfPastEvents;
        boolean order;
        String sortByParameter = request.getParameter("sortBy");
        String orderParameter = String.valueOf(request.getSession().getAttribute("order"));
        if(sortByParameter == null || sortByParameter.isEmpty()){
            sortByParameter = String.valueOf(request.getSession().getAttribute("sortBy"));
        }else{
            request.getSession().setAttribute("order", !orderParameter.matches("true"));
            request.getSession().setAttribute("sortBy", sortByParameter);
        }
        if(sortByParameter == null || sortByParameter.matches("null")){
            sortByParameter = null;
        }
        orderParameter = String.valueOf(request.getSession().getAttribute("order"));
        order = orderParameter.matches("true");
        if(offset == null){
            numberOfPastEvents = EventService.getAll(0, NUMBER_OF_PAST_EVENTS_PER_PAGE, eventList,
                    true, sortByParameter, order);
        }else {
            numberOfPastEvents = EventService.getAll(Integer.parseInt(request.getParameter("numberOfCurrentPage")) * NUMBER_OF_PAST_EVENTS_PER_PAGE, NUMBER_OF_PAST_EVENTS_PER_PAGE, eventList,
                    true, sortByParameter, order);
        }
        request.getSession().setAttribute("pastEventList", eventList);
        request.getSession().setAttribute("numberOfPastEventsPages", Math.ceil(((double)numberOfPastEvents) / NUMBER_OF_PAST_EVENTS_PER_PAGE));
        servletContext.getRequestDispatcher("/pastEventsDetails.jsp").forward(request, response);
    }
}
