package com.my.commands;

import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.service.ReportService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;

/**
 * Used to create report with topic and event corresponding to topicID and eventID parameters in request and save to data source.
 * Redirects to start page.
 */
public class CreateReportCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        String topicIDString = request.getParameter("topicID");
        String eventIDString = request.getParameter("eventID");
        int topicID = Integer.parseInt(topicIDString.substring(0, topicIDString.indexOf("|") - 1));
        int eventID = Integer.parseInt(eventIDString.substring(0, eventIDString.indexOf("|") - 1));
        try {
            int reportID = ReportService.save(null, topicID);
            EventService.setReportToEvent(ReportService.get(reportID), EventService.get(eventID));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectReportIDOrTopicID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
