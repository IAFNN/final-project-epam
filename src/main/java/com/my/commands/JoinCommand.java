package com.my.commands;

import com.my.DTO.AbstractUserDTO;
import com.my.DTO.EventDTO;
import com.my.exception.IncorrectDataException;
import com.my.service.EventService;
import com.my.service.UserService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;

/**
 * Used to register current user to event specified by id parameter in request.
 * Checks if current user is already registered for this event.
 * Redirects to start page.
 */
public class JoinCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        try {
            EventDTO event = EventService.get(Integer.parseInt(request.getParameter("id")));
            AbstractUserDTO user = UserService.get(String.valueOf(request.getSession().getAttribute("login")));
            if(EventService.getUsersByEvent(event).contains(user)){
                Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
                request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.alreadyRegistered"));
                response.sendRedirect("frontControllerServlet?command=GetStartPage");
                return;
            }
            EventService.setUserToEvent(user, event);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectEventID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}