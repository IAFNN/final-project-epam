package com.my.commands;

import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import com.my.service.OfferService;
import com.my.util.WebUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Properties;
/**
 * Used to create offer for joining as speaker report, which corresponds to id parameter in request.
 * Offer is specified by current user's login, which is stored in session attribute.
 * Redirects to start page.
 */
public class OfferJoinReportCommand extends FrontCommand{
    @Override
    public void process() throws ServletException, IOException {
        int reportID = Integer.parseInt(request.getParameter("id"));
        String login = String.valueOf(request.getSession().getAttribute("login"));
        try {
            OfferService.save(UserType.MODERATOR, "Edit", -1, login, -1, reportID);
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }catch (IncorrectDataException e){
            Properties localeFile = WebUtils.loadProperties(String.valueOf(request.getSession().getAttribute("locale")));
            request.getSession().setAttribute("errorMessage", localeFile.getProperty("label.incorrectReportID"));
            response.sendRedirect("frontControllerServlet?command=GetStartPage");
        }
    }
}
