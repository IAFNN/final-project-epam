package com.my.commands;

import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.DTO.TopicDTO;
import com.my.service.EventService;
import com.my.service.ReportService;
import com.my.service.TopicService;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Util class for getting data from data source
 */
public class CommandUtils {
    private CommandUtils() {
    }

    /**
     * Used to initialize data about past events, future events, reports and topics
     * @param session HttpSession where lists of different DTOs and number of pages needed to display it will be saved
     */
    public static void initializeDataOnStartPage(HttpSession session) {
        List<EventDTO> pastEvents = new ArrayList<>();
        List<EventDTO> futureEvents = new ArrayList<>();
        List<ReportDTO> reportList = new ArrayList<>();
        List<TopicDTO> topicList = new ArrayList<>();
        int numberOfPastEvents = EventService.getAll(0, 5, pastEvents, true);
        int numberOfFutureEvents = EventService.getAll(0, 5, futureEvents, false);
        int numberOfReport = ReportService.getAll(0, 5, reportList);
        int numberOfTopic = TopicService.getAll(0, 5, topicList);
        session.setAttribute("pastEventList", pastEvents);
        session.setAttribute("numberOfPastEventsPages", Math.ceil(((double)numberOfPastEvents) / 5));
        session.setAttribute("futureEventList", futureEvents);
        session.setAttribute("numberOfFutureEventsPages", Math.ceil(((double)numberOfFutureEvents) / 5));
        session.setAttribute("reportList", reportList);
        session.setAttribute("numberOfReportsPages", Math.ceil(((double)numberOfReport) / 5));
        session.setAttribute("topicList", topicList);
        session.setAttribute("numberOfTopicPages", Math.ceil(((double)numberOfTopic) / 5));
    }

    /**
     * Used to update data about past events in session according to number of page and limit of records per page
     * @param session HttpSession where list of DTOs and number of pages needed to display it will be saved
     * @param pastEventsPage number of page that should be displayed
     * @param pastEventsLimit number of records per page
     */
    public static void updatePastEventsDataInSession(HttpSession session, int pastEventsPage, int pastEventsLimit) {
        List<EventDTO> pastEvents = new ArrayList<>();
        int numberOfPastEvents = EventService.getAll(pastEventsPage * pastEventsLimit, pastEventsLimit, pastEvents, true);
        session.setAttribute("pastEventList", pastEvents);
        session.setAttribute("numberOfPastEventsPages", Math.ceil(((double)numberOfPastEvents) / pastEventsLimit));
    }

    /**
     * Used to update data about future events in session according to number of page and limit of records per page
     * @param session HttpSession where list of DTOs and number of pages needed to display it will be saved
     * @param futureEventsPage number of page that should be displayed
     * @param futureEventsLimit number of records per page
     */
    public static void updateFutureEventsDataInSession(HttpSession session, int futureEventsPage, int futureEventsLimit) {
        List<EventDTO> futureEvents = new ArrayList<>();
        int numberOfFutureEvents = EventService.getAll(futureEventsPage * futureEventsLimit, futureEventsLimit, futureEvents, false);
        session.setAttribute("futureEventList", futureEvents);
        session.setAttribute("numberOfFutureEventsPages", Math.ceil(((double)numberOfFutureEvents) / futureEventsLimit));
    }

    /**
     * Used to update data about reports in session according to number of page and limit of records per page
     * @param session HttpSession where list of DTOs and number of pages needed to display it will be saved
     * @param reportsPage number of page that should be displayed
     * @param reportLimit number of records per page
     */
    public static void updateReportsDataInSession(HttpSession session, int reportsPage, int reportLimit) {
        List<ReportDTO> reports = new ArrayList<>();
        int numberOfReports = ReportService.getAll(reportsPage * reportLimit, reportLimit, reports);
        session.setAttribute("reportList", reports);
        session.setAttribute("numberOfReportPages", Math.ceil(((double)numberOfReports) / reportLimit));
    }

    /**
     * Used to update data about topics in session according to number of page and limit of records per page
     * @param session HttpSession where list of DTOs and number of pages needed to display it will be saved
     * @param topicsPage number of page that should be displayed
     * @param topicLimit number of records per page
     */
    public static void updateTopicsDataInSession(HttpSession session, int topicsPage, int topicLimit) {
        List<TopicDTO> topics = new ArrayList<>();
        int numberOfTopics = TopicService.getAll(topicsPage * topicLimit, topicLimit, topics);
        session.setAttribute("topicList", topics);
        session.setAttribute("numberOfTopicPages", Math.ceil(((double)numberOfTopics) / topicLimit));
    }
}
