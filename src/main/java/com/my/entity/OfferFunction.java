package com.my.entity;

/**
 * Interface representing action that is proposed in offer.
 * Some default methods should be overridden according to offer type.
 */
public interface OfferFunction {
    void doRequest();
    default int getTopicID(){
        return -1;
    }
    default String getSpeakerLogin(){
        return null;
    }
    default int getReportID(){
        return -1;
    }
    default int getEventID(){
        return -1;
    }
}
