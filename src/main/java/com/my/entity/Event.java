package com.my.entity;

import java.time.Instant;
/**
 * Entity representing record from events table
 */
public class Event {
    private static int eventCount = 0;
    private String name;
    private Instant date;
    private String location;
    private int id;
    {
        id = eventCount;
        eventCount++;
    }
    public Event(String name, Instant date, String location) {
        this.name = name;
        this.date = date;
        this.location = location;
    }
    public Event(int id, String name, Instant date, String location){
        this.id = id;
        this.name = name;
        this.date = date;
        this.location = location;
    }
    public String getName() {
        return name;
    }

    public Instant getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Event){
            return ((Event) obj).id == id;
        }else{
            return false;
        }
    }
}
