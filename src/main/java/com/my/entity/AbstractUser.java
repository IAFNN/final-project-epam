package com.my.entity;

import java.util.Objects;

/**
 * Entity representing record from abstractusers table
 */
public class AbstractUser {
    private String name;
    private String password;
    private final String login;
    private UserType userType;
    private String salt;
    public AbstractUser(String name, String login, String password, UserType userType, String salt){
        this.name = name;
        this.password = password;
        this.login = login;
        this.userType = userType;
        this.salt = salt;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AbstractUser){
            return Objects.equals(((AbstractUser) obj).login, login) && Objects.equals(((AbstractUser) obj).name, name) && Objects.equals(((AbstractUser) obj).password, password) && ((AbstractUser) obj).userType == userType;
        }else{
            return false;
        }
    }

    public String getSalt() {
        return salt;
    }
}
