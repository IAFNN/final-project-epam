package com.my.entity;
/**
 * Entity representing record from offers table
 */
public class Offer {
    private static int offerCount = 0;
    private UserType targetUserType;
    private OfferFunction offerFunction;
    private String offerType;
    private int id;
    {
        id = offerCount;
        offerCount++;
    }
    public Offer(UserType targetUserType, String offerType, OfferFunction offerFunction) {
        this.targetUserType = targetUserType;
        this.offerFunction = offerFunction;
        this.offerType = offerType;
    }
    public Offer(int id, UserType targetUserType, String offerType, OfferFunction offerFunction){
        this.targetUserType = targetUserType;
        this.offerFunction = offerFunction;
        this.id = id;
        this.offerType = offerType;
    }
    public UserType getTargetUserType() {
        return targetUserType;
    }

    public OfferFunction getOfferFunction() {
        return offerFunction;
    }

    public int getId() {
        return id;
    }

    public void setTargetUserType(UserType targetUserType) {
        this.targetUserType = targetUserType;
    }

    public void setOfferFunction(OfferFunction offerFunction) {
        this.offerFunction = offerFunction;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }
}
