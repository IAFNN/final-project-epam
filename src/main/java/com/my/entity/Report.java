package com.my.entity;
/**
 * Entity representing record from report table
 */
public class Report {
    private static int reportCount = 0;
    private int id;
    {
        id = reportCount;
        reportCount++;
    }
    private Topic topic;

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public AbstractUser getSpeaker() {
        return speaker;
    }

    public void setSpeaker(AbstractUser speaker) {
        this.speaker = speaker;
    }

    private AbstractUser speaker;

    public Report(Topic topic, AbstractUser speaker) {
        this.topic = topic;
        this.speaker = speaker;
    }
    public Report(int id, Topic topic, AbstractUser speaker){
        this.id = id;
        this.topic = topic;
        this.speaker = speaker;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Report){
            return ((Report) obj).topic.equals(topic) && ((Report) obj).speaker.equals(speaker);
        }else{
            return false;
        }
    }
}
