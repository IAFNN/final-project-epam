package com.my.entity;

/**
 * Enum represents user type of abstract user
 */
public enum UserType {
    USER, SPEAKER, MODERATOR;

    /**
     * Used to convert value of enum instance on which this method was called to database-styled string
     * @return database styled string
     */
    public String convertToDBEnum(){
        switch (this){
            case USER:
                return "User";
            case SPEAKER:
                return "Speaker";
            case MODERATOR:
                return "Moderator";
        }
        return null;
    }
}
