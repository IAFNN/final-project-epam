package com.my.entity;
/**
 * Entity representing record from topics table
 */
public class Topic {
    private static int topicCount = 0;
    {
        id = topicCount;
        topicCount++;
    }
    private int id;
    private String name;

    public Topic(String name) {
        this.name = name;
    }
    public Topic(int id, String name){
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Topic){
            return ((Topic) obj).id == id && ((Topic) obj).name.equals(name);
        }else{
            return false;
        }
    }
}
