package com.my.DTO;
/**
 * DTO class representing Report entity in data source
 */
public class ReportDTO {
    private int id;
    private int topicID;
    private String topicName;
    private String speakerLogin;
    private String speakerName;
    private int eventID;
    private String eventName;

    public ReportDTO(int id, int topicID, String topicName, String speakerLogin, String speakerName, int eventId, String eventName){
        this.id = id;
        this.topicID = topicID;
        this.topicName = topicName;
        this.speakerLogin = speakerLogin;
        this.speakerName = speakerName;
        this.eventID = eventId;
        this.eventName = eventName;

    }

    public int getId() {
        return id;
    }

    public int getTopicID() {
        return topicID;
    }

    public void setTopicID(int topicID) {
        this.topicID = topicID;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getSpeakerLogin() {
        return speakerLogin;
    }

    public void setSpeakerLogin(String speakerLogin) {
        this.speakerLogin = speakerLogin;
    }

    public String getSpeakerName() {
        return speakerName;
    }

    public void setSpeakerName(String speakerName) {
        this.speakerName = speakerName;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ReportDTO){
            return ((ReportDTO) obj).id == id && ((ReportDTO) obj).eventID == eventID && ((ReportDTO) obj).topicID == topicID && ((ReportDTO) obj).speakerLogin.equals(speakerLogin);
        }else{
            return false;
        }
    }
}
