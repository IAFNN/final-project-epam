package com.my.DTO;
/**
 * DTO class representing Topic entity in data source
 */
public class TopicDTO {
    private int id;
    private String name;
    private int eventID;
    private String eventName;
    public TopicDTO(int id, String name, int eventID, String eventName){
        this.id = id;
        this.name = name;
        this.eventID = eventID;
        this.eventName = eventName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof TopicDTO){
            return ((TopicDTO) obj).id == id && ((TopicDTO) obj).eventID == eventID;
        }else{
            return false;
        }
    }
}
