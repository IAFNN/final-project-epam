package com.my.DTO;

import com.my.entity.UserType;
/**
 * DTO class representing Offer entity in data source
 */
public class OfferDTO {
    private int topicID;
    private int reportID;
    private int eventID;
    private String speakerLogin;
    private String offerType;
    private int id;
    private UserType targetUserType;
    public OfferDTO(int id, String offerType, int topicID, int reportID, int eventID, String speakerLogin, UserType targetUserType){
        this.id = id;
        this.offerType = offerType;
        this.topicID = topicID;
        this.reportID = reportID;
        this.eventID = eventID;
        this.speakerLogin = speakerLogin;
        this.targetUserType = targetUserType;
    }

    public int getId() {
        return id;
    }
    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public int getTopicID() {
        return topicID;
    }

    public void setTopicID(int topicID) {
        this.topicID = topicID;
    }

    public int getReportID() {
        return reportID;
    }

    public void setReportID(int reportID) {
        this.reportID = reportID;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getSpeakerLogin() {
        return speakerLogin;
    }

    public void setSpeakerLogin(String speakerLogin) {
        this.speakerLogin = speakerLogin;
    }

    public UserType getTargetUserType() {
        return targetUserType;
    }

    public void setTargetUserType(UserType targetUserType) {
        this.targetUserType = targetUserType;
    }
}
