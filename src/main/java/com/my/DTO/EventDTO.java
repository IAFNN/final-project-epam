package com.my.DTO;

import java.util.List;
/**
 * DTO class representing Event entity in data source
 */
public class EventDTO {
    private String name;
    private String date;
    private String location;
    private int id;
    private List<AbstractUserDTO> usersRegistered;
    private List<AbstractUserDTO> usersAttended;
    private List<ReportDTO> reports;
    public EventDTO(int id, String name, String date, String location, List<AbstractUserDTO> usersRegistered, List<AbstractUserDTO> usersAttended, List<ReportDTO> reports){
        this.id = id;
        this.name = name;
        this.date = date;
        this.location = location;
        this.usersRegistered = usersRegistered;
        this.usersAttended = usersAttended;
        this.reports = reports;
    }
    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<AbstractUserDTO> getUsersRegistered() {
        return usersRegistered;
    }

    public void setUsersRegistered(List<AbstractUserDTO> usersRegistered) {
        this.usersRegistered = usersRegistered;
    }

    public List<AbstractUserDTO> getUsersAttended() {
        return usersAttended;
    }

    public void setUsersAttended(List<AbstractUserDTO> usersAttended) {
        this.usersAttended = usersAttended;
    }

    public List<ReportDTO> getReports() {
        return reports;
    }

    public void setReports(List<ReportDTO> reports) {
        this.reports = reports;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof EventDTO){
            return ((EventDTO) obj).id == id;
        }else {
            return false;
        }
    }
}
