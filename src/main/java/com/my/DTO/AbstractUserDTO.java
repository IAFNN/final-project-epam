package com.my.DTO;

import com.my.entity.UserType;

/**
 * DTO class representing AbstractUser entity in data source
 */
public class AbstractUserDTO {
    private String name;
    private final String login;
    private UserType userType;
    public AbstractUserDTO(String name, String login, UserType userType){
        this.name = name;
        this.userType = userType;
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }
    public void setName(String name) {
        this.name = name;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AbstractUserDTO){
            return ((AbstractUserDTO) obj).login.equals(login) && ((AbstractUserDTO) obj).name.equals(name) && ((AbstractUserDTO) obj).userType == userType;
        }else {
            return false;
        }
    }
}
