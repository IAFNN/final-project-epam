package com.my.DAO.impl;

import com.my.DAO.DatabaseDAO;
import com.my.DAO.HikariCPDataSource;
import com.my.DAO.ReportDAO;
import com.my.DAO.TopicDAO;
import com.my.entity.AbstractUser;
import com.my.entity.Report;
import com.my.entity.UserType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReportDAOImpl implements DatabaseDAO<Report, Integer>, com.my.DAO.ReportDAO {
    private static final ReportDAO instance = new ReportDAOImpl();

    private ReportDAOImpl() {
    }
    /**
     * Used for getting instance of this class
     * @return instance of ReportDAO implementation
     */
    public static ReportDAO getInstance() {
        return instance;
    }

    @Override
    public Optional<Report> get(Integer primaryKey) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "report");
            query = query.replaceFirst("\\?", String.format("ID=%d", primaryKey));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Report result = extractFromResultSet(resultSet);
                connection.close();
                return Optional.of(result);
            } else {
                connection.close();
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public List<Report> getAll() {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectAllFromTable();
            query = query.replaceFirst("\\?", "report");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<Report> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void save(Report report) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(3);
            query = query.replaceFirst("\\?", "report");
            query = query.replaceFirst("\\?", "ID");
            query = query.replaceFirst("\\?", "TopicID");
            query = query.replaceFirst("\\?", "SpeakerLogin");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, report.getId());
            statement.setInt(2, report.getTopic().getId());
            if(report.getSpeaker() != null) {
                statement.setString(3, report.getSpeaker().getLogin());
            }else{
                statement.setNull(3, Types.VARCHAR);
            }
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Report report, String[] params){
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.updateTable(2, 1);
            query = query.replaceFirst("\\.", "report");
            query = query.replaceFirst("\\.", "SpeakerLogin");
            query = query.replaceFirst("\\.", "TopicID");
            query = query.replaceFirst("\\.", String.format("ID=%d", report.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, params[0]);
            statement.setString(2, params[1]);
            statement.executeUpdate();
            TopicDAO topicDAO = TopicDAOImpl.getInstance();
            report.setTopic(topicDAO.get(Integer.parseInt(params[1])).orElse(null));
            AbstractUserDAOImpl abstractUserDAOImpl = AbstractUserDAOImpl.getInstance();
            report.setSpeaker(abstractUserDAOImpl.get(params[0]).orElse(null));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void delete(Report report) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.deleteFromTable();
            query = query.replaceFirst("\\?", "report");
            query = query.replaceFirst("\\?", String.format("ID=%d", report.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Report extractFromResultSet(ResultSet resultSet) throws SQLException {
        TopicDAO topicDAO = TopicDAOImpl.getInstance();
        AbstractUserDAOImpl userDAO = AbstractUserDAOImpl.getInstance();
        AbstractUser speaker = userDAO.get(resultSet.getString(3)).orElse(null);
        if (speaker == null || speaker.getUserType() != UserType.SPEAKER) {
            speaker = null;
        }
        Report report = new Report(resultSet.getInt(1), topicDAO.get(Integer.parseInt(resultSet.getString(2))).orElse(null),
                speaker);
        return report;
    }

    @Override
    public int getAll(int offset, int limit, List<Report> result) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                String query = SQLQueries.selectAllFromTableWithLimit();
                query = query.replaceFirst("\\?", "report");
                statement = connection.prepareStatement(query);
                statement.setInt(1, offset);
                statement.setInt(2, limit);
            }else{
                String query = SQLQueries.selectAllFromTableWithCount();
                query = query.replaceFirst("\\?", "report");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getReportsByUser(int offset, int limit, List<Report> result, AbstractUser user) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                String query = SQLQueries.selectAllFromTableWithLimitCondition();
                query = query.replaceFirst("\\?", "report");
                query = query.replaceFirst("\\?", String.format("SpeakerLogin='%s'", user.getLogin()));
                statement = connection.prepareStatement(query);
                statement.setInt(1, offset);
                statement.setInt(2, limit);
            }else{
                String query = SQLQueries.selectAllFromTableWithCountCondition();
                query = query.replaceFirst("\\?", "report");
                query = query.replaceFirst("\\?", String.format("SpeakerLogin='%s'", user.getLogin()));
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
