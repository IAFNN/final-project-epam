package com.my.DAO.impl;

/**
 * Used to build and get mySQL queries
 */
public class SQLQueries {
    private SQLQueries(){}

    /**
     * Used to get select query with given number of conditions
     * @param conditionCount number of conditions
     * @return select sql query
     */
    public static String selectFromTableWithCondition(int conditionCount){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("select * from ? where ?");
        for(int i = 1; i < conditionCount; i++){
            stringBuilder.append(" and ?");
        }
        return stringBuilder.toString();
    }

    /**
     * Used to get select all sql query
     */
    public static String selectAllFromTable(){
        return "select * from ?";
    }

    /**
     * Used to get select all sql query with counting total number of records
     */
    public static String selectAllFromTableWithCount(){
        return "select SQL_CALC_FOUND_ROWS * from ?";
    }

    /**
     * Used to get select all sql query with counting total number of records and one condition
     */
    public static String selectAllFromTableWithCountCondition(){
        return "select SQL_CALC_FOUND_ROWS * from ? where ?";
    }
    /**
     * Used to get select all sql query with counting total number of records and limit
     */
    public static String selectAllFromTableWithLimit(){
        return "select SQL_CALC_FOUND_ROWS * from ? limit ?, ?";
    }
    /**
     * Used to get select all sql query with counting total number of records, one condition and limit
     */
    public static String selectAllFromTableWithLimitCondition(){
        return "select SQL_CALC_FOUND_ROWS * from ? where ? limit ?, ?";
    }
    /**
     * Used to get select all from events table sql query with counting total number of records, one condition
     * and limit sorted by time
     */
    public static String selectAllFromTableOrderByTime(){
        return "select SQL_CALC_FOUND_ROWS * from events where ? order by Time ? limit ?, ?";
    }
    /**
     * Used to get select all from events table sql query with counting total number of records, one condition
     * and limit sorted by number of registered of users
     */
    public static String selectAllFromTableOrderByNumberOfUsers(){
        return "select SQL_CALC_FOUND_ROWS * from events left outer join users_events on events.ID = users_events.EventID where ? group by ID order by count(users_events.EventID) ? limit ?, ?";
    }
    /**
     * Used to get select all from events table sql query with counting total number of records, one condition
     * and limit sorted by number of reports
     */
    public static String selectAllFromTablesOrderByNumberOfReports(){
        return "select SQL_CALC_FOUND_ROWS * from events left outer join events_reports on events.ID = events_reports.EventID where ? group by ID order by count(events_reports.EventID) ? limit ?, ?";
    }

    /**
     * Used to get select number of found rows sql query
     */
    public static String selectFoundRows(){
        return "select FOUND_ROWS()";
    }

    /**
     * Used to get insert into sql query with given number of parameters
     * @param parameterCount number of parameters
     * @return insert into sql query
     */
    public static String insertIntoTableWithParameters(int parameterCount){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("insert into ?(?");
        for(int i = 1; i < parameterCount; i++) {
            stringBuilder.append(", ?");
        }
        stringBuilder.append(") values (?");
        for(int i = 1; i < parameterCount; i++){
            stringBuilder.append(", ?");
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    /**
     * Used to get update sql query with given number of parameters and conditions
     * @param parameterCount number of parameters
     * @param conditionCount number of conditions
     * @return update sql query
     */
    public static String updateTable(int parameterCount, int conditionCount){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("update . set .=?");
        for(int i = 1; i < parameterCount; i++){
            stringBuilder.append(", .=?");
        }
        stringBuilder.append(" where .");
        for(int i = 1; i < conditionCount; i++){
            stringBuilder.append(" and .");
        }
        return stringBuilder.toString();
    }

    /**
     * Used to get delete sql query with one condition
     */
    public static String deleteFromTable(){
        return "delete from ? where ?";
    }
}