package com.my.DAO.impl;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.HikariCPDataSource;
import com.my.entity.AbstractUser;
import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AbstractUserDAOImpl implements AbstractUserDAO {
    private static final AbstractUserDAOImpl instance = new AbstractUserDAOImpl();

    private AbstractUserDAOImpl() {
    }

    /**
     * Used for getting instance of this class
     * @return instance of AbstractUserDAO implementation
     */
    public static AbstractUserDAOImpl getInstance() {
        return instance;
    }

    @Override
    public Optional<AbstractUser> get(String primaryKey) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "abstractusers");
            query = query.replaceFirst("\\?", String.format("Login='%s'", primaryKey));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                AbstractUser result = extractFromResultSet(resultSet);
                connection.close();
                return Optional.of(result);
            } else {
                connection.close();
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public List<AbstractUser> getAll() {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectAllFromTable();
            query = query.replaceFirst("\\?", "abstractusers");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<AbstractUser> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void save(AbstractUser abstractUser) throws IncorrectDataException {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(5);
            query = query.replaceFirst("\\?", "abstractusers");
            query = query.replaceFirst("\\?", "name");
            query = query.replaceFirst("\\?", "login");
            query = query.replaceFirst("\\?", "password");
            query = query.replaceFirst("\\?", "type");
            query = query.replaceFirst("\\?", "salt");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, abstractUser.getName());
            statement.setString(2, abstractUser.getLogin());
            statement.setString(3, abstractUser.getPassword());
            statement.setString(4, abstractUser.getUserType().convertToDBEnum());
            statement.setString(5, abstractUser.getSalt());
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IncorrectDataException();
        }
    }

    @Override
    public void update(AbstractUser abstractUser, String[] params) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.updateTable(3, 1);
            query = query.replaceFirst("\\.", "abstractusers");
            query = query.replaceFirst("\\.", "Name");
            query = query.replaceFirst("\\.", "Password");
            query = query.replaceFirst("\\.", "Type");
            query = query.replaceFirst("\\.", String.format("Login='%s'", abstractUser.getLogin()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, params[0]);
            statement.setString(2, params[1]);
            statement.setString(3, params[2]);
            statement.executeUpdate();
            abstractUser.setName(params[0]);
            abstractUser.setPassword(params[1]);
            abstractUser.setUserType(UserType.valueOf(params[2].toUpperCase()));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void delete(AbstractUser abstractUser) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.deleteFromTable();
            query = query.replaceFirst("\\?", "abstractusers");
            query = query.replaceFirst("\\?", String.format("Login='%s'", abstractUser.getLogin()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public AbstractUser extractFromResultSet(ResultSet resultSet) throws SQLException {
        AbstractUser result = new AbstractUser(resultSet.getString(1), resultSet.getString(2),
                resultSet.getString(3), UserType.valueOf(resultSet.getString(4).toUpperCase()), resultSet.getString(5));
        return result;
    }

    @Override
    public int getAll(int offset, int limit, List<AbstractUser> result) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                String query = SQLQueries.selectAllFromTableWithLimit();
                query = query.replaceFirst("\\?", "abstractusers");
                statement = connection.prepareStatement(query);
                statement.setInt(1, offset);
                statement.setInt(2, limit);
            }else{
                String query = SQLQueries.selectAllFromTableWithCount();
                query = query.replaceFirst("\\?", "abstractusers");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
