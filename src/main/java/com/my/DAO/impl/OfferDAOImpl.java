package com.my.DAO.impl;

import com.my.DAO.*;
import com.my.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OfferDAOImpl implements DatabaseDAO<Offer, Integer>, com.my.DAO.OfferDAO {
    private static final OfferDAO instance = new OfferDAOImpl();
    private OfferDAOImpl() {
    }
    /**
     * Used for getting instance of this class
     * @return instance of OfferDAO implementation
     */
    public static OfferDAO getInstance() {
        return instance;
    }

    @Override
    public Optional<Offer> get(Integer primaryKey) {
        try(Connection connection = HikariCPDataSource.getConnection()){
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "offers");
            query = query.replaceFirst("\\?", String.format("ID=%d", primaryKey));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Offer result = extractFromResultSet(resultSet);
            connection.close();
            return Optional.of(result);
        }catch (SQLException e){
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public List<Offer> getAll() {
        try(Connection connection = HikariCPDataSource.getConnection()){
            String query = SQLQueries.selectAllFromTable();
            query = query.replaceFirst("\\?", "offers");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<Offer> result = new ArrayList<>();
            while(resultSet.next()){
                result.add(extractFromResultSet(resultSet));
            }
            connection.close();
            return result;
        }catch (SQLException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void save(Offer offer) {
        try(Connection connection = HikariCPDataSource.getConnection()){
            String query = SQLQueries.insertIntoTableWithParameters(7);
            query = query.replaceFirst("\\?", "offers");
            query = query.replaceFirst("\\?", "ID");
            query = query.replaceFirst("\\?", "targetUserType");
            query = query.replaceFirst("\\?", "offerType");
            query = query.replaceFirst("\\?", "topicID");
            query = query.replaceFirst("\\?", "speakerLogin");
            query = query.replaceFirst("\\?", "eventID");
            query = query.replaceFirst("\\?", "reportID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, offer.getId());
            statement.setString(2, offer.getTargetUserType().convertToDBEnum());
            statement.setString(3, offer.getOfferType());
            if(offer.getOfferType().equalsIgnoreCase("create")){
                statement.setInt(4, offer.getOfferFunction().getTopicID());
                statement.setString(5, offer.getOfferFunction().getSpeakerLogin());
                statement.setInt(6, offer.getOfferFunction().getEventID());
                statement.setNull(7, Types.INTEGER);
            }else{
                if(offer.getTargetUserType() == UserType.MODERATOR){
                    statement.setNull(4, Types.INTEGER);
                    statement.setString(5, offer.getOfferFunction().getSpeakerLogin());
                    statement.setNull(6, Types.INTEGER);
                    statement.setInt(7, offer.getOfferFunction().getReportID());
                }else{
                    statement.setInt(4, offer.getOfferFunction().getTopicID());
                    statement.setNull(5, Types.VARCHAR);
                    statement.setNull(6, Types.INTEGER);
                    statement.setInt(7, offer.getOfferFunction().getReportID());
                }
            }
            statement.executeUpdate();
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public Offer extractFromResultSet(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(1);
        UserType targetUserType = UserType.valueOf(resultSet.getString(2).toUpperCase());
        String offerType = resultSet.getString(3);
        OfferFunction offerFunction;
        if(offerType.equalsIgnoreCase("create")){
            int topicID = resultSet.getInt(4);
            String speakerLogin = resultSet.getString(5);
            int eventID = resultSet.getInt(6);
            offerFunction = new OfferFunction() {
                @Override
                public void doRequest() {
                    ReportDAO reportDAO = ReportDAOImpl.getInstance();
                    TopicDAO topicDAO = TopicDAOImpl.getInstance();
                    EventDAO eventDAO = EventDAOImpl.getInstance();
                    AbstractUserDAO abstractUserDAO = AbstractUserDAOImpl.getInstance();
                    Event event = eventDAO.get(eventID).orElse(null);
                    Report report = new Report(topicDAO.get(topicID).orElse(null), abstractUserDAO.get(speakerLogin).orElse(null));
                    reportDAO.save(report);
                    eventDAO.setReportToEvent(report, event);
                }

                @Override
                public int getTopicID() {
                    return topicID;
                }

                @Override
                public String getSpeakerLogin() {
                    return speakerLogin;
                }

                @Override
                public int getEventID() {
                    return eventID;
                }
            };
        }else{
            if(targetUserType == UserType.MODERATOR){
                String speakerLogin = resultSet.getString(5);
                int reportID = resultSet.getInt(7);
                offerFunction = new OfferFunction() {
                    @Override
                    public void doRequest() {
                        ReportDAO reportDAO = ReportDAOImpl.getInstance();
                        Report report = reportDAO.get(reportID).get();
                        reportDAO.update(report, new String[]{speakerLogin, String.valueOf(report.getTopic().getId())});
                    }

                    @Override
                    public int getReportID() {
                        return reportID;
                    }

                    @Override
                    public String getSpeakerLogin() {
                        return speakerLogin;
                    }
                };
            }else{
                int topicID = resultSet.getInt(4);
                int reportID = resultSet.getInt(7);
                offerFunction = new OfferFunction() {
                    @Override
                    public void doRequest() {
                        ReportDAO reportDAO = ReportDAOImpl.getInstance();
                        Report report = reportDAO.get(reportID).get();
                        reportDAO.update(report, new String[]{report.getSpeaker().getLogin(), String.valueOf(topicID)});
                    }

                    @Override
                    public int getTopicID() {
                        return topicID;
                    }

                    @Override
                    public int getReportID() {
                        return reportID;
                    }
                };
            }
        }
        return new Offer(id, targetUserType, offerType, offerFunction);
    }

    @Override
    public void update(Offer offer, String[] params) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Offer offer) {
        try(Connection connection = HikariCPDataSource.getConnection()){
            String query = SQLQueries.deleteFromTable();
            query = query.replaceFirst("\\?", "offers");
            query = query.replaceFirst("\\?", String.format("ID=%d", offer.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getAll(int offset, int limit, List<Offer> result) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                String query = SQLQueries.selectAllFromTableWithLimit();
                query = query.replaceFirst("\\?", "offers");
                statement = connection.prepareStatement(query);
                statement.setInt(1, offset);
                statement.setInt(2, limit);
            }else{
                String query = SQLQueries.selectAllFromTableWithCount();
                query = query.replaceFirst("\\?", "offers");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
