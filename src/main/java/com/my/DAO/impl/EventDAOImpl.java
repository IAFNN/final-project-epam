package com.my.DAO.impl;

import com.my.DAO.*;
import com.my.entity.AbstractUser;
import com.my.entity.Event;
import com.my.entity.Report;
import com.my.entity.Topic;
import com.my.exception.IncorrectDataException;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EventDAOImpl implements DatabaseDAO<Event, Integer>, com.my.DAO.EventDAO {
    private static final EventDAOImpl instance = new EventDAOImpl();
    private EventDAOImpl() {
    }
    /**
     * Used for getting instance of this class
     * @return instance of EventDAO implementation
     */
    public static EventDAOImpl getInstance() {
        return instance;
    }

    @Override
    public Optional<Event> get(Integer primaryKey) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "events");
            query = query.replaceFirst("\\?", String.format("ID=%d", primaryKey));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Event result = extractFromResultSet(resultSet);
                connection.close();
                return Optional.of(result);
            } else {
                connection.close();
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public List<Event> getAll() {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectAllFromTable();
            query = query.replaceFirst("\\?", "events");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<Event> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void save(Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(4);
            query = query.replaceFirst("\\?", "events");
            query = query.replaceFirst("\\?", "ID");
            query = query.replaceFirst("\\?", "Name");
            query = query.replaceFirst("\\?", "Time");
            query = query.replaceFirst("\\?", "Location");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, event.getId());
            statement.setString(2, event.getName());
            statement.setTimestamp(3, Timestamp.from(event.getDate()));
            statement.setString(4, event.getLocation());
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Event event, String[] params) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.updateTable(3, 1);
            query = query.replaceFirst("\\.", "events");
            query = query.replaceFirst("\\.", "Name");
            query = query.replaceFirst("\\.", "Time");
            query = query.replaceFirst("\\.", "Location");
            query = query.replaceFirst("\\.", String.format("ID=%d", event.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, params[0]);
            LocalDateTime dateTime = LocalDateTime.parse(params[1]);
            statement.setTimestamp(2, Timestamp.valueOf(dateTime));
            statement.setString(3, params[2]);
            statement.executeUpdate();
            event.setName(params[0]);
            event.setLocation(params[2]);
            params[1] = params[1] + "Z";
            event.setDate(Instant.parse(params[1]));
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void delete(Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.deleteFromTable();
            query = query.replaceFirst("\\?", "events");
            query = query.replaceFirst("\\?", String.format("ID=%d", event.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<AbstractUser> getUsersByEvent(Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "users_events");
            query = query.replaceFirst("\\?", String.format("EventID=%d", event.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<AbstractUser> users = new ArrayList<>();
            AbstractUserDAO abstractUserDAO = AbstractUserDAOImpl.getInstance();
            while (resultSet.next()) {
                query = SQLQueries.selectFromTableWithCondition(1);
                query = query.replaceFirst("\\?", "abstractusers");
                query = query.replaceFirst("\\?", String.format("Login='%s'", resultSet.getString(1)));
                statement = connection.prepareStatement(query);
                ResultSet resultSet1 = statement.executeQuery();
                resultSet1.next();
                users.add(abstractUserDAO.extractFromResultSet(resultSet1));
            }
            connection.close();
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public List<Event> getEventsByUser(AbstractUser user) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "users_events");
            query = query.replaceFirst("\\?", String.format("UserLogin='%s'", user.getLogin()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Event> events = new ArrayList<>();
            while (resultSet.next()) {
                query = SQLQueries.selectFromTableWithCondition(1);
                query = query.replaceFirst("\\?", "events");
                query = query.replaceFirst("\\?", String.format("ID=%s", resultSet.getString(2)));
                statement = connection.prepareStatement(query);
                events.add(extractFromResultSet(statement.executeQuery()));
            }
            connection.close();
            return events;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public List<Report> getReportsByEvent(Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "events_reports");
            query = query.replaceFirst("\\?", String.format("EventID=%d", event.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Report> reports = new ArrayList<>();
            ReportDAO reportDAO = ReportDAOImpl.getInstance();
            while (resultSet.next()) {
                query = SQLQueries.selectFromTableWithCondition(1);
                query = query.replaceFirst("\\?", "report");
                query = query.replaceFirst("\\?", String.format("ID=%s", resultSet.getString(2)));
                statement = connection.prepareStatement(query);
                ResultSet resultSet1 = statement.executeQuery();
                resultSet1.next();
                reports.add(reportDAO.extractFromResultSet(resultSet1));
            }
            connection.close();
            return reports;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<Event> getEventByReport(Report report) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "events_reports");
            query = query.replaceFirst("\\?", String.format("ReportID=%d", report.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                query = SQLQueries.selectFromTableWithCondition(1);
                query = query.replaceFirst("\\?", "events");
                query = query.replaceFirst("\\?", String.format("ID=%s", resultSet.getString(1)));
                statement = connection.prepareStatement(query);
                ResultSet resultSet1 = statement.executeQuery();
                resultSet1.next();
                Event result = extractFromResultSet(resultSet1);
                connection.close();
                return Optional.of(result);
            } else {
                connection.close();
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public List<Topic> getTopicsByEvent(Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "events_topics");
            query = query.replaceFirst("\\?", String.format("EventsID=%d", event.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Topic> topics = new ArrayList<>();
            TopicDAO topicDAO = TopicDAOImpl.getInstance();
            while (resultSet.next()) {
                query = SQLQueries.selectFromTableWithCondition(1);
                query = query.replaceFirst("\\?", "topics");
                query = query.replaceFirst("\\?", String.format("ID=%s", resultSet.getString(2)));
                statement = connection.prepareStatement(query);
                ResultSet resultSet1 = statement.executeQuery();
                resultSet1.next();
                topics.add(topicDAO.extractFromResultSet(resultSet1));
            }
            connection.close();
            return topics;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<Event> getEventByTopic(Topic topic) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "events_topics");
            query = query.replaceFirst("\\?", String.format("TopicID=%d", topic.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                query = SQLQueries.selectFromTableWithCondition(1);
                query = query.replaceFirst("\\?", "events");
                query = query.replaceFirst("\\?", String.format("ID=%s", resultSet.getString(1)));
                statement = connection.prepareStatement(query);
                ResultSet resultSet1 = statement.executeQuery();
                resultSet1.next();
                Event result = extractFromResultSet(resultSet1);
                connection.close();
                return Optional.of(result);
            } else {
                connection.close();
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
    @Override
    public void setEventToTopic(Event event, Topic topic){
        try(Connection connection = HikariCPDataSource.getConnection()){
            String query = SQLQueries.insertIntoTableWithParameters(2);
            query = query.replaceFirst("\\?", "events_topics");
            query = query.replaceFirst("\\?", "eventsid");
            query = query.replaceFirst("\\?", "topicid");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, event.getId());
            statement.setInt(2, topic.getId());
            statement.executeUpdate();
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void addReportToEvent(Event event, Report report) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(2);
            query = query.replaceFirst("\\?", "events_reports");
            query = query.replaceFirst("\\?", "eventsid");
            query = query.replaceFirst("\\?", "reportid");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, event.getId());
            statement.setInt(2, report.getId());
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addTopicToEvent(Topic topic, Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(2);
            query = query.replaceFirst("\\?", "events_topics");
            query = query.replaceFirst("\\?", "eventsid");
            query = query.replaceFirst("\\?", "topicid");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, event.getId());
            statement.setInt(2, topic.getId());
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addUserToEvent(AbstractUser user, Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(3);
            query = query.replaceFirst("\\?", "users_events");
            query = query.replaceFirst("\\?", "UserLogin");
            query = query.replaceFirst("\\?", "EventID");
            query = query.replaceFirst("\\?", "hasAttended");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getLogin());
            statement.setInt(2, event.getId());
            statement.setBoolean(3, false);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Event extractFromResultSet(ResultSet resultSet) throws SQLException {
        Event event = new Event(resultSet.getInt(1), resultSet.getString(2), resultSet.getTimestamp(3).toInstant(), resultSet.getString(4));
        return event;
    }

    @Override
    public void registerAttendance(Event event, AbstractUser user) throws IncorrectDataException{
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(2);
            query = query.replaceFirst("\\?", "users_events");
            query = query.replaceFirst("\\?", String.format("EventID=%d", event.getId()));
            query = query.replaceFirst("\\?", String.format("UserLogin='%s'", user.getLogin()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next()){
                throw new IncorrectDataException();
            }
            query = SQLQueries.updateTable(1, 2);
            query = query.replaceFirst("\\.", "users_events");
            query = query.replaceFirst("\\.", "hasAttended");
            query = query.replaceFirst("\\.", String.format("EventID=%d", event.getId()));
            query = query.replaceFirst("\\.", String.format("UserLogin='%s'", user.getLogin()));
            statement = connection.prepareStatement(query);
            statement.setBoolean(1, true);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getHasAttended(Event event, AbstractUser user) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(2);
            query = query.replaceFirst("\\?", "users_events");
            query = query.replaceFirst("\\?", String.format("EventID=%d", event.getId()));
            query = query.replaceFirst("\\?", String.format("UserLogin='%s'", user.getLogin()));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            boolean result = resultSet.getBoolean(3);
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<AbstractUser> getUsersAttendedEvent(Event event) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(2);
            query = query.replaceFirst("\\?", "users_events");
            query = query.replaceFirst("\\?", String.format("EventID=%d", event.getId()));
            query = query.replaceFirst("\\?", "hasAttended=1");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<AbstractUser> result = new ArrayList<>();
            AbstractUserDAOImpl userDAO = AbstractUserDAOImpl.getInstance();
            while (resultSet.next()) {
                result.add(userDAO.get(resultSet.getString(1)).get());
            }
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
    @Override
    public void setReportToEvent(Report report, Event event){
        try(Connection connection = HikariCPDataSource.getConnection()){
            String query = SQLQueries.insertIntoTableWithParameters(2);
            query = query.replaceFirst("\\?", "events_reports");
            query = query.replaceFirst("\\?", "eventid");
            query = query.replaceFirst("\\?", "reportid");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, event.getId());
            statement.setInt(2, report.getId());
            statement.executeUpdate();
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getAll(int offset, int limit, List<Event> result) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                String query = SQLQueries.selectAllFromTableWithLimit();
                query = query.replaceFirst("\\?", "events");
                statement = connection.prepareStatement(query);
                statement.setInt(1, offset);
                statement.setInt(2, limit);
            }else{
                String query = SQLQueries.selectAllFromTableWithCount();
                query = query.replaceFirst("\\?", "events");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    @Override
    public int getAllPastEvents(int offset, int limit, List<Event> result, String sortBy, boolean order){
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                if(sortBy != null && !sortBy.isEmpty()) {
                    String orderString;
                    if(order){
                        orderString = "ASC";
                    }else{
                        orderString = "DESC";
                    }
                    if(sortBy.equals("time")){
                        String query = SQLQueries.selectAllFromTableOrderByTime();
                        query = query.replaceFirst("\\?", "Time<NOW()");
                        query = query.replaceFirst("\\?", orderString);
                        statement = connection.prepareStatement(query);
                        statement.setInt(1, offset);
                        statement.setInt(2, limit);
                    }else if(sortBy.equals("numberOfUsers")){
                        String query = SQLQueries.selectAllFromTableOrderByNumberOfUsers();
                        query = query.replaceFirst("\\?", "Time<NOW()");
                        query = query.replaceFirst("\\?", orderString);
                        statement = connection.prepareStatement(query);
                        statement.setInt(1, offset);
                        statement.setInt(2, limit);
                    }else{
                        String query = SQLQueries.selectAllFromTablesOrderByNumberOfReports();
                        query = query.replaceFirst("\\?", "Time<NOW()");
                        query = query.replaceFirst("\\?", orderString);
                        statement = connection.prepareStatement(query);
                        statement.setInt(1, offset);
                        statement.setInt(2, limit);
                    }
                }else{
                    String query = SQLQueries.selectAllFromTableWithLimitCondition();
                    query = query.replaceFirst("\\?", "events");
                    query = query.replaceFirst("\\?", "Time<NOW()");
                    statement = connection.prepareStatement(query);
                    statement.setInt(1, offset);
                    statement.setInt(2, limit);
                }
            }else{
                String query = SQLQueries.selectAllFromTableWithCountCondition();
                query = query.replaceFirst("\\?", "events");
                query = query.replaceFirst("\\?", "Time<NOW()");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    @Override
    public int getAllFutureEvents(int offset, int limit, List<Event> result, String sortBy, boolean order){
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                if(sortBy != null && !sortBy.isEmpty()) {
                    String orderString;
                    if(order){
                        orderString = "ASC";
                    }else{
                        orderString = "DESC";
                    }
                    if(sortBy.equals("time")){
                        String query = SQLQueries.selectAllFromTableOrderByTime();
                        query = query.replaceFirst("\\?", "Time>NOW()");
                        query = query.replaceFirst("\\?", orderString);
                        statement = connection.prepareStatement(query);
                        statement.setInt(1, offset);
                        statement.setInt(2, limit);
                    }else if(sortBy.equals("numberOfUsers")){
                        String query = SQLQueries.selectAllFromTableOrderByNumberOfUsers();
                        query = query.replaceFirst("\\?", "Time>NOW()");
                        query = query.replaceFirst("\\?", orderString);
                        statement = connection.prepareStatement(query);
                        statement.setInt(1, offset);
                        statement.setInt(2, limit);
                    }else{
                        String query = SQLQueries.selectAllFromTablesOrderByNumberOfReports();
                        query = query.replaceFirst("\\?", "Time>NOW()");
                        query = query.replaceFirst("\\?", orderString);
                        statement = connection.prepareStatement(query);
                        statement.setInt(1, offset);
                        statement.setInt(2, limit);
                    }
                }else{
                    String query = SQLQueries.selectAllFromTableWithLimitCondition();
                    query = query.replaceFirst("\\?", "events");
                    query = query.replaceFirst("\\?", "Time>NOW()");
                    statement = connection.prepareStatement(query);
                    statement.setInt(1, offset);
                    statement.setInt(2, limit);
                }
            }else{
                String query = SQLQueries.selectAllFromTableWithCountCondition();
                query = query.replaceFirst("\\?", "events");
                query = query.replaceFirst("\\?", "Time>NOW()");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
