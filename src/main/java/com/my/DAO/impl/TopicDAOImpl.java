package com.my.DAO.impl;

import com.my.DAO.DatabaseDAO;
import com.my.DAO.HikariCPDataSource;
import com.my.DAO.TopicDAO;
import com.my.entity.Topic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TopicDAOImpl implements DatabaseDAO<Topic, Integer>, com.my.DAO.TopicDAO {
    private static final TopicDAO instance = new TopicDAOImpl();

    private TopicDAOImpl() {
    }
    /**
     * Used for getting instance of this class
     * @return instance of TopicDAO implementation
     */
    public static TopicDAO getInstance() {
        return instance;
    }

    @Override
    public Optional<Topic> get(Integer primaryKey) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectFromTableWithCondition(1);
            query = query.replaceFirst("\\?", "topics");
            query = query.replaceFirst("\\?", String.format("ID=%d", primaryKey));
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Topic result = extractFromResultSet(resultSet);
                connection.close();
                return Optional.of(result);
            } else {
                connection.close();
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public int getAll(int offset, int limit, List<Topic> result) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            PreparedStatement statement;
            if(limit > 0) {
                String query = SQLQueries.selectAllFromTableWithLimit();
                query = query.replaceFirst("\\?", "topics");
                statement = connection.prepareStatement(query);
                statement.setInt(1, offset);
                statement.setInt(2, limit);
            }else{
                String query = SQLQueries.selectAllFromTableWithCount();
                query = query.replaceFirst("\\?", "topics");
                statement = connection.prepareStatement(query);
            }
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            statement = connection.prepareStatement(SQLQueries.selectFoundRows());
            resultSet = statement.executeQuery();
            resultSet.next();
            int resultCount = resultSet.getInt(1);
            connection.close();
            return resultCount;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    @Override
    public List<Topic> getAll() {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.selectAllFromTable();
            query = query.replaceFirst("\\?", "topics");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            List<Topic> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(extractFromResultSet(resultSet));
            }
            connection.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void save(Topic topic) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.insertIntoTableWithParameters(2);
            query = query.replaceFirst("\\?", "topics");
            query = query.replaceFirst("\\?", "ID");
            query = query.replaceFirst("\\?", "Name");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, topic.getId());
            statement.setString(2, topic.getName());
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Topic topic, String[] params) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.updateTable(1, 1);
            query = query.replaceFirst("\\.", "report");
            query = query.replaceFirst("\\.", "Name");
            query = query.replaceFirst("\\.", String.format("ID=%d", topic.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, params[0]);
            statement.executeUpdate();
            connection.close();
            topic.setName(params[0]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void delete(Topic topic) {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            String query = SQLQueries.deleteFromTable();
            query = query.replaceFirst("\\?", "topics");
            query = query.replaceFirst("\\?", String.format("ID=%d", topic.getId()));
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Topic extractFromResultSet(ResultSet resultSet) throws SQLException {
        Topic topic = new Topic(resultSet.getInt(1), resultSet.getString(2));
        return topic;
    }
}
