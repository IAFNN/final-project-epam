package com.my.DAO;

import com.my.entity.AbstractUser;
import com.my.entity.Report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
/**
 * Report sql table DAO
 */
public interface ReportDAO extends DatabaseDAO<Report, Integer>{
    /**
     * Used for getting report entity, which represents record in table with id specified by primaryKey parameter
     * @param primaryKey id of report to find
     * @return Optional with report entity if such report was found or empty optional otherwise
     */
    Optional<Report> get(Integer primaryKey);
    /**
     * Used for getting list with all reports in table
     * @return List containing all reports, which exist in table. Empty list if no reports were found.
     */
    List<Report> getAll();
    /**
     * Used to save report in table
     * @param report report to be saved in table
     */
    void save(Report report);
    /**
     * Used to update data in report record in table specified by report entity
     * @param report report to be updated
     * @param params String array where first element is speaker login, second - topic id
     */
    void update(Report report, String[] params);
    /**
     * Used to delete report from table
     * @param report Report to be deleted
     */
    void delete(Report report);
    /**
     * Used to convert ResultSet to Report
     * @param resultSet ResultSet with id, topic id and user id columns with indexes 1-3
     * @return Report entity represented by resultSet
     * @throws SQLException if resultSet is invalid
     */
    Report extractFromResultSet(ResultSet resultSet) throws SQLException;
    /**
     * Used to get all Reports from table according to given offset and limit.
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved Reports will be stored
     * @return number of all reports in table
     */
    int getAll(int offset, int limit, List<Report> result);

    /**
     * Used to get all Reports, where given User is registered as speaker from table according to given offset and limit.
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved Reports will be stored
     * @param user speaker, whose reports will be retrieved
     * @return number of all reports in table where given user is registered as speaker
     */
    int getReportsByUser(int offset, int limit, List<Report> result, AbstractUser user);
}
