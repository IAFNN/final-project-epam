package com.my.DAO;

import com.my.entity.Offer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
/**
 * Offers sql table DAO
 */
public interface OfferDAO extends DatabaseDAO<Offer, Integer> {
    /**
     * Used for getting offer entity, which represents record in table with id specified by primaryKey parameter
     * @param primaryKey id of offer to find
     * @return Optional with offer entity if such offer was found or empty optional otherwise
     */
    Optional<Offer> get(Integer primaryKey);
    /**
     * Used for getting list with all offers in table
     * @return List containing all offers, which exist in table. Empty list if no offers were found.
     */
    List<Offer> getAll();

    /**
     * Used to save offer in table
     * @param offer offer to be saved in table
     */
    void save(Offer offer);
    /**
     * Used to convert ResultSet to Offer
     * @param resultSet ResultSet with id, target user type, offer type and other parameters, which depend on offer type
     *                  with indexes 1-5
     * @return Offer entity represented by resultSet
     * @throws SQLException if resultSet is invalid
     */
    Offer extractFromResultSet(ResultSet resultSet) throws SQLException;

    /**
     * Operation unsupported
     */
    void update(Offer offer, String[] params);
    /**
     * Used to delete offer from table
     * @param offer Offer to be deleted
     */
    void delete(Offer offer);
    /**
     * Used to get all Offers from table according to given offset and limit.
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved Offers will be stored
     * @return number of all offers in table
     */
    int getAll(int offset, int limit, List<Offer> result);
}
