package com.my.DAO;

import com.my.entity.AbstractUser;
import com.my.entity.Event;
import com.my.entity.Report;
import com.my.entity.Topic;
import com.my.exception.IncorrectDataException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
/**
 * Event sql table DAO
 */
public interface EventDAO extends DatabaseDAO<Event, Integer>{
    /**
     * Used for getting event entity, which represents record in table with id specified by primaryKey parameter
     * @param primaryKey id of event to find
     * @return Optional with event entity if such event was found or empty optional otherwise
     */
    Optional<Event> get(Integer primaryKey);
    /**
     * Used for getting list with all events in table
     * @return List containing all events, which exist in table. Empty list if no events were found.
     */
    List<Event> getAll();
    /**
     * Used to save event in table
     * @param event event to be saved in table
     */
    void save(Event event);
    /**
     * Used to update data in event record in table specified by event entity
     * @param event event to be updated
     * @param params String array where first element is name, second - time and third - location
     */
    void update(Event event, String[] params);
    /**
     * Used to delete event from table
     * @param event Event to be deleted
     */
    void delete(Event event);

    /**
     * Used to get List with users registered for given event
     * @param event event for which users are registered
     * @return List with users registered for event
     */
    List<AbstractUser> getUsersByEvent(Event event);

    /**
     * Used to get List with events for which given user is registered
     * @param user user whose events will be retrieved
     * @return List of events where give user is registered
     */
    List<Event> getEventsByUser(AbstractUser user);

    /**
     * Used to get list of reports for given event
     * @param event event, which reports will be retrieved
     * @return List of reports for event
     */
    List<Report> getReportsByEvent(Event event);

    Optional<Event> getEventByReport(Report report);

    List<Topic> getTopicsByEvent(Event event);

    Optional<Event> getEventByTopic(Topic topic);

    void setEventToTopic(Event event, Topic topic);

    void addReportToEvent(Event event, Report report);

    void addTopicToEvent(Topic topic, Event event);

    void addUserToEvent(AbstractUser user, Event event);

    /**
     * Used to convert ResultSet to Event
     * @param resultSet ResultSet with id, name, date and location columns with indexes 1-4
     * @return Event entity represented by resultSet
     * @throws SQLException if resultSet is invalid
     */
    Event extractFromResultSet(ResultSet resultSet) throws SQLException;

    /**
     * Used to register attendance of given user in given event
     * @param event event that user has attended
     * @param user user that attended event
     * @throws IncorrectDataException if such user or event doesn't exist or if such user hasn't registered for this event
     */
    void registerAttendance(Event event, AbstractUser user) throws IncorrectDataException;

    /**
     * Used to check if given user attended given event
     * @param event event to check if user attended it
     * @param user user to check if he/she attended event
     * @return true if given user has attended given event, false otherwise
     */
    boolean getHasAttended(Event event, AbstractUser user);

    List<AbstractUser> getUsersAttendedEvent(Event event);

    void setReportToEvent(Report report, Event event);
    /**
     * Used to get all Events from table according to given offset and limit.
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved Events will be stored
     * @return number of all events in table
     */
    int getAll(int offset, int limit, List<Event> result);

    /**
     * Used to get all past events from table according to given offset and limit, sorted according to sortBy parameter
     * in order according to order parameter
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved events will be stored
     * @param sortBy parameter by which events will be sorted
     * @param order order in which events will be sorted
     * @return number of all past events
     */
    int getAllPastEvents(int offset, int limit, List<Event> result, String sortBy, boolean order);
    /**
     * Used to get all future events from table according to given offset and limit, sorted according to sortBy parameter
     * in order according to order parameter
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved events will be stored
     * @param sortBy parameter by which events will be sorted
     * @param order order in which events will be sorted
     * @return number of all future events
     */
    int getAllFutureEvents(int offset, int limit, List<Event> result, String sortBy, boolean order);
}
