package com.my.DAO;

import com.my.entity.AbstractUser;
import com.my.exception.IncorrectDataException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
/**
 * Abstract user sql table DAO
 */
public interface AbstractUserDAO extends DatabaseDAO<AbstractUser, String> {
    /**
     * Used for getting user entity, which represents record in table with login specified by primaryKey parameter
     * @param primaryKey login of user to find
     * @return Optional with user entity if such user was found or empty optional otherwise
     */
    @Override
    Optional<AbstractUser> get(String primaryKey);
    /**
     * Used for getting list with all users in table
     * @return List containing all users, which exist in table. Empty list if no users were found.
     */
    @Override
    List<AbstractUser> getAll();
    /**
     * Used to save user in table
     * @param abstractUser user to be saved in table
     * @throws IncorrectDataException if some data in user is invalid
     */
    @Override
    void save(AbstractUser abstractUser) throws IncorrectDataException;

    /**
     * Used to update data in user record in table specified by abstractUser entity
     * @param abstractUser user to be updated
     * @param params String array where first element is name, second - password, third - user type
     */
    @Override
    void update(AbstractUser abstractUser, String[] params);

    /**
     * Used to delete user from table
     * @param abstractUser User to be deleted
     */
    @Override
    void delete(AbstractUser abstractUser);

    /**
     * Used to convert ResultSet to User
     * @param resultSet ResultSet with name, login, password, user type and salt columns with indexes 1-5
     * @return User entity represented by resultSet
     * @throws SQLException if resultSet is invalid
     */
    AbstractUser extractFromResultSet(ResultSet resultSet) throws SQLException;

    /**
     * Used to get all Users from table according to given offset and limit.
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved Users will be stored
     * @return number of all users in table
     */
    @Override
    int getAll(int offset, int limit, List<AbstractUser> result);
}
