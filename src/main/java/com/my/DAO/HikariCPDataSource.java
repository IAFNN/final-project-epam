package com.my.DAO;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * mySQL database connection pool
 */
public class HikariCPDataSource {
    private static HikariConfig hikariConfig = new HikariConfig();
    private static HikariDataSource dataSource;

    /**
     * Used to get connection to database
     * @return connection to database
     */
    public static synchronized Connection getConnection() throws SQLException {
        if (dataSource == null) {
            throw new RuntimeException();
        }
        return dataSource.getConnection();
    }

    /**
     * Used to initialize connection pool.
     * Some details of database connection are specified in database.properties in resources directory.
     * @param realPath real path to application
     */
    public static void init(String realPath) {
        Properties databaseProperties = new Properties();
        try (InputStream inputStream = new FileInputStream(realPath + "WEB-INF/classes/database.properties")) {
            databaseProperties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        hikariConfig.setJdbcUrl(databaseProperties.getProperty("url"));
        hikariConfig.setUsername(databaseProperties.getProperty("user"));
        hikariConfig.setPassword(databaseProperties.getProperty("password"));
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        hikariConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource = new HikariDataSource(hikariConfig);
    }

    private HikariCPDataSource() {
    }
}

