package com.my.DAO;

import com.my.exception.IncorrectDataException;

import java.util.List;
import java.util.Optional;

/**
 * Interface that must be implemented by sql table DAOs
 * @param <T> entity of sql table
 * @param <K> primary key for access to table
 */
public interface DatabaseDAO<T, K> {
    /**
     * Used to get entity by primary key
     * @param primaryKey primary key for accessing table
     * @return entity represented by record in table
     */
    Optional<T> get(K primaryKey);

    /**
     * Used to get limited number of entities with given offset
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved entities will be stored
     * @return total number of entities in table
     */
    int getAll(int offset, int limit, List<T> result);

    /**
     * Used to get all entities in table
     * @return List with all entities in table
     */
    List<T> getAll();

    /**
     * Used to save entity in sql table
     * @param t entity to be saved
     * @throws IncorrectDataException if some data in entity is invalid
     */
    void save(T t) throws IncorrectDataException;

    /**
     * Used to update data in sql table
     * @param t entity which represent record in sql table
     * @param params String array with new data
     */
    void update(T t, String[] params);

    /**
     * Used to delete entity from sql table
     * @param t entity to be deleted
     */
    void delete(T t);
}
