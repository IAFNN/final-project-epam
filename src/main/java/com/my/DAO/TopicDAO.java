package com.my.DAO;

import com.my.entity.Topic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
/**
 * Topic sql table DAO
 */
public interface TopicDAO extends DatabaseDAO<Topic, Integer>{
    /**
     * Used for getting topic entity, which represents record in table with id specified by primaryKey parameter
     * @param primaryKey id of topic to find
     * @return Optional with topic entity if such topic was found or empty optional otherwise
     */
    Optional<Topic> get(Integer primaryKey);
    /**
     * Used to get all Topic from table according to given offset and limit.
     * @param offset number of records to be skipped
     * @param limit number of records to be retrieved
     * @param result List where retrieved Topics will be stored
     * @return number of all topics in table
     */
    int getAll(int offset, int limit, List<Topic> result);

    /**
     * Used for getting list with all topics in table
     * @return List containing all topics, which exist in table. Empty list if no topics were found.
     */
    List<Topic> getAll();
    /**
     * Used to save topic in table
     * @param topic topic to be saved in table
     */
    void save(Topic topic);
    /**
     * Used to update data in topic record in table specified by topic entity
     * @param topic topic to be updated
     * @param params String array where first element is name
     */
    void update(Topic topic, String[] params);
    /**
     * Used to delete topic from table
     * @param topic Topic to be deleted
     */
    void delete(Topic topic);
    /**
     * Used to convert ResultSet to Topic
     * @param resultSet ResultSet with id and name columns with indexes 1-2
     * @return Topic entity represented by resultSet
     * @throws SQLException if resultSet is invalid
     */
    Topic extractFromResultSet(ResultSet resultSet) throws SQLException;
}
