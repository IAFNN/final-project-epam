package com.my.mappers;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.impl.AbstractUserDAOImpl;
import com.my.DTO.AbstractUserDTO;
import com.my.entity.AbstractUser;

/**
 * Used to convert UserDTO to User entity and vice versa. Requires initialization.
 */
public class UserMapper {
    private static AbstractUserDAO userDAO;
    public static AbstractUserDTO toDTO(AbstractUser user){
        return new AbstractUserDTO(user.getName(), user.getLogin(), user.getUserType());
    }
    public static AbstractUser toEntity(AbstractUserDTO userDTO){
        return userDAO.get(userDTO.getLogin()).orElse(null);
    }
    public static void init(AbstractUserDAO userDAO){
        UserMapper.userDAO = userDAO;
    }
}
