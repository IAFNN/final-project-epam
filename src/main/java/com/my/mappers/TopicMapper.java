package com.my.mappers;

import com.my.DAO.EventDAO;
import com.my.DAO.TopicDAO;
import com.my.DTO.TopicDTO;
import com.my.entity.Event;
import com.my.entity.Topic;

/**
 * Used to convert TopicDTO to Topic entity and vice versa. Requires initialization.
 */
public class TopicMapper {
    private static EventDAO eventDAO;
    private static TopicDAO topicDAO;
    public static void init(EventDAO eventDAO, TopicDAO topicDAO){
        TopicMapper.eventDAO = eventDAO;
        TopicMapper.topicDAO = topicDAO;
    }
    public static TopicDTO toDTO(Topic topic){
        Event event = eventDAO.getEventByTopic(topic).orElse(null);
        int eventID;
        String eventName;
        if(event == null){
            eventID = -1;
            eventName = null;
        }else{
            eventID = event.getId();
            eventName = event.getName();
        }
        return new TopicDTO(topic.getId(), topic.getName(), eventID, eventName);
    }
    public static Topic toEntity(TopicDTO topicDTO){
        return topicDAO.get(topicDTO.getId()).orElse(null);
    }
}
