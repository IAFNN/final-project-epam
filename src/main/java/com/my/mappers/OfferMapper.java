package com.my.mappers;

import com.my.DAO.OfferDAO;
import com.my.DAO.impl.OfferDAOImpl;
import com.my.DTO.OfferDTO;
import com.my.entity.Offer;
/**
 * Used to convert OfferDTO to Offer entity and vice versa. Requires initialization.
 */
public class OfferMapper {
    public static OfferDTO toDTO(Offer offer){
        return new OfferDTO(offer.getId(), offer.getOfferType(), offer.getOfferFunction().getTopicID(), offer.getOfferFunction().getReportID(),
                offer.getOfferFunction().getEventID(), offer.getOfferFunction().getSpeakerLogin(), offer.getTargetUserType());
    }
    public static Offer toEntity(OfferDTO offerDTO){
        OfferDAO offerDAO = OfferDAOImpl.getInstance();
        return offerDAO.get(offerDTO.getId()).orElse(null);
    }
}
