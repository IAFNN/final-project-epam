package com.my.mappers;

import com.my.DAO.EventDAO;
import com.my.DTO.AbstractUserDTO;
import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.entity.AbstractUser;
import com.my.entity.Event;
import com.my.entity.Report;
import com.my.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to convert EventDTO to Event entity and vice versa. Requires initialization.
 */
public class EventMapper {
    private static EventDAO eventDAO;
    public static EventDTO toDTO(Event event){
        List<Report> reports = eventDAO.getReportsByEvent(event);
        List<ReportDTO> reportDTOS = new ArrayList<>();
        for(Report report : reports){
            reportDTOS.add(ReportMapper.toDTO(report));
        }
        List<AbstractUser> usersRegistered = eventDAO.getUsersByEvent(event);
        List<AbstractUserDTO> usersRegisteredDTOS = new ArrayList<>();
        for(AbstractUser abstractUser : usersRegistered){
            usersRegisteredDTOS.add(UserMapper.toDTO(abstractUser));
        }
        List<AbstractUser> usersAttended = eventDAO.getUsersAttendedEvent(event);
        List<AbstractUserDTO> usersAttendedDTOS = new ArrayList<>();
        for(AbstractUser abstractUser : usersAttended){
            usersAttendedDTOS.add(UserMapper.toDTO(abstractUser));
        }
        return new EventDTO(event.getId(), event.getName(), Utils.convertInstantToString(event.getDate()), event.getLocation(),
                usersRegisteredDTOS, usersAttendedDTOS, reportDTOS);
    }
    public static Event toEntity(EventDTO eventDTO){
        return eventDAO.get(eventDTO.getId()).orElse(null);
    }
    public static void init(EventDAO eventDAO){
        EventMapper.eventDAO = eventDAO;
    }
}
