package com.my.mappers;

import com.my.DAO.EventDAO;
import com.my.DAO.ReportDAO;
import com.my.DAO.impl.EventDAOImpl;
import com.my.DAO.impl.ReportDAOImpl;
import com.my.DTO.ReportDTO;
import com.my.entity.Event;
import com.my.entity.Report;
/**
 * Used to convert ReportDTO to Report entity and vice versa. Requires initialization.
 */
public class ReportMapper {
    private static EventDAO eventDAO;
    private static ReportDAO reportDAO;
    public static void init(EventDAO eventDAO, ReportDAO reportDAO){
        ReportMapper.eventDAO = eventDAO;
        ReportMapper.reportDAO = reportDAO;
    }
    public static ReportDTO toDTO(Report report){
        Event event = eventDAO.getEventByReport(report).orElse(null);
        int eventID;
        String eventName;
        String speakerName;
        String speakerLogin;
        if(report.getSpeaker() == null){
            speakerLogin = null;
            speakerName = null;
        }else{
            speakerLogin = report.getSpeaker().getLogin();
            speakerName = report.getSpeaker().getName();
        }
        if(event == null) {
            eventID = -1;
            eventName = null;
        }else{
            eventID = event.getId();
            eventName = event.getName();
        }
        return new ReportDTO(report.getId(), report.getTopic().getId(), report.getTopic().getName(), speakerLogin,
                speakerName, eventID, eventName);
    }
    public static Report toEntity(ReportDTO reportDTO){
        return reportDAO.get(reportDTO.getId()).orElse(null);
    }
}
