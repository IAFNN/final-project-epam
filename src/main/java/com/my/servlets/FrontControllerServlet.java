package com.my.servlets;

import com.my.commands.FrontCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


import java.io.IOException;

/**
 * Main entry point to program via web application
 */
@WebServlet(name = "frontControllerServlet", value = "/frontControllerServlet")
public class FrontControllerServlet extends HttpServlet {
    private static Logger logger = LogManager.getLogger(FrontControllerServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FrontCommand command = getCommand(req);
        command.init(getServletContext(), req, resp);
        command.process();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FrontCommand command = getCommand(req);
        command.init(getServletContext(), req, resp);
        command.process();
    }

    /**
     * Dynamically loads command class specified by command parameter in request.
     * Logs all commands and user's logins who called for that commands.
     * @param request HttpServletRequest with command parameter
     * @return command which will process request
     */
    private FrontCommand getCommand(HttpServletRequest request) {
        try {
            logger.info(String.format("User %s called %s command", request.getSession().getAttribute("login"), request.getParameter("command")));
            Class<?> type = Class.forName(String.format("com.my.commands.%sCommand", request.getParameter("command")));
            return type.asSubclass(FrontCommand.class).getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}