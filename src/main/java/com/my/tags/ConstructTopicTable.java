package com.my.tags;

import com.my.DTO.TopicDTO;
import com.my.tags.topic.table.*;
import com.my.util.WebUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
/**
 * Tag used to build topic table. Attributes are:<br>
 * submits - all buttons with 'submit' type<br>
 * buttons - all buttons that activate some js code once clicked<br>
 * locale - locale on which all text will be displayed<br>
 * topics - list of topics to display<br>
 * displayId - should topic id, event id and buttons be displayed<br>
 */
public class ConstructTopicTable extends SimpleTagSupport {
    public void setDisplayID(boolean displayID) {
        this.displayID = displayID;
    }

    private boolean displayID;
    private List<TopicDTO> topics;
    private HashMap<String, String> submits;
    private HashMap<String, String> buttons;

    public void setLocale(String locale) {
        this.locale = locale;
    }

    private String locale;
    @Override
    public void doTag() throws JspException, IOException {
        JspWriter writer = getJspContext().getOut();
        String constructedTable;
        TopicTable table = new TopicTableImpl();
        if(displayID){
            table = new IDTopicTableDecorator(table);
        }
        table = new NameTopicTableDecorator(table);
        if(displayID){
            table = new EventIDTopicTableDecorator(table);
        }
        table = new EventNameTopicTableDecorator(table);
        if(displayID){
            table = new ButtonsTopicTableDecorator(table);
        }
        constructedTable = WebUtils.constructTopicTable(locale, topics, submits, buttons, table).toString();
        writer.append(constructedTable);
    }

    public void setTopics(List<TopicDTO> topics) {
        this.topics = topics;
    }

    public void setSubmits(HashMap<String, String> submits) {
        this.submits = submits;
    }

    public void setButtons(HashMap<String, String> buttons) {
        this.buttons = buttons;
    }
}
