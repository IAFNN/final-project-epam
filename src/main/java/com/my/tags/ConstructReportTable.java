package com.my.tags;

import com.my.DTO.ReportDTO;
import com.my.tags.report.table.*;
import com.my.util.WebUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
/**
 * Tag used to build report table. Attributes are:<br>
 * submits - all buttons with 'submit' type<br>
 * buttons - all buttons that activate some js code once clicked<br>
 * locale - locale on which all text will be displayed<br>
 * reports - list of reports to display<br>
 * displayId - should report id, topic id, event id and buttons be displayed<br>
 * displayLogin - should user logins be displayed<br>
 */
public class ConstructReportTable extends SimpleTagSupport {
    private boolean displayId;

    public void setLocale(String locale) {
        this.locale = locale;
    }

    private String locale;


    public void setReports(List<ReportDTO> reports) {
        this.reports = reports;
    }

    private List<ReportDTO> reports;

    public void setDisplayId(boolean displayId) {
        this.displayId = displayId;
    }

    private boolean displayLogin;

    private HashMap<String, String> submits;
    private HashMap<String, String> buttons;
    public void setDisplayLogin(boolean displayLogin) {
        this.displayLogin = displayLogin;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter writer = getJspContext().getOut();
        String constructedTable;
        ReportTable reportTable = new ReportTableImpl();
        if(displayId){
            reportTable = new IDReportTableDecorator(reportTable);
            reportTable = new TopicIDReportTableDecorator(reportTable);
        }
        reportTable = new TopicNameReportTableDecorator(reportTable);
        if(displayLogin){
            reportTable = new SpeakerLoginReportTableDecorator(reportTable);
        }
        reportTable = new SpeakerNameReportTableDecorator(reportTable);
        if(displayId){
            reportTable = new EventIDReportTableDecorator(reportTable);
        }
        reportTable = new EventNameReportTableDecorator(reportTable);
        if(displayId){
            reportTable = new ButtonsReportTableDecorator(reportTable);
        }
        constructedTable = WebUtils.constructReportTable(locale, reports, submits, buttons, reportTable).toString();
        writer.append(constructedTable);
    }

    public void setSubmits(HashMap<String, String> submits) {
        this.submits = submits;
    }

    public void setButtons(HashMap<String, String> buttons) {
        this.buttons = buttons;
    }
}
