package com.my.tags;

import com.my.DTO.EventDTO;
import com.my.tags.event.table.*;
import com.my.util.WebUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Tag used to build event table. Attributes are:<br>
 * submits - all buttons with 'submit' type<br>
 * buttons - all buttons that activate some js code once clicked<br>
 * locale - locale on which all text will be displayed<br>
 * events - list of events to display<br>
 * displayId - should event id, event information about users and reports and buttons be displayed<br>
 * displayStatistics - should information about number of users attended event be displayed<br>
 */
public class ConstructEventTable extends SimpleTagSupport {
    private boolean displayId;
    private HashMap<String, String> submits;
    private HashMap<String, String> buttons;

    public void setLocale(String locale) {
        this.locale = locale;
    }

    private String locale;

    public void setEvents(List<EventDTO> events) {
        this.events = events;
    }

    private List<EventDTO> events;

    public void setDisplayStatistics(boolean displayStatistics) {
        this.displayStatistics = displayStatistics;
    }

    private boolean displayStatistics;

    public void setDisplayId(boolean displayId) {
        this.displayId = displayId;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter writer = getJspContext().getOut();
        String constructedTable;
        EventTable eventTable = new EventTableImpl();
        if(displayId){
            eventTable = new IDEventTableDecorator(eventTable);
        }
        eventTable = new NameEventTableDecorator(eventTable);
        eventTable = new TimeEventTableDecorator(eventTable);
        eventTable = new LocationEventTableDecorator(eventTable);
        if(displayId) {
            eventTable = new NumberOfUsersRegisteredEventTableDecorator(eventTable);
            if(displayStatistics){
                eventTable = new NumberOfUsersAttendedEventTableDecorator(eventTable);
            }
            eventTable = new NumberOfReportsEventTableDecorator(eventTable);
            eventTable = new ButtonsEventTableDecorator(eventTable);
        }
        constructedTable = WebUtils.constructEventTable(locale, events, submits, buttons, eventTable).toString();
        writer.append(constructedTable);
    }

    public void setSubmits(HashMap<String, String> submits) {
        this.submits = submits;
    }

    public void setButtons(HashMap<String, String> buttons) {
        this.buttons = buttons;
    }
}
