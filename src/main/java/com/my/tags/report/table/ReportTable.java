package com.my.tags.report.table;

import java.util.List;
/**
 * Base class for setting up report table. Order of columns in list determine how columns will be displayed.
 */
public abstract class ReportTable {
    protected List<ReportTableColumns> columns;
    public abstract List<ReportTableColumns> setupTable();
}
