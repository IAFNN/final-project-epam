package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with speaker name column
 */
public class SpeakerNameReportTableDecorator extends ReportTableDecorator {
    public SpeakerNameReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.SPEAKER_NAME);
        return list;
    }
}
