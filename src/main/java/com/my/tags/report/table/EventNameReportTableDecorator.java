package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with event name column
 */
public class EventNameReportTableDecorator extends ReportTableDecorator {
    public EventNameReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.EVENT_NAME);
        return list;
    }
}
