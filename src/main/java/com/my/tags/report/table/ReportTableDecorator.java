package com.my.tags.report.table;

import java.util.List;
/**
 * Base class for decorating report table
 */
public abstract class ReportTableDecorator extends ReportTable {
    private ReportTable table;
    public ReportTableDecorator(ReportTable table){
        this.table = table;
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        return table.setupTable();
    }
}
