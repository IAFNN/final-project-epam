package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with buttons
 */
public class ButtonsReportTableDecorator extends ReportTableDecorator {
    public ButtonsReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.BUTTONS);
        return list;
    }
}
