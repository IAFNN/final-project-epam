package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with speaker login column
 */
public class SpeakerLoginReportTableDecorator extends ReportTableDecorator {
    public SpeakerLoginReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.SPEAKER_LOGIN);
        return list;
    }
}
