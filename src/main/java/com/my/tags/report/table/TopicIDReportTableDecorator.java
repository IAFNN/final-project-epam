package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with topic id column
 */
public class TopicIDReportTableDecorator extends ReportTableDecorator {
    public TopicIDReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.TOPIC_ID);
        return list;
    }
}
