package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with id column
 */
public class IDReportTableDecorator extends ReportTableDecorator {
    public IDReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.ID);
        return list;
    }
}
