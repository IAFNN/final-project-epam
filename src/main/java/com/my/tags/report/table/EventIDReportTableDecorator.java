package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with event id column
 */
public class EventIDReportTableDecorator extends ReportTableDecorator {
    public EventIDReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.EVENT_ID);
        return list;
    }
}
