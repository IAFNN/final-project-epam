package com.my.tags.report.table;

import java.util.List;
/**
 * Decorates report table with topic name column
 */
public class TopicNameReportTableDecorator extends ReportTableDecorator {
    public TopicNameReportTableDecorator(ReportTable table){
        super(table);
    }

    @Override
    public List<ReportTableColumns> setupTable() {
        List<ReportTableColumns> list = super.setupTable();
        list.add(ReportTableColumns.TOPIC_NAME);
        return list;
    }
}
