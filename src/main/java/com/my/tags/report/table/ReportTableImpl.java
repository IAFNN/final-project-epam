package com.my.tags.report.table;

import java.util.ArrayList;
import java.util.List;
/**
 * List of base columns for report table
 */
public class ReportTableImpl extends ReportTable {
    @Override
    public List<ReportTableColumns> setupTable() {
        return new ArrayList<>();
    }
}
