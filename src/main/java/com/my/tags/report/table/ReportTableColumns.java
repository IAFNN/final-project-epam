package com.my.tags.report.table;
/**
 * All possible columns in report table
 */
public enum ReportTableColumns {
    ID, TOPIC_NAME, TOPIC_ID, SPEAKER_NAME, SPEAKER_LOGIN, BUTTONS, EVENT_NAME, EVENT_ID
}
