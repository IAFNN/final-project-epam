package com.my.tags.topic.table;

import java.util.List;
/**
 * Base class for decorating topic table
 */
public abstract class TopicTableDecorator extends TopicTable{
    private TopicTable table;
    public TopicTableDecorator(TopicTable table){
        this.table = table;
    }

    @Override
    public List<TopicTableColumns> setupTable() {
        return table.setupTable();
    }
}
