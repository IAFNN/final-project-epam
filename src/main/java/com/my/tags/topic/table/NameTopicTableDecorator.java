package com.my.tags.topic.table;

import java.util.List;
/**
 * Decorates topic table with name column
 */
public class NameTopicTableDecorator extends TopicTableDecorator{
    public NameTopicTableDecorator(TopicTable table){
        super(table);
    }

    @Override
    public List<TopicTableColumns> setupTable() {
        List<TopicTableColumns> list = super.setupTable();
        list.add(TopicTableColumns.NAME);
        return list;
    }
}
