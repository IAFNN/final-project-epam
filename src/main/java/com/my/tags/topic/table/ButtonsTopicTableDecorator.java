package com.my.tags.topic.table;

import java.util.List;
/**
 * Decorates topic table with buttons
 */
public class ButtonsTopicTableDecorator extends TopicTableDecorator{
    public ButtonsTopicTableDecorator(TopicTable table){
        super(table);
    }

    @Override
    public List<TopicTableColumns> setupTable() {
        List<TopicTableColumns> list = super.setupTable();
        list.add(TopicTableColumns.BUTTONS);
        return list;
    }
}
