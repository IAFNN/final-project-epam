package com.my.tags.topic.table;

import java.util.List;
/**
 * Decorates topic table with event name column
 */
public class EventNameTopicTableDecorator extends TopicTableDecorator{
    public EventNameTopicTableDecorator(TopicTable table){
        super(table);
    }

    @Override
    public List<TopicTableColumns> setupTable() {
        List<TopicTableColumns> list = super.setupTable();
        list.add(TopicTableColumns.EVENT_NAME);
        return list;
    }
}
