package com.my.tags.topic.table;

import java.util.List;
/**
 * Decorates topic table with event id column
 */
public class EventIDTopicTableDecorator extends TopicTableDecorator{
    public EventIDTopicTableDecorator(TopicTable table){
        super(table);
    }

    @Override
    public List<TopicTableColumns> setupTable() {
        List<TopicTableColumns> list = super.setupTable();
        list.add(TopicTableColumns.EVENT_ID);
        return list;
    }
}
