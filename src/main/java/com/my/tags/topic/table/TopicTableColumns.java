package com.my.tags.topic.table;
/**
 * All possible columns in topic table
 */
public enum TopicTableColumns {
    ID, NAME, EVENT_ID, EVENT_NAME, BUTTONS
}
