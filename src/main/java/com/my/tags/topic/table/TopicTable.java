package com.my.tags.topic.table;

import java.util.List;
/**
 * Base class for setting up topic table. Order of columns in list determine how columns will be displayed.
 */
public abstract class TopicTable {
    protected List<TopicTableColumns> columns;
    public abstract List<TopicTableColumns> setupTable();
}
