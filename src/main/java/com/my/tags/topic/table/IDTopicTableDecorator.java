package com.my.tags.topic.table;

import java.util.List;
/**
 * Decorates topic table with id column
 */
public class IDTopicTableDecorator extends TopicTableDecorator{
    public IDTopicTableDecorator(TopicTable table){
        super(table);
    }

    @Override
    public List<TopicTableColumns> setupTable() {
        List<TopicTableColumns> list = super.setupTable();
        list.add(TopicTableColumns.ID);
        return list;
    }
}
