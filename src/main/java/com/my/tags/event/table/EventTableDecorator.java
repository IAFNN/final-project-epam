package com.my.tags.event.table;

import java.util.List;

/**
 * Base class for decorating event table
 */
public abstract class EventTableDecorator extends EventTable {
    private EventTable table;
    public EventTableDecorator(EventTable table){
        this.table = table;
    }

    @Override
    public List<EventTableColumns> setupTable() {
        return table.setupTable();
    }
}
