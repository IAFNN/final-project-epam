package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with number of reports for event column
 */
public class NumberOfReportsEventTableDecorator extends EventTableDecorator {
    public NumberOfReportsEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.NUMBER_OF_REPORTS);
        return list;
    }
}
