package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with id column
 */
public class IDEventTableDecorator extends EventTableDecorator {
    public IDEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.ID);
        return list;
    }
}
