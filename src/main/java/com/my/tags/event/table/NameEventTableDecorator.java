package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with name column
 */
public class NameEventTableDecorator extends EventTableDecorator {
    public NameEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.NAME);
        return list;
    }
}
