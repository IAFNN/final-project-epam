package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with number of users attended event column
 */
public class NumberOfUsersAttendedEventTableDecorator extends EventTableDecorator {
    public NumberOfUsersAttendedEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.NUMBER_OF_USERS_ATTENDED);
        return list;
    }
}
