package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with location column
 */
public class LocationEventTableDecorator extends EventTableDecorator {
    public LocationEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.LOCATION);
        return list;
    }
}
