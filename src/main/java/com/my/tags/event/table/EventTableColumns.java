package com.my.tags.event.table;

/**
 * All possible columns in event table
 */
public enum EventTableColumns {
    ID, NAME, TIME, LOCATION, NUMBER_OF_REGISTERED_USERS, NUMBER_OF_USERS_ATTENDED, NUMBER_OF_REPORTS, BUTTONS
}
