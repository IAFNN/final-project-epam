package com.my.tags.event.table;

import java.util.List;

/**
 * Base class for setting up event table. Order of columns in list determine how columns will be displayed.
 */
public abstract class EventTable {
    protected List<EventTableColumns> columns;
    public abstract List<EventTableColumns> setupTable();
}
