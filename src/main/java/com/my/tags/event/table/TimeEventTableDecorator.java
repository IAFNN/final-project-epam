package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with time column
 */
public class TimeEventTableDecorator extends EventTableDecorator {
    public TimeEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.TIME);
        return list;
    }
}
