package com.my.tags.event.table;

import java.util.ArrayList;
import java.util.List;

/**
 * List of base columns for event table
 */
public class EventTableImpl extends EventTable {
    @Override
    public List<EventTableColumns> setupTable() {
        return new ArrayList<>();
    }
}
