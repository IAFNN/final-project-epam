package com.my.tags.event.table;

import java.util.List;
/**
 * Decorates event table with number of users registered for event column
 */
public class NumberOfUsersRegisteredEventTableDecorator extends EventTableDecorator {
    public NumberOfUsersRegisteredEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.NUMBER_OF_REGISTERED_USERS);
        return list;
    }
}
