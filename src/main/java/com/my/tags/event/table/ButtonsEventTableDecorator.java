package com.my.tags.event.table;

import java.util.List;

/**
 * Decorates event table with buttons
 */
public class ButtonsEventTableDecorator extends EventTableDecorator {
    public ButtonsEventTableDecorator(EventTable table){
        super(table);
    }

    @Override
    public List<EventTableColumns> setupTable() {
        List<EventTableColumns> list = super.setupTable();
        list.add(EventTableColumns.BUTTONS);
        return list;
    }
}
