package com.my.tags;

import com.my.DTO.OfferDTO;
import com.my.util.WebUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
/**
 * Tag used to build offer table. Attributes are:<br>
 * submits - all buttons with 'submit' type<br>
 * buttons - all buttons that activate some js code once clicked<br>
 * locale - locale on which all text will be displayed<br>
 * offerList - list of offers to display<br>
 */
public class ConstructOffersTable extends SimpleTagSupport {
    private String locale;
    private List<OfferDTO> offerList;
    private HashMap<String, String> submits;
    private HashMap<String, String> buttons;
    @Override
    public void doTag() throws JspException, IOException {
        JspWriter writer = getJspContext().getOut();
        String constructedTable;
        constructedTable = WebUtils.constructOffersTable(locale, offerList, submits, buttons).toString();
        writer.append(constructedTable);
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public void setOfferList(List<OfferDTO> offerList) {
        this.offerList = offerList;
    }

    public void setSubmits(HashMap<String, String> submits) {
        this.submits = submits;
    }

    public void setButtons(HashMap<String, String> buttons) {
        this.buttons = buttons;
    }
}
