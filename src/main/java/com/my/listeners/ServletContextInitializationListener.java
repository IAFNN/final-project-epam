package com.my.listeners;

import com.my.DAO.HikariCPDataSource;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.my.DAO.impl.*;
import com.my.mappers.EventMapper;
import com.my.mappers.ReportMapper;
import com.my.mappers.TopicMapper;
import com.my.mappers.UserMapper;
import com.my.service.*;
import com.my.util.MailUtil;
import com.my.util.WebUtils;

/**
 * Used for initializing
 */
@WebListener
public class ServletContextInitializationListener implements ServletContextListener {
    /**
     * Method is called on application startup.
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being initialized
     *
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        HikariCPDataSource.init(sce.getServletContext().getRealPath(""));
        WebUtils.init(sce.getServletContext().getRealPath(""));
        MailUtil.init(sce.getServletContext().getRealPath(""));
        EventService.init(AbstractUserDAOImpl.getInstance(), EventDAOImpl.getInstance());
        OfferService.init(AbstractUserDAOImpl.getInstance(), EventDAOImpl.getInstance(), OfferDAOImpl.getInstance(),
                ReportDAOImpl.getInstance(), TopicDAOImpl.getInstance());
        ReportService.init(AbstractUserDAOImpl.getInstance(), ReportDAOImpl.getInstance(), TopicDAOImpl.getInstance());
        TopicService.init(TopicDAOImpl.getInstance());
        UserService.init(AbstractUserDAOImpl.getInstance());
        ReportMapper.init(EventDAOImpl.getInstance(), ReportDAOImpl.getInstance());
        EventMapper.init(EventDAOImpl.getInstance());
        TopicMapper.init(EventDAOImpl.getInstance(), TopicDAOImpl.getInstance());
        UserMapper.init(AbstractUserDAOImpl.getInstance());
    }
}
