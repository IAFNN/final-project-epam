package com.my.exception;

/**
 * Represents problem in data entered by user
 */
public class IncorrectDataException extends Exception{
    public IncorrectDataException(String message){
        this.message = message;
    }
    private String message;

    @Override
    public String getMessage() {
        return message;
    }
    public IncorrectDataException(){}
}
