package com.my.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Checks if user chosen to remember him/her during login, if yes automatically authorizes and redirects to start page
 */
@WebFilter(urlPatterns = "/index.jsp")
public class AuthorizedFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (((HttpServletRequest) request).getSession().getAttribute("autoAuthorize") != null) {
            ((HttpServletResponse) response).sendRedirect("frontControllerServlet?command=GetStartPage");
        }
        chain.doFilter(request, response);
    }
}
