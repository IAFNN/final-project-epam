package com.my.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.my.entity.UserType;

import java.io.IOException;
import java.util.Set;

/**
 * Checks if user is accessing available to his user type command, if no redirects to start page.
 * If user is not authorized redirects to login page.
 * List of allowed commands for user type can be changed in List constants in code.
 */
@WebFilter(urlPatterns = "/*")
public class SecurityFilter implements Filter {
    /**
     * List of allowed commands for 'user' user type
     */
    private static final Set<String> USER_ALLOWED_COMMANDS = Set.of("ChangeLocale", "ChangePageFutureEvents", "ChangePagePastEvents", "ChangePageReports",
            "ChangePageTopics", "FutureEventDetails", "Join", "Login", "PastEventDetails", "Register", "ReportDetails", "SignOut", "TopicDetails", "GetStartPage");
    /**
     * List of allowed commands for 'speaker' user type
     */
    private static final Set<String> SPEAKER_ALLOWED_COMMANDS = Set.of("ChangeLocale", "ChangePageFutureEvents", "ChangePagePastEvents", "ChangePageReports",
            "ChangePageTopics", "FutureEventDetails", "Login", "PastEventDetails", "Register", "ReportDetails", "SignOut", "TopicDetails", "AcceptOffer",
            "DeclineOffer", "OfferCreateReport", "OfferJoinReport", "OffersDetails", "GetStartPage");
    /**
     * List of allowed commands for 'moderator' user type
     */
    private static final Set<String> MODERATOR_ALLOWED_COMMANDS = Set.of("ChangeLocale", "ChangePageFutureEvents", "ChangePagePastEvents", "ChangePageReports",
            "ChangePageTopics", "FutureEventDetails", "Login", "PastEventDetails", "Register", "ReportDetails", "SignOut", "TopicDetails", "AcceptOffer",
            "DeclineOffer", "OfferChangeTopic", "OffersDetails", "Create", "CreateTopic", "Delete", "Edit", "RegisterAttendance", "GetStartPage",
            "CreateReport");
    /**
     * List of allowed commands for unauthorized users
     */
    private static final Set<String> UNAUTHORIZED_ALLOWED_COMMAND = Set.of("Login", "Register", "ChangeLocale");
    /**
     * List of allowed paths for everyone
     */
    private static final Set<String> ALLOWED_PATHS = Set.of("", "/index.jsp", "/register.jsp", "/styles/inputStyle.css", "/styles/style.css", "/scripts/validation.js");

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        if(ALLOWED_PATHS.contains(servletRequest.getRequestURI().substring(servletRequest.getContextPath().length()).replaceAll("[/]+$", ""))){
            chain.doFilter(request, response);
        }else{
            String command = String.valueOf(servletRequest.getParameter("command"));
            if(servletRequest.getSession().getAttribute("userType") == null){
                if(!UNAUTHORIZED_ALLOWED_COMMAND.contains(command)) {
                    ((HttpServletResponse) response).sendRedirect("index.jsp");
                }else{
                    chain.doFilter(request, response);
                }
            }else {
                UserType userType = UserType.valueOf(String.valueOf(servletRequest.getSession().getAttribute("userType")).toUpperCase());
                if(command.equals("null")){
                    chain.doFilter(request, response);
                    return;
                }
                switch (userType){
                    case USER:
                        if(!USER_ALLOWED_COMMANDS.contains(command)){
                            ((HttpServletResponse) response).sendRedirect("start.jsp");
                        }else{
                            chain.doFilter(request, response);
                        }
                        break;
                    case SPEAKER:
                        if(!SPEAKER_ALLOWED_COMMANDS.contains(command)){
                            ((HttpServletResponse) response).sendRedirect("start.jsp");
                        }else{
                            chain.doFilter(request, response);
                        }
                        break;
                    case MODERATOR:
                        if(!MODERATOR_ALLOWED_COMMANDS.contains(command)){
                            ((HttpServletResponse) response).sendRedirect("start.jsp");
                        }else{
                            chain.doFilter(request, response);
                        }
                }
            }
        }
    }
}