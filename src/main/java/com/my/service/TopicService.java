package com.my.service;

import com.my.DAO.TopicDAO;
import com.my.DTO.TopicDTO;
import com.my.entity.Topic;
import com.my.exception.IncorrectDataException;
import com.my.mappers.TopicMapper;

import java.util.ArrayList;
import java.util.List;
/**
 * Used to manipulate data using DAO. Requires initialization.
 */
public class TopicService {
    private static TopicDAO topicDAO;
    /**
     * Service initialization
     * @param topicDAO TopicDAO implementation
     */
    public static void init(TopicDAO topicDAO){
        TopicService.topicDAO = topicDAO;
    }
    /**
     * Used to get topic specified by id parameter
     * @param id id of topic to find
     * @return TopicDTO specified by id
     * @throws IncorrectDataException if topic with this id does not exist
     */
    public static TopicDTO get(int id) throws IncorrectDataException {
        try {
            return TopicMapper.toDTO(topicDAO.get(id).orElse(null));
        }catch (NullPointerException e){
            throw new IncorrectDataException();
        }
    }
    /**
     * Used to get list of all topics
     * @return List of all topics
     */
    public static List<TopicDTO> getAll(){
        List<Topic> topics = topicDAO.getAll();
        List<TopicDTO> result = new ArrayList<>();
        topics.forEach(user -> result.add(TopicMapper.toDTO(user)));
        return result;
    }
    /**
     * Used to save topic specified by given parameters
     * @param name name of topic
     */
    public static TopicDTO save(String name){
        Topic topic = new Topic(name);
        topicDAO.save(topic);
        return TopicMapper.toDTO(topic);
    }
    /**
     * Used to update information about topic specified by id parameter
     * @param id id of topic to be updated
     * @param name new name of topic
     */
    public static void update(int id, String name){
        Topic topic = topicDAO.get(id).orElse(null);
        if(topic == null){
            return;
        }
        topicDAO.update(topic, new String[]{name});
    }
    /**
     * Used to delete topic specified by id
     * @param id id of topic to be deleted
     */
    public static void delete(int id){
        Topic topic = topicDAO.get(id).orElse(null);
        if(topic == null){
            return;
        }
        topicDAO.delete(topic);
    }
    /**
     * Used to get List with limited number of topics specified by offset and limit parameters
     * @param offset number of topics to be skipped
     * @param limit number of topics to retrieve
     * @param result List where retrieved topics will be stored
     * @return number of all topics
     */
    public static int getAll(int offset, int limit, List<TopicDTO> result){
        List<Topic> topics = new ArrayList<>();
        int resultCount = topicDAO.getAll(offset, limit, topics);
        topics.forEach(user -> result.add(TopicMapper.toDTO(user)));
        return resultCount;
    }
}
