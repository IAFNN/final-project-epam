package com.my.service;

import com.my.DAO.*;
import com.my.DAO.impl.*;
import com.my.DTO.OfferDTO;
import com.my.DTO.TopicDTO;
import com.my.entity.*;
import com.my.exception.IncorrectDataException;
import com.my.mappers.OfferMapper;
import java.util.ArrayList;
import java.util.List;
/**
 * Used to manipulate data using DAO. Requires initialization.
 */
public class OfferService {
    private static AbstractUserDAO userDAO;
    private static EventDAO eventDAO;
    private static OfferDAO offerDAO;
    private static ReportDAO reportDAO;
    private static TopicDAO topicDAO;
    /**
     * Service initialization
     * @param userDAO UserDAO implementation
     * @param eventDAO EventDAO implementation
     * @param offerDAO OfferDAO implementation
     * @param reportDAO ReportDAO implementation
     * @param topicDAO TopicDAO implementation
     */
    public static void init(AbstractUserDAO userDAO, EventDAO eventDAO, OfferDAO offerDAO, ReportDAO reportDAO, TopicDAO topicDAO){
        OfferService.userDAO = userDAO;
        OfferService.eventDAO = eventDAO;
        OfferService.offerDAO = offerDAO;
        OfferService.reportDAO = reportDAO;
        OfferService.topicDAO = topicDAO;
    }
    /**
     * Used to get offer specified by id parameter
     * @param id id of offer to find
     * @return OfferDTO specified by id
     * @throws IncorrectDataException if offer with this id does not exist
     */
    public static OfferDTO get(int id) throws IncorrectDataException {
        try {
            return OfferMapper.toDTO(offerDAO.get(id).orElse(null));
        }catch (NullPointerException e){
            throw new IncorrectDataException();
        }
    }
    /**
     * Used to get list of all offers
     * @return List of all offers
     */
    public static List<OfferDTO> getAll(){
        List<Offer> offers = offerDAO.getAll();
        List<OfferDTO> result = new ArrayList<>();
        offers.forEach(user -> result.add(OfferMapper.toDTO(user)));
        return result;
    }
    /**
     * Used to save offer specified by given parameters
     * @param targetUserType offer target user type
     * @param offerType offer type
     * @param topicID id of topic
     * @param speakerLogin login of speaker
     * @param eventID id of event
     * @param reportID id of report
     */
    public static void save(UserType targetUserType, String offerType, int topicID, String speakerLogin, int eventID, int reportID) throws IncorrectDataException {
        OfferFunction offerFunction;
        if(offerType.equalsIgnoreCase("create")){
            try {
                TopicService.get(topicID);
                UserService.get(speakerLogin);
                EventService.get(eventID);
            }catch (IncorrectDataException e){
                throw new IncorrectDataException("1");
            }
            offerFunction = new OfferFunction() {
                @Override
                public void doRequest() {
                    Event event = eventDAO.get(eventID).orElse(null);
                    Report report = new Report(topicDAO.get(topicID).orElse(null), userDAO.get(speakerLogin).orElse(null));
                    reportDAO.save(report);
                    eventDAO.setReportToEvent(report, event);
                }

                @Override
                public int getTopicID() {
                    return topicID;
                }

                @Override
                public String getSpeakerLogin() {
                    return speakerLogin;
                }

                @Override
                public int getEventID() {
                    return eventID;
                }
            };
        }else{
            if(targetUserType == UserType.MODERATOR){
                try {
                    ReportService.get(reportID);
                    UserService.get(speakerLogin);
                }catch (IncorrectDataException e){
                    throw new IncorrectDataException("2");
                }
                offerFunction = new OfferFunction() {
                    @Override
                    public void doRequest() {
                        Report report = reportDAO.get(reportID).get();
                        reportDAO.update(report, new String[]{speakerLogin, String.valueOf(report.getTopic().getId())});
                    }

                    @Override
                    public int getReportID() {
                        return reportID;
                    }

                    @Override
                    public String getSpeakerLogin() {
                        return speakerLogin;
                    }
                };
            }else{
                try {
                    TopicService.get(topicID);
                    ReportService.get(reportID);
                }catch (IncorrectDataException e){
                    throw new IncorrectDataException("3");
                }
                offerFunction = new OfferFunction() {
                    @Override
                    public void doRequest() {
                        Report report = reportDAO.get(reportID).get();
                        reportDAO.update(report, new String[]{report.getSpeaker().getLogin(), String.valueOf(topicID)});
                    }

                    @Override
                    public int getTopicID() {
                        return topicID;
                    }

                    @Override
                    public int getReportID() {
                        return reportID;
                    }
                };
            }
        }
        Offer offer = new Offer(targetUserType, offerType, offerFunction);
        offerDAO.save(offer);
    }

    /**
     * Unsupported operation
     */
    public static void update(int id){
        throw new UnsupportedOperationException();
    }
    /**
     * Used to delete offer specified by id
     * @param id id of offer to be deleted
     * @throws IncorrectDataException if offer with this id doesn't exist
     */
    public static void delete(int id) throws IncorrectDataException {
        Offer offer = offerDAO.get(id).orElse(null);
        if(offer == null){
            throw new IncorrectDataException();
        }
        offerDAO.delete(offer);
    }
    /**
     * Used to get List with limited number of offers specified by offset and limit parameters
     * @param offset number of offers to be skipped
     * @param limit number of offers to retrieve
     * @param result List where retrieved offers will be stored
     * @return number of all offers
     */
    public static int getAll(int offset, int limit, List<OfferDTO> result){
        List<Offer> offers = new ArrayList<>();
        int resultCount = offerDAO.getAll(offset, limit, offers);
        offers.forEach(user -> result.add(OfferMapper.toDTO(user)));
        return resultCount;
    }

    /**
     * Used to accept and execute offer specified by offerID parameter
     * @param offerID id of offer to be executed
     * @throws IncorrectDataException if offer with this id doesn't exist
     */
    public static void doRequest(int offerID) throws IncorrectDataException {
        Offer offer = offerDAO.get(offerID).orElse(null);
        if(offer == null){
            throw new IncorrectDataException();
        }
        offer.getOfferFunction().doRequest();
        offerDAO.delete(offer);
    }
}
