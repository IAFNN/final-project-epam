package com.my.service;

import com.my.DAO.AbstractUserDAO;
import com.my.DTO.AbstractUserDTO;
import com.my.entity.AbstractUser;
import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import com.my.mappers.UserMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to manipulate data using DAO. Requires initialization.
 */
public class UserService {
    private static AbstractUserDAO userDAO;
    /**
     * Service initialization
     * @param userDAO AbstractUserDAO implementation
     */
    public static void init(AbstractUserDAO userDAO){
        UserService.userDAO = userDAO;
    }
    /**
     * Used to get user specified by login parameter
     * @param login login of user to find
     * @return AbstractUserDTO specified by login
     * @throws IncorrectDataException if user with this login does not exist
     */
    public static AbstractUserDTO get(String login) throws IncorrectDataException {
        try {
            return UserMapper.toDTO(userDAO.get(login).orElse(null));
        }catch (NullPointerException e){
            throw new IncorrectDataException();
        }
    }
    /**
     * Used to get list of all users
     * @return List of all users
     */
    public static List<AbstractUserDTO> getAll(){
        List<AbstractUser> users = userDAO.getAll();
        List<AbstractUserDTO> result = new ArrayList<>();
        users.forEach(user -> result.add(UserMapper.toDTO(user)));
        return result;
    }
    /**
     * Used to save user specified by given parameters
     * @param login login of user
     * @param name name of user
     * @param password user password
     * @param userType user type of user
     * @param salt user's salt, used to hash password
     */
    public static void save(String login, String name, String password, UserType userType, String salt) throws IncorrectDataException {
        AbstractUser user = new AbstractUser(name, login, password, userType, salt);
        userDAO.save(user);
    }

    /**
     * Used to update information about user specified by login parameter
     * @param login login of user to be updated
     * @param name new name of topic
     * @param password new user password
     * @param userType new user type of user
     */
    public static void update(String login, String name, String password, String userType){
        AbstractUser user = userDAO.get(login).orElse(null);
        if(user == null){
            return;
        }
        if(name == null){
            name = user.getName();
        }
        if(password == null){
            password = user.getPassword();
        }
        if(userType == null){
            userType = user.getUserType().convertToDBEnum();
        }
        userDAO.update(user, new String[]{name, password, userType});
    }
    /**
     * Used to delete user specified by login
     * @param login login of user to be deleted
     */
    public static void delete(String login){
        AbstractUser user = userDAO.get(login).orElse(null);
        if(user == null){
            return;
        }
        userDAO.delete(user);
    }
    /**
     * Used to get List with limited number of users specified by offset and limit parameters
     * @param offset number of users to be skipped
     * @param limit number of users to retrieve
     * @param result List where retrieved users will be stored
     * @return number of all users
     */
    public static int getAll(int offset, int limit, List<AbstractUserDTO> result){
        List<AbstractUser> users = new ArrayList<>();
        int resultCount = userDAO.getAll(offset, limit, users);
        users.forEach(user -> result.add(UserMapper.toDTO(user)));
        return resultCount;
    }

    /**
     * Used to get hashed password of user
     * @param login specifies user whose password will be retrieved
     * @return hashed password
     */
    public static String getPassword(String login){
        AbstractUser user = userDAO.get(login).orElse(null);
        if(user == null){
            return null;
        }
        return user.getPassword();
    }

    /**
     * Used to get salt for hashing password
     * @param login specifies user whose salt will be retrieved
     * @return salt used to hash user's password
     */
    public static String getSalt(String login){
        AbstractUser user = userDAO.get(login).orElse(null);
        if(user == null){
            return null;
        }
        return user.getSalt();
    }
}
