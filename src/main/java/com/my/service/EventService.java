package com.my.service;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.EventDAO;
import com.my.DTO.AbstractUserDTO;
import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.DTO.TopicDTO;
import com.my.entity.AbstractUser;
import com.my.entity.Event;
import com.my.entity.Report;
import com.my.entity.Topic;
import com.my.exception.IncorrectDataException;
import com.my.mappers.EventMapper;
import com.my.mappers.ReportMapper;
import com.my.mappers.TopicMapper;
import com.my.mappers.UserMapper;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Used to manipulate data using DAO. Requires initialization.
 */
public class EventService {
    private static EventDAO eventDAO;
    private static AbstractUserDAO userDAO;

    /**
     * Used to get event specified by id parameter
     * @param id id of event to find
     * @return EventDTO specified by id
     * @throws IncorrectDataException if event with this id does not exist
     */
    public static EventDTO get(int id) throws IncorrectDataException {
        try {
            return EventMapper.toDTO(eventDAO.get(id).orElse(null));
        }catch (NullPointerException e){
            throw new IncorrectDataException();
        }
    }

    /**
     * Used to get list of all events
     * @return List of all events
     */
    public static List<EventDTO> getAll(){
        List<Event> events = eventDAO.getAll();
        List<EventDTO> result = new ArrayList<>();
        events.forEach(user -> result.add(EventMapper.toDTO(user)));
        return result;
    }

    /**
     * Used to save event specified by given parameters
     * @param name event name
     * @param time event time in format YYYY-MM-DDTHH:MM
     * @param location event location
     * @return EventDTO representing saved event
     */
    public static EventDTO save(String name, String time, String location){
        Instant timeInstant = Instant.parse(time + ":00Z");
        Event event = new Event(name, timeInstant, location);
        eventDAO.save(event);
        return EventMapper.toDTO(event);
    }

    /**
     * Used to update information about event specified by id parameter
     * @param id id of event to be updated
     * @param name new event name
     * @param time new event time in format YYYY-MM-DDTHH:MM:SS
     * @param location new event location
     * @throws IncorrectDataException if event with this id doesn't exist
     */
    public static void update(int id, String name, String time, String location) throws IncorrectDataException {
        Event event = eventDAO.get(id).orElse(null);
        if(event == null){
            throw new IncorrectDataException();
        }
        eventDAO.update(event, new String[]{name, time, location});
    }

    /**
     * Used to delete event specified by id
     * @param id id of event to be deleted
     * @throws IncorrectDataException if event with this id doesn't exist
     */
    public static void delete(int id) throws IncorrectDataException {
        Event event = eventDAO.get(id).orElse(null);
        if(event == null){
            throw new IncorrectDataException();
        }
        eventDAO.delete(event);
    }

    /**
     * Used to get List with limited number of events specified by offset and limit parameters
     * @param offset number of events to be skipped
     * @param limit number of events to retrieve
     * @param result List where retrieved events will be stored
     * @param pastEvents parameters which specifies if events should past or future
     * @return number of all past or future events
     */
    public static int getAll(int offset, int limit, List<EventDTO> result, boolean pastEvents){
        List<Event> events = new ArrayList<>();
        int resultCount;
        if(pastEvents) {
            resultCount = eventDAO.getAllPastEvents(offset, limit, events, null, false);
        }else{
            resultCount = eventDAO.getAllFutureEvents(offset, limit, events, null, false);
        }
        events.forEach(user -> result.add(EventMapper.toDTO(user)));
        return resultCount;
    }

    /**
     * Used to get List with limited number of events specified by offset and limit parameters sorted according to
     * sortBy and order parameters
     * @param offset number of events to be skipped
     * @param limit number of events to retrieve
     * @param result List where retrieved events will be stored
     * @param pastEvents parameters which specifies if events should past or future
     * @param sortBy parameter by which events should be sorted
     * @param order if true ascending order, otherwise descending
     * @return number of all past or future events
     */
    public static int getAll(int offset, int limit, List<EventDTO> result, boolean pastEvents, String sortBy, boolean order){
        List<Event> events = new ArrayList<>();
        int resultCount;
        if(pastEvents) {
            resultCount = eventDAO.getAllPastEvents(offset, limit, events, sortBy, order);
        }else{
            resultCount = eventDAO.getAllFutureEvents(offset, limit, events, sortBy, order);
        }
        events.forEach(user -> result.add(EventMapper.toDTO(user)));
        return resultCount;
    }
    public static EventDTO getEventByReport(ReportDTO reportDTO){
        Event event = eventDAO.getEventByReport(ReportMapper.toEntity(reportDTO)).orElse(null);
        if(event == null){
            return null;
        }
        return EventMapper.toDTO(event);
    }
    public static EventDTO getEventByTopic(TopicDTO topicDTO){
        Event event = eventDAO.getEventByTopic(TopicMapper.toEntity(topicDTO)).orElse(null);
        if(event == null){
            return null;
        }
        return EventMapper.toDTO(event);
    }
    public static void setEventToTopic(EventDTO eventDTO, TopicDTO topicDTO){
        Topic topic = TopicMapper.toEntity(topicDTO);
        eventDAO.setEventToTopic(EventMapper.toEntity(eventDTO), topic);
    }
    public static List<AbstractUserDTO> getUsersByEvent(EventDTO eventDTO){
        List<AbstractUser> users = eventDAO.getUsersByEvent(EventMapper.toEntity(eventDTO));
        List<AbstractUserDTO> result = new ArrayList<>();
        users.forEach(user -> result.add(UserMapper.toDTO(user)));
        return result;
    }
    public static List<AbstractUserDTO> getUsersAttendedEvent(EventDTO eventDTO){
        List<AbstractUser> users = eventDAO.getUsersAttendedEvent(EventMapper.toEntity(eventDTO));
        List<AbstractUserDTO> result = new ArrayList<>();
        users.forEach(user -> result.add(UserMapper.toDTO(user)));
        return result;
    }

    /**
     * Used to register user attendance on given event
     * @param userDTO user whose attendance will be registered
     * @param eventDTO event where user attendance will be registered
     * @throws IncorrectDataException if such user or event doesn't exist or if user is not registered for this event
     */
    public static void registerAttendance(AbstractUserDTO userDTO, EventDTO eventDTO) throws IncorrectDataException {
        Event event = eventDAO.get(eventDTO.getId()).orElse(null);
        AbstractUser user = userDAO.get(userDTO.getLogin()).orElse(null);
        eventDAO.registerAttendance(event, user);
    }
    public static List<ReportDTO> getReportsByEvent(EventDTO eventDTO){
        List<Report> reports = eventDAO.getReportsByEvent(EventMapper.toEntity(eventDTO));
        List<ReportDTO> result = new ArrayList<>();
        reports.forEach(user -> result.add(ReportMapper.toDTO(user)));
        return result;
    }
    public static void setUserToEvent(AbstractUserDTO userDTO, EventDTO eventDTO){
        eventDAO.addUserToEvent(UserMapper.toEntity(userDTO), EventMapper.toEntity(eventDTO));
    }
    public static void setReportToEvent(ReportDTO reportDTO, EventDTO eventDTO){
        eventDAO.setReportToEvent(ReportMapper.toEntity(reportDTO), EventMapper.toEntity(eventDTO));
    }

    /**
     * Service initialization
     * @param userDAO UserDAO implementation
     * @param eventDAO EventDAO implementation
     */
    public static void init(AbstractUserDAO userDAO, EventDAO eventDAO){
        EventService.userDAO = userDAO;
        EventService.eventDAO = eventDAO;
    }
}
