package com.my.service;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.EventDAO;
import com.my.DAO.ReportDAO;
import com.my.DAO.TopicDAO;
import com.my.DAO.impl.AbstractUserDAOImpl;
import com.my.DAO.impl.ReportDAOImpl;
import com.my.DAO.impl.TopicDAOImpl;
import com.my.DTO.AbstractUserDTO;
import com.my.DTO.ReportDTO;
import com.my.DTO.TopicDTO;
import com.my.entity.AbstractUser;
import com.my.entity.Report;
import com.my.entity.Topic;
import com.my.exception.IncorrectDataException;
import com.my.mappers.ReportMapper;
import com.my.mappers.UserMapper;

import java.util.ArrayList;
import java.util.List;
/**
 * Used to manipulate data using DAO. Requires initialization.
 */
public class ReportService {
    private static AbstractUserDAO userDAO;
    private static ReportDAO reportDAO;
    private static TopicDAO topicDAO;
    /**
     * Service initialization
     * @param userDAO UserDAO implementation
     * @param reportDAO ReportDAO implementation
     * @param topicDAO TopicDAO implementation
     */
    public static void init(AbstractUserDAO userDAO, ReportDAO reportDAO, TopicDAO topicDAO){
        ReportService.userDAO = userDAO;
        ReportService.reportDAO = reportDAO;
        ReportService.topicDAO = topicDAO;
    }
    /**
     * Used to get report specified by id parameter
     * @param id id of report to find
     * @return ReportDTO specified by id
     * @throws IncorrectDataException if report with this id does not exist
     */
    public static ReportDTO get(int id) throws IncorrectDataException {
        try {
            return ReportMapper.toDTO(reportDAO.get(id).orElse(null));
        }catch (NullPointerException e){
            throw new IncorrectDataException();
        }
    }
    /**
     * Used to get list of all reports
     * @return List of all reports
     */
    public static List<ReportDTO> getAll(){
        List<Report> reports = reportDAO.getAll();
        List<ReportDTO> result = new ArrayList<>();
        reports.forEach(user -> result.add(ReportMapper.toDTO(user)));
        return result;
    }
    /**
     * Used to save report specified by given parameters
     * @param topicID id of topic
     * @param speakerLogin login of speaker
     */
    public static int save(String speakerLogin, int topicID){
        AbstractUser speaker = userDAO.get(speakerLogin).orElse(null);
        Topic topic = topicDAO.get(topicID).orElse(null);
        Report report = new Report(topic, speaker);
        reportDAO.save(report);
        return report.getId();
    }
    /**
     * Used to update information about report specified by id parameter
     * @param id id of report to be updated
     * @param speakerLogin speaker login of new speaker
     * @param topicID id of new topic
     */
    public static void update(int id, String speakerLogin, int topicID){
        Report report = reportDAO.get(id).orElse(null);
        if(report == null){
            return;
        }
        reportDAO.update(report, new String[]{speakerLogin, String.valueOf(topicID)});
    }
    /**
     * Used to delete report specified by id
     * @param id id of report to be deleted
     */
    public static void delete(int id){
        Report report = reportDAO.get(id).orElse(null);
        if(report == null){
            return;
        }
        reportDAO.delete(report);
    }
    /**
     * Used to get List with limited number of reports specified by offset and limit parameters
     * @param offset number of reports to be skipped
     * @param limit number of reports to retrieve
     * @param result List where retrieved reports will be stored
     * @return number of all reports
     */
    public static int getAll(int offset, int limit, List<ReportDTO> result){
        List<Report> reports = new ArrayList<>();
        int resultCount = reportDAO.getAll(offset, limit, reports);
        reports.forEach(user -> result.add(ReportMapper.toDTO(user)));
        return resultCount;
    }
    /**
     * Used to get List with limited number of reports, where user equals to given in parameter, specified by offset and limit parameters
     * @param offset number of reports to be skipped
     * @param limit number of reports to retrieve
     * @param result List where retrieved reports will be stored
     * @param user user whose reports will be retrieved
     * @return number of all reports where given user is speaker
     */
    public static int getMyAll(int offset, int limit, List<ReportDTO> result, AbstractUserDTO user){
        List<Report> reports = new ArrayList<>();
        int resultCount = reportDAO.getReportsByUser(offset, limit, reports, UserMapper.toEntity(user));
        reports.forEach(report -> result.add(ReportMapper.toDTO(report)));
        return resultCount;
    }
}
