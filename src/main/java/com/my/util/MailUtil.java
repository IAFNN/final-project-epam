package com.my.util;

import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Util class for email notifications. Requires initialization.
 * Some details of email connection are specified in email.properties in resources directory.
 */
public class MailUtil {
    private static final Properties emailProperties = new Properties();
    private static String from;
    private static String password;
    private static final String host = "smtp.gmail.com";
    private static final String port = "587";
    private static final String timeout = "5000";
    private static final Properties systemProperties = System.getProperties();
    static {
        systemProperties.setProperty("mail.smtp.auth", "true");
        systemProperties.setProperty("mail.smtp.host", host);
        systemProperties.setProperty("mail.smtp.port", port);
        systemProperties.setProperty("mail.smtp.connectiontimeout", timeout);
        systemProperties.setProperty("mail.smtp.timeout", timeout);
        systemProperties.setProperty("mail.smtp.writetimeout", timeout);
        systemProperties.setProperty("mail.smtp.starttls.enable", "true");
    }

    /**
     * Used to send email
     * @param recipientAddress recipient email address
     * @param subject subject of letter
     * @param messageText text of letter
     * @throws MessagingException if message wasn't sent successfully
     */
    public static void sendEmail(String recipientAddress, String subject, String messageText) throws MessagingException {
        Session session = Session.getDefaultInstance(systemProperties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });
        MimeMessage message = new MimeMessage(session);
        message.setFrom(from);
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientAddress));
        message.setSubject(subject);
        message.setText(messageText);
        Transport.send(message);
    }

    /**
     * Used to initialize class
     * @param realPath real path to application
     */
    public static void init(String realPath){
        try(InputStream stream = new FileInputStream(realPath + "WEB-INF/classes/email.properties")){
            emailProperties.load(stream);
            from = emailProperties.getProperty("email.login");
            password = emailProperties.getProperty("email.password");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
