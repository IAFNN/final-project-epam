package com.my.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * General Util class
 */
public class Utils {
    private Utils() {
    }

    /**
     * Used to convert Instant object to String in format HH:MM\tDD.MM.YYYY
     * @param time Instant instance to be converted
     * @return String in format HH:MM\tDD.MM.YYYY
     */
    public static String convertInstantToString(Instant time) {
        StringBuilder stringBuilder = new StringBuilder();
        ZonedDateTime zonedTime = time.atZone(ZoneOffset.UTC);
        stringBuilder.append(String.format("%02d", zonedTime.getHour())).append(":").append(String.format("%02d", zonedTime.getMinute())).append("\t");
        stringBuilder.append(String.format("%02d", zonedTime.getDayOfMonth())).append(".").append(String.format("%02d", zonedTime.getMonth().getValue())).append(".").append(zonedTime.getYear());
        return stringBuilder.toString();
    }

    /**
     * Used to generate sequence of random 16 bytes and convert to String
     * @return String representing sequence of random 16 bytes
     */
    public static String generateSalt(){
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return new String(salt);
    }

    /**
     * Used to hash given password using given salt
     * @param password password to be hashed
     * @param salt salt used to hash password
     * @return hashed password
     */
    public static String hashPassword(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return new String(factory.generateSecret(spec).getEncoded());
    }

    /**
     * Used to build text of message which will be sent to user as notification about changes in event they registered for
     * @param id event id where changes happened
     * @param newName new event name
     * @param newDate new event date
     * @param newLocation new event location
     * @return built message text
     */
    public static String buildMessageText(String id, String newName, String newDate, String newLocation){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Something has changed in event(Event ID: %s) you joined\n", id));
        stringBuilder.append(String.format("New Event name is %s\n", newName));
        stringBuilder.append(String.format("New Event date is %s\n", newDate));
        stringBuilder.append(String.format("New Event location is %s\n", newLocation));
        return stringBuilder.toString();
    }
}
