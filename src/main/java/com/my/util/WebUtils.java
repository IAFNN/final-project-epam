package com.my.util;

import com.my.DTO.*;
import com.my.tags.event.table.EventTable;
import com.my.tags.report.table.ReportTable;
import com.my.tags.topic.table.TopicTable;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * Util class for building complex html elements. Requires initialization.
 */
public class WebUtils {
    private WebUtils() {
    }
    /**
     * Used to build report table in html
     * @param locale current user locale
     * @param reports list of reports to be displayed in table
     * @param submits list of submits to be displayed in table
     * @param buttons list of buttons to be displayed in table
     * @param table list of enums which specify columns that will be in table
     * @return built table
     */
    public static StringBuilder constructReportTable(String locale, List<ReportDTO> reports, HashMap<String, String> submits,
                                                    HashMap<String, String> buttons, ReportTable table){
        Properties localeFile = loadProperties(locale);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<tr class='headerRow'>\n");
        table.setupTable().forEach(topicTableColumn -> {
            switch (topicTableColumn){
                case ID:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.ID")));
                    break;
                case TOPIC_ID:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.topicID")));
                    break;
                case TOPIC_NAME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.topicName")));
                    break;
                case EVENT_ID:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.eventID")));
                    break;
                case EVENT_NAME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.eventName")));
                    break;
                case SPEAKER_LOGIN:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.speakerLogin")));
                    break;
                case SPEAKER_NAME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.speakerName")));
                    break;
            }
        });
        stringBuilder.append("</tr>\n");
        for (ReportDTO report : reports) {
            stringBuilder.append("<tr class=\"contentRow\">\n");
            table.setupTable().forEach(topicTableColumn -> {
                switch (topicTableColumn){
                    case ID:
                        stringBuilder.append(String.format("<td>%s</td>\n", report.getId()));
                        break;
                    case TOPIC_ID:
                        if(report.getTopicID() != -1) {
                            stringBuilder.append(String.format("<td>%s</td>\n", report.getTopicID()));
                        }else{
                            stringBuilder.append("<td>-</td>\n");
                        }
                        break;
                    case TOPIC_NAME:
                        if(report.getTopicName() != null) {
                            stringBuilder.append(String.format("<td>%s</td>\n", report.getTopicName()));
                        }else{
                            stringBuilder.append(String.format("<td>%s</td>\n", localeFile.getProperty("label.noTopic")));
                        }
                        break;
                    case EVENT_ID:
                        if(report.getEventID() != -1){
                            stringBuilder.append(String.format("<td>%s</td>\n", report.getEventID()));
                        }else{
                            stringBuilder.append("<td>-</td>\n");
                        }
                        break;
                    case EVENT_NAME:
                        if(report.getEventName() != null){
                            stringBuilder.append(String.format("<td>%s</td>\n", report.getEventName()));
                        }else{
                            stringBuilder.append(String.format("<td>%s</td>\n", localeFile.getProperty("label.noEvent")));
                        }
                        break;
                    case SPEAKER_LOGIN:
                        if(report.getSpeakerLogin() != null){
                            stringBuilder.append(String.format("<td>%s</td>\n", report.getSpeakerLogin()));
                        }else{
                            stringBuilder.append("<td>-</td>\n");
                        }
                        break;
                    case SPEAKER_NAME:
                        if(report.getSpeakerName() != null){
                            stringBuilder.append(String.format("<td>%s</td>\n", report.getSpeakerName()));
                        }else{
                            stringBuilder.append(String.format("<td>%s</td>\n", localeFile.getProperty("label.noSpeaker")));
                        }
                        break;
                    case BUTTONS:
                        stringBuilder.append(constructButtons(submits, buttons, String.valueOf(report.getId()), localeFile));
                }
            });
            stringBuilder.append("</tr>\n");
        }
        return stringBuilder;
    }

    /**
     * Used to build event table in html
     * @param locale current user locale
     * @param events list of events to be displayed in table
     * @param submits list of submits to be displayed in table
     * @param buttons list of buttons to be displayed in table
     * @param table list of enums which specify columns that will be in table
     * @return built table
     */
    public static StringBuilder constructEventTable(String locale, List<EventDTO> events, HashMap<String, String> submits,
                                                     HashMap<String, String> buttons, EventTable table){
        Properties localeFile = loadProperties(locale);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<tr class='headerRow'>\n");
        table.setupTable().forEach(topicTableColumn -> {
            switch (topicTableColumn){
                case ID:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.ID")));
                    break;
                case NAME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.name")));
                    break;
                case TIME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.time")));
                    break;
                case LOCATION:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.location")));
                    break;
                case NUMBER_OF_REGISTERED_USERS:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.numberOfRegisteredUsers")));
                    break;
                case NUMBER_OF_USERS_ATTENDED:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.numberOfAttendedUsers")));
                    break;
                case NUMBER_OF_REPORTS:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.numberOfReports")));
                    break;
            }
        });
        stringBuilder.append("</tr>\n");
        for (EventDTO event : events) {
            stringBuilder.append("<tr class=\"contentRow\">\n");
            table.setupTable().forEach(topicTableColumn -> {
                switch (topicTableColumn){
                    case ID:
                        stringBuilder.append(String.format("<td>%s</td>\n", event.getId()));
                        break;
                    case NAME:
                        stringBuilder.append(String.format("<td>%s</td>\n", event.getName()));
                        break;
                    case TIME:
                        stringBuilder.append(String.format("<td>%s</td>\n", event.getDate()));
                        break;
                    case LOCATION:
                        stringBuilder.append(String.format("<td>%s</td>\n", event.getLocation()));
                        break;
                    case NUMBER_OF_REGISTERED_USERS:
                        if(!event.getUsersRegistered().isEmpty()) {
                            stringBuilder.append(String.format("<td>%s</td>\n", event.getUsersRegistered().size()));
                        }else{
                            stringBuilder.append("<td>0</td>\n");
                        }
                        break;
                    case NUMBER_OF_USERS_ATTENDED:
                        if(!event.getUsersAttended().isEmpty()) {
                            stringBuilder.append(String.format("<td>%s</td>\n", event.getUsersAttended().size()));
                        }else{
                            stringBuilder.append("<td>0</td>\n");
                        }
                        break;
                    case NUMBER_OF_REPORTS:
                        if(!event.getReports().isEmpty()) {
                            stringBuilder.append(String.format("<td>%s</td>\n", event.getReports().size()));
                        }else{
                            stringBuilder.append("<td>0</td>\n");
                        }
                        break;
                    case BUTTONS:
                        stringBuilder.append(constructButtons(submits, buttons, String.valueOf(event.getId()), localeFile));
                }
            });
            stringBuilder.append("</tr>\n");
        }
        return stringBuilder;
    }

    /**
     * Used to build topic table in html
     * @param locale current user locale
     * @param topics list of topics to be displayed in table
     * @param submits list of submits to be displayed in table
     * @param buttons list of buttons to be displayed in table
     * @param table list of enums which specify columns that will be in table
     * @return built table
     */
    public static StringBuilder constructTopicTable(String locale, List<TopicDTO> topics, HashMap<String, String> submits,
                                                    HashMap<String, String> buttons, TopicTable table){
        Properties localeFile = loadProperties(locale);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<tr class='headerRow'>\n");
        table.setupTable().forEach(topicTableColumn -> {
            switch (topicTableColumn){
                case ID:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.ID")));
                    break;
                case NAME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.name")));
                    break;
                case EVENT_ID:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.eventID")));
                    break;
                case EVENT_NAME:
                    stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.eventName")));
                    break;
            }
        });
        stringBuilder.append("</tr>\n");
        for (TopicDTO topic : topics) {
            stringBuilder.append("<tr class=\"contentRow\">\n");
            table.setupTable().forEach(topicTableColumn -> {
                switch (topicTableColumn){
                    case ID:
                        stringBuilder.append(String.format("<td>%s</td>\n", topic.getId()));
                        break;
                    case NAME:
                        stringBuilder.append(String.format("<td>%s</td>\n", topic.getName()));
                        break;
                    case EVENT_ID:
                        if(topic.getEventID() != -1){
                            stringBuilder.append(String.format("<td>%s</td>\n", topic.getEventID()));
                        }else{
                            stringBuilder.append("<td>-</td>\n");
                        }
                        break;
                    case EVENT_NAME:
                        if(topic.getEventName() != null){
                            stringBuilder.append(String.format("<td>%s</td>\n", topic.getEventName()));
                        }else{
                            stringBuilder.append(String.format("<td>%s</td>\n", localeFile.getProperty("label.noEvent")));
                        }
                        break;
                    case BUTTONS:
                        stringBuilder.append(constructButtons(submits, buttons, String.valueOf(topic.getId()), localeFile));
                }
            });
            stringBuilder.append("</tr>\n");
        }
        return stringBuilder;
    }

    /**
     * Used to build offer table in html
     * @param locale current user locale
     * @param offerList list of offers to be displayed in table
     * @param submits list of submits to be displayed in table
     * @param buttons list of buttons to be displayed in table
     * @return built table
     */
    public static StringBuilder constructOffersTable(String locale, List<OfferDTO> offerList, HashMap<String, String> submits,
                                                     HashMap<String, String> buttons){
        Properties localeFile = loadProperties(locale);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<tr class='headerRow'>\n");
        stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.ID")));
        stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.offerType")));
        stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.reportID")));
        stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.topicID")));
        stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.speakerLogin")));
        stringBuilder.append(String.format("<th>%s</th>\n", localeFile.getProperty("label.eventID")));
        stringBuilder.append("</tr>\n");
        for (OfferDTO offer : offerList) {
            stringBuilder.append("<tr class=\"contentRow\">\n");
            stringBuilder.append(String.format("<td>%s</td>\n", offer.getId()));
            stringBuilder.append(String.format("<td>%s</td>\n", offer.getOfferType()));
            if(offer.getReportID() != -1) {
                stringBuilder.append(String.format("<td>%s</td>\n", offer.getReportID()));
            }else{
                stringBuilder.append("<td>-</td>\n");
            }
            if(offer.getTopicID() != -1) {
                stringBuilder.append(String.format("<td>%s</td>\n", offer.getTopicID()));
            }else{
                stringBuilder.append("<td>-</td>\n");
            }
            if(offer.getSpeakerLogin() != null) {
                stringBuilder.append(String.format("<td>%s</td>\n", offer.getSpeakerLogin()));
            }else{
                stringBuilder.append("<td>-</td>\n");
            }
            if(offer.getEventID() != -1) {
                stringBuilder.append(String.format("<td>%s</td>\n", offer.getEventID()));
            }else{
                stringBuilder.append("<td>-</td>\n");
            }
            stringBuilder.append(constructButtons(submits, buttons, String.valueOf(offer.getId()), localeFile));
            stringBuilder.append("</tr>\n");
        }
        return stringBuilder;
    }

    /**
     * Used to build table cells with given submits and buttons
     * @param submits HashMap where key corresponds to key in locale file and value corresponds to command that should
     *                process request created by this submit
     * @param buttons HashMap where key corresponds to key in locale file and value corresponds to javascript code to be
     *                executed when this button is pushed
     * @param identifier identifier of current row in table
     * @param localeFile properties file with all text labels
     * @return built cells
     */
    public static StringBuilder constructButtons(HashMap<String, String> submits, HashMap<String, String> buttons, String identifier,
                                                 Properties localeFile){
        StringBuilder stringBuilder = new StringBuilder();
        if(submits != null) {
            for (String submitName : submits.keySet()) {
                stringBuilder.append("<td>\n");
                stringBuilder.append("<form action='frontControllerServlet' method='post'>\n");
                stringBuilder.append(String.format("<input type='hidden' name='command' value='%s'/>\n", submits.get(submitName)));
                stringBuilder.append(String.format("<input type='hidden' name='id' value='%s'/>\n", identifier));
                stringBuilder.append(String.format("<input type='submit' class=\"btn btn-primary\" value='%s'/>\n", localeFile.getProperty(submitName)));
                stringBuilder.append("</form>\n");
                stringBuilder.append("</td>\n");
            }
        }
        if(buttons != null) {
            for (String buttonName : buttons.keySet()) {
                stringBuilder.append("<td>\n");
                stringBuilder.append(String.format("<button type='button' class=\"btn btn-primary\" onclick='%s'>%s</button>", buttons.get(buttonName),
                        localeFile.getProperty(buttonName)).replaceFirst("\\.", identifier));
                stringBuilder.append("</td>\n");
            }
        }
        return stringBuilder;
    }

    /**
     * Used to initialize class
     * @param realPath real path to application
     */
    public static void init(String realPath) {
        WebUtils.realPath = realPath;
    }

    private static String realPath;

    /**
     * Used to load locale file which corresponds to given locale parameter
     * @param locale locale of properties file to be loaded
     * @return loaded properties file
     */
    public static Properties loadProperties(String locale) {
        Properties localeFile = new Properties();
        String propertiesPath;
        if (locale.isEmpty() || locale.equals("null")) {
            propertiesPath = "WEB-INF/classes/locale.properties";
        } else {
            propertiesPath = String.format("WEB-INF/classes/locale_%s.properties", locale);
        }
        try (InputStream inputStream = new FileInputStream(realPath + propertiesPath)) {
            localeFile.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return localeFile;
    }
}
