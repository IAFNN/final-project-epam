<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myLib" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
	<title>Offers</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="styles/inputStyle.css"/>
	<link rel="stylesheet" type="text/css" href="styles/tabs.css">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
	<script src="scripts/validation.js"></script>
	<script src="scripts/offersDetails.js"></script>
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
	<div>
		<form action="frontControllerServlet">
			<input type="hidden" name="command" value="GetStartPage"/>
			<input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.startPage"/>">
		</form>
	</div>
	<div>
		<label for="locale"></label>
		<form action="frontControllerServlet" method="post">
			<select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
				<option>Choose Language</option>
				<option>en</option>
				<option>ua</option>
			</select>
			<input type="hidden" name="command" value="ChangeLocale"/>
			<input type="hidden" name="currentPage" value="offersDetails.jsp"/>
		</form>
	</div>
	<div>
		<form action="frontControllerServlet" method="post">
			<input type="hidden" name="command" value="SignOut"/>
			<input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.signOut"/>">
		</form>
	</div>
</header>
<div class="mainContentContainer" style="margin-bottom: 150px">
	<div class="multipleInputFieldsCont" style="height: 400px">
		<table id="offersTable" class="table table-bordered border-primary">
			<jsp:useBean id="submits" class="java.util.HashMap"/>
			<std:set target="${submits}" property="label.accept" value="AcceptOffer"/>
			<std:set target="${submits}" property="label.decline" value="DeclineOffer"/>
			<myLib:constructOffersTable locale="${sessionScope.locale}"
			                           offerList="${sessionScope.offerList}" submits="${submits}"/>
		</table>
		<div class="paginationButtons">
			<tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfOffersPages}" commandName="OffersDetails"/>
		</div>

	</div>
</div>
</body>
</html>
