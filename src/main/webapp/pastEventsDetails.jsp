<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myLib" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <title>Past Event Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/style.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="styles/tabs.css"/>
    <link rel="stylesheet" type="text/css" href="styles/inputStyle.css"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="scripts/pastEventsDetails.js"></script>
    <script src="scripts/validation.js"></script>
    <script src="scripts/eventDetails.js"></script>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
    <div>
        <form action="frontControllerServlet">
            <input type="hidden" name="command" value="GetStartPage"/>
            <input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.startPage"/>">
        </form>
    </div>
    <div>
        <label for="locale"></label>
        <form action="frontControllerServlet" method="post">
            <select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
                <option>Choose Language</option>
                <option>en</option>
                <option>ua</option>
            </select>
            <input type="hidden" name="command" value="ChangeLocale"/>
            <input type="hidden" name="currentPage" value="pastEventsDetails.jsp"/>
        </form>
    </div>
    <div>
        <form action="frontControllerServlet" method="post">
            <input type="hidden" name="command" value="SignOut"/>
            <input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.signOut"/>">
        </form>
    </div>
</header>
<div class="mainContentContainer" style="margin-bottom: 150px">
    <div class="multipleInputFieldsCont" id="multipleInputFieldsCont" style="height: 750px">
        <std:if test="${sessionScope.userType == 'MODERATOR'}">
            <jsp:useBean id="buttons" class="java.util.HashMap"/>
            <std:set target="${buttons}" property="label.hasAttended" value="return registerAttendancePressed(.)"/>
            <table id="eventsDetailsTable" class="table table-bordered border-primary">
                <myLib:constructEventTable displayId="true" displayStatistics="true"
                                           locale="${sessionScope.locale}" events="${sessionScope.pastEventList}"
                buttons="${buttons}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons withSorting="true" numberOfPages="${sessionScope.numberOfPastEventsPages}" commandName="PastEventDetails"/>
            </div>
        </std:if>
        <std:if test="${sessionScope.userType == 'USER' || sessionScope.userType == 'SPEAKER'}">
            <table id="eventsDetailsTable" class="table table-bordered border-primary">
                <myLib:constructEventTable displayId="true" locale="${sessionScope.locale}"
                                           events="${sessionScope.pastEventList}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons withSorting="true" numberOfPages="${sessionScope.numberOfPastEventsPages}" commandName="PastEventDetails"/>
            </div>
        </std:if>
        <div>
            <button type="button" class="btn btn-outline-primary" onclick="changeSorting('time'); document.getElementById('paginationForm').submit()"><fmt:message
                    key="label.sortByTime"/></button>
            <button type="button" class="btn btn-outline-primary" onclick="changeSorting('numberOfUsers'); document.getElementById('paginationForm').submit()"><fmt:message
                    key="label.sortByNumberOfUsers"/></button>
            <button type="button" class="btn btn-outline-primary" onclick="changeSorting('numberOfReports'); document.getElementById('paginationForm').submit()"><fmt:message
                    key="label.sortByNumberOfReports"/></button>
        </div>
        <form action="frontControllerServlet" method="post" name="joinForm">
            <div class="inputField">
                <input type="hidden" name="command" value="RegisterAttendance" id="commandParameter">
                <input type="hidden" name="eventID" value="" id="idParameter">
                <div class="multipleInputFieldsCont" id="eventsInput">
                    <div class="inputField">
                        <div class="form-outline mb-4">
                            <label for="loginField" class="form-label"><fmt:message key="input.login"/></label>
                            <input type="text" class="form-control" name="login" id="loginField" onchange="document.getElementById('loginValidationError').style.display = 'none'"/>
                        </div>
                        <div id="loginValidationError" class="validationError">
                            <label><fmt:message key="label.loginValidationError"/></label>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value="<fmt:message key="label.confirm"/>"
                           onclick="return validateRegistrationAttendance('eventIDField', 'eventsDetailsTable', 'loginField')"/>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
