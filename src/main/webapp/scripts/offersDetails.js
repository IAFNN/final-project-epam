function acceptPressed(){
    let result = validateID("IDField", "offersTable", 0, "IDValidationError");
    if(result){
        changeCommandValue('AcceptOffer');
        return true;
    }else{
        document.getElementById("IDValidationError").style.display = "flex";
        return false;
    }
}
function declinePressed(){
    let result = validateID("IDField", "offersTable", 0, "IDValidationError");
    if(result){
        changeCommandValue('DeclineOffer');
        return true;
    }else{
        document.getElementById("IDValidationError").style.display = "flex";
        return false;
    }
}
function changeCommandValue(newValue){
    const hiddenParameter = document.querySelector('#commandParameter');
    hiddenParameter.value = newValue;
}