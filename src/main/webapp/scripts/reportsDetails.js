function displayForSpeaker() {
    const allReports = document.getElementById("tableMyReports");
    const myReports = document.getElementById("tableAllReports");
    if (isChecked) {
        myReports.style.display = "flex";
        allReports.style.display = "none";
    } else {
        allReports.style.display = "flex";
        myReports.style.display = "none";
    }
    isChecked = !isChecked;
}

let isChecked = false;

function changeCommandValue(newValue) {
    const hiddenParameter = document.querySelector('#commandParameter');
    hiddenParameter.value = newValue;
}

function showReportsInput() {
    document.getElementById("reportsInput").style.display = "inherit";
}

function joinReportPressed() {
    let result = validateID('IDField', 'tableAllReportsElement', 0, "IDValidationError");
    if (result) {
        changeCommandValue('OfferJoinReport');
        return true;
    } else {
        return false;
    }
}

function offerCreateReportPressed() {
    showReportsInput();
    changeCommandValue('OfferCreateReport');
}
function createReportPressed() {
    showReportsInput();
    changeCommandValue('CreateReport');
}
function changeTopicPressed(id){
    changeCommandValue('OfferChangeTopic');
    changeIDValue(id);
    document.getElementById("topicChangeInput").style.display = "flex";
    return true;
}
function changeIDValue(newValue) {
    const hiddenParameter = document.querySelector('#idParameter');
    hiddenParameter.value = newValue;
}