function changeCommandValue(newValue) {
    const hiddenParameter = document.querySelector('#commandParameter');
    hiddenParameter.value = newValue;
}
function changeIDValue(newValue) {
    const hiddenParameter = document.querySelector('#idParameter');
    hiddenParameter.value = newValue;
}
function showEventsInput(toFillInputs, id) {
    if (toFillInputs) {
        let table = document.getElementById("eventsDetailsTable");
        for (let row of table.rows) {
            if (parseInt(row.cells[0].textContent) === id) {
                document.getElementById("nameField").value = row.cells[1].textContent;
                document.getElementById("timeInput").value = row.cells[2].textContent;
                document.getElementById("locationField").value = row.cells[3].textContent;
                break;
            }
        }
    }
    document.getElementById("eventsInput").style.display = "inherit";
}


function parseDate(string) {
    let result = new Date();
    result.setHours(string.substring(0, 2));
    result.setMinutes(string.substring(3, 5));
    result.setDate(string.substring(6, 8));
    result.setMonth(string.substring(9, 11));
    result.setFullYear(string.substring(12, 16));
    return result;
}

function joinPressed() {
    let result = validateID('IDField', 'eventsDetailsTable', 0, "IDValidationError");
    if (result) {
        changeCommandValue('Join');
        return true;
    } else {
        return false;
    }
}

function editPressed(id) {
    changeCommandValue('Edit');
    changeIDValue(id);
    showEventsInput(true, id);
    return true;
}

function deletePressed() {
    let result = validateID('IDField', 'eventsDetailsTable', 0, "IDValidationError");
    if (result) {
        changeCommandValue('Delete');
        return true;
    } else {
        return false;
    }
}

function createPressed() {
    document.getElementById("multipleInputFieldsCont").style.height = "750px";
    changeCommandValue('Create');
    showEventsInput(false);
}

function changeSorting(sortBy){
    let sortByParameters = document.getElementsByClassName("sortByParameter");
    for(let sortByParameter of sortByParameters){
        sortByParameter.value = sortBy;
    }
}

function init(){
    document.getElementById("timeInput").min = new Date().toISOString().slice(0, 16);
}