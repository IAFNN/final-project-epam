function validateLogin() {
    let login = document.forms["loginForm"]["login"];
    let loginCorrect = new RegExp(".{6,45}").test(login.value);
    if(!loginCorrect){
        document.getElementById("loginValidationError").style.display = "flex";
    }
    let password = document.forms["loginForm"]["password"];
    let passwordCorrect = new RegExp("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{1,45})$").test(password.value);
    if(!passwordCorrect){
        document.getElementById("passwordValidationError").style.display = "flex";
    }
    return loginCorrect === true && passwordCorrect === true;
}

function validateRegister() {
    let name = document.forms["registerForm"]["name"].value;
    let nameCorrect = new RegExp(".{6,45}").test(name);
    if(!nameCorrect){
        document.getElementById("nameValidationError").style.display = "flex";
    }
    let login = document.forms["registerForm"]["login"].value;
    let loginCorrect = new RegExp(".{6,45}").test(login);
    if(!loginCorrect){
        document.getElementById("loginValidationError").style.display = "flex";
    }
    let password = document.forms["registerForm"]["password"].value;
    let passwordCorrect = new RegExp("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{1,45})$").test(password);
    if(!passwordCorrect){
        document.getElementById("passwordValidationError").style.display = "flex";
    }
    return loginCorrect === true && nameCorrect === true && passwordCorrect === true;
}

function validateID(IDField, tableID, columnIndex, validationErrorID) {
    let ID = document.getElementById(IDField).value;
    let table = document.getElementById(tableID);
    for (let row of table.rows) {
        if (ID === row.cells[columnIndex].innerHTML) {
            return true;
        }
    }
    document.getElementById(validationErrorID).style.display = "flex";
    return false;
}

function validateEventsInput(nameField, timeInput, locationField) {
    let name = document.getElementById(nameField).value;
    let location = document.getElementById(locationField).value;
    let time = document.getElementById(timeInput).value;
    let nameCorrect = new RegExp(".+").test(name);
    let locationCorrect = new RegExp(".+").test(location);
    let timeCorrect = new RegExp('[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}').test(time);
    if(!nameCorrect){
        document.getElementById("nameValidationError").style.display = "flex";
    }
    if(!locationCorrect){
        document.getElementById("locationValidationError").style.display = "flex";
    }
    if(!timeCorrect){
        document.getElementById("timeValidationError").style.display = "flex";
    }
    return nameCorrect && locationCorrect && timeCorrect;
}

function validateRegistrationAttendance(eventIDField, eventsTable, userLoginField) {
    let eventID = document.getElementById(eventIDField).value;
    let eventIDCorrect = false;
    let table = document.getElementById(eventsTable);
    for (let row of table.rows) {
        eventIDCorrect = (eventID === row.cells[0].innerHTML) || eventIDCorrect;
    }
    let loginCorrect = new RegExp(".{6,45}").test(document.getElementById(userLoginField).value);
    if(!eventIDCorrect){
        document.getElementById("IDValidationError").style.display="flex";
    }
    if(!loginCorrect){
        document.getElementById("loginValidationError").style.display="flex";
    }
    return loginCorrect && eventIDCorrect;
}
function validateTopicInput(topicNameField){
    let topicName = document.getElementById(topicNameField).value;
    let topicNameCorrect = new RegExp(".+").test(topicName);
    if(!topicNameCorrect){
        document.getElementById("topicNameValidationError").style.display = "flex";
    }
    return topicNameCorrect;
}
function displayError(errorMessage){
    alert(errorMessage);
}