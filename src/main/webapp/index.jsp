<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <title>Login page</title>
    <link href="styles/inputStyle.css" rel="stylesheet" type="text/css">
    <link href="styles/style.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="scripts/validation.js"></script>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
    <div>
        <label for="locale"></label>
        <form action="frontControllerServlet" method="post">
            <select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; console.log(this.value); this.form.submit()">
                <option>Choose Language</option>
                <option>en</option>
                <option>ua</option>
            </select>
            <input type="hidden" name="command" value="ChangeLocale"/>
            <input type="hidden" name="currentPage" value="index.jsp"/>
        </form>
    </div>
</header>
<div class="mainContentContainer">
    <form action="frontControllerServlet" method="post" name="loginForm">
        <div class="form-outline mb-4">
            <label class="form-label" for="form2Example1"><fmt:message key="input.login"/></label>
            <input type="email" id="form2Example1" class="form-control" onchange="document.getElementById('loginValidationError').style.display = 'none'" name="login" />
        </div>
        <div class="form-outline mb-4">
        <div id="loginValidationError" class="validationError">
            <label><fmt:message key="label.loginValidationError"/></label>
        </div>
        </div>
        <div class="form-outline mb-4">
            <label class="form-label" for="form2Example2"><fmt:message key="input.password"/></label>
            <input type="password" id="form2Example2" name="password" class="form-control" onchange="document.getElementById('passwordValidationError').style.display = 'none'"/>
        </div>
        <div class="form-outline mb-4">
        <div id="passwordValidationError" class="validationError">
            <label><fmt:message key="label.passwordValidationError"/></label>
        </div>
        </div>
        <div class="row mb-4">
            <div class="col d-flex justify-content-center">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="rememberMe" value="" id="form2Example31" checked />
                    <label class="form-check-label" for="form2Example31"><fmt:message key="input.rememberMe"/></label>
                </div>
            </div>
        </div>
        <input type="submit" class="btn btn-primary btn-block mb-4" value="<fmt:message key="label.loginButton"/>" onclick="return validateLogin()"/>
        <div class="text-center">
            <p><a href="register.jsp"><fmt:message key="label.register"/></a></p>
        </div>
        <input type="hidden" value="Login" name="command"/>
    </form>
</div>
</body>
</html>