<%@ tag import="com.my.DTO.AbstractUserDTO" %>
<%@ tag import="java.util.List" %>
<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="registeredUsersList" type="java.util.List" required="true" rtexprvalue="true" %>
<%
	out.append("<select name='login' class=\"form-select\" id='loginSelect'>\n");
	for(AbstractUserDTO userDTO : (List<AbstractUserDTO>)registeredUsersList){
		out.append(String.format("<option>%s | %s</option>\n", userDTO.getLogin(), userDTO.getName()));
	}
	out.append("</select>\n");
%>