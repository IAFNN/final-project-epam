<%@ tag import="com.my.DTO.EventDTO" %>
<%@ tag import="java.util.List" %>
<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="eventList" type="java.util.List" required="true" rtexprvalue="true" %>
<%
	out.append("<select name='eventID' class=\"form-select\" id='eventIDSelect'>\n");
	for(EventDTO eventDTO : (List<EventDTO>)eventList){
		out.append(String.format("<option>%d | %s</option>\n", eventDTO.getId(), eventDTO.getName()));
	}
	out.append("</select>\n");
%>