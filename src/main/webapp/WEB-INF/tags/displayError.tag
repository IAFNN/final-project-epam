<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="errorMessageAttributeName" required="true" rtexprvalue="true" %>
<%
	out.append(String.format("displayError('%s')", session.getAttribute(errorMessageAttributeName)));
    session.removeAttribute(errorMessageAttributeName);
%>