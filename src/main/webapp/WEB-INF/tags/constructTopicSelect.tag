<%@ tag import="com.my.DTO.TopicDTO" %>
<%@ tag import="java.util.List" %>
<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="topicList" type="java.util.List" required="true" rtexprvalue="true" %>
<%
	out.append("<select class=\"form-select\" name='topicID' id='topicIDSelect'>\n");
	for(TopicDTO topicDTO : (List<TopicDTO>)topicList){
        out.append(String.format("<option>%d | %s</option>\n", topicDTO.getId(), topicDTO.getName()));
	}
    out.append("</select>\n");
%>