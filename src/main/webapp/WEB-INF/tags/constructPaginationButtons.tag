<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="numberOfPages" type="java.lang.Integer" required="true" rtexprvalue="true" %>
<%@ attribute name="commandName" type="java.lang.String" required="true" %>
<%@ attribute name="withSorting" type="java.lang.Boolean"%>
<%
	out.append("<nav aria-label=\"Page navigation example\">\n");
    out.append("<div class=\"pagination\">");
	for(int i = 0; i < numberOfPages; i++){
        out.append("<div class=\"page-item\">");
        out.append("<form action='frontControllerServlet' id='paginationForm'>\n");
        out.append(String.format("<input type='hidden' name='numberOfCurrentPage' value='%d'/>\n", i));
        out.append(String.format("<input type='hidden' name='command' value='%s'/>\n", commandName));
        out.append(String.format("<input type='submit' value='%d' class=\"page-link\"/>\n", i + 1));
        if(withSorting != null && withSorting){
	        out.append("<input type='hidden' name='sortBy' value='' class='sortByParameter'/>\n");
        }
        out.append("</form>\n");
        out.append("</div>\n");
	}
	out.append("</div>");
    out.append("</nav>");
%>