<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myLib" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <title>Join Event</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="styles/tabs.css"/>
    <link rel="stylesheet" type="text/css" href="styles/inputStyle.css"/>
    <link rel="stylesheet" type="text/css" href="styles/style.css"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="scripts/validation.js"></script>
    <script src="scripts/eventDetails.js"></script>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale"/>
</head>
<body onload="init(); <std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
    <div>
        <form action="frontControllerServlet">
            <input type="hidden" name="command" value="GetStartPage"/>
            <input type="submit"  class="btn btn-outline-primary" value="<fmt:message key="label.startPage"/>">
        </form>
    </div>
    <div>
        <label for="locale"></label>
        <form action="frontControllerServlet" method="post">
            <select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
                <option>Choose Language</option>
                <option>en</option>
                <option>ua</option>
            </select>
            <input type="hidden" name="command" value="ChangeLocale"/>
            <input type="hidden" name="currentPage" value="eventDetails.jsp"/>
        </form>
    </div>
    <div>
        <form action="frontControllerServlet" method="post">
            <input type="hidden" name="command" value="SignOut"/>
            <input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.signOut"/>">
        </form>
    </div>
</header>
<div class="mainContentContainer" style="margin-bottom: 150px">
    <div class="multipleInputFieldsCont" id="multipleInputFieldsCont" style="height: 500px">
        <table id="eventsDetailsTable" class="table table-bordered border-primary">
            <jsp:useBean id="submits" class="java.util.HashMap"/>
            <jsp:useBean id="buttons" class="java.util.HashMap"/>
            <std:if test="${sessionScope.userType == 'USER'}">
                <std:set target="${submits}" property="label.join" value="Join"/>
            </std:if>
            <std:if test="${sessionScope.userType == 'MODERATOR'}">
                <std:set target="${buttons}" property="label.edit" value="return editPressed(.)"/>
                <std:set target="${submits}" property="label.delete" value="Delete"/>
            </std:if>
            <myLib:constructEventTable displayId="true" locale="${sessionScope.locale}" events="${sessionScope.futureEventList}"
                submits="${submits}" buttons="${buttons}"/>
        </table>
        <div class="paginationButtons">
            <tg:constructPaginationButtons withSorting="true" numberOfPages="${sessionScope.numberOfFutureEventsPages}" commandName="FutureEventDetails"/>
        </div>
        <div>
            <button type="button" class="btn btn-outline-primary" onclick="changeSorting('time'); document.getElementById('paginationForm').submit()"><fmt:message
                    key="label.sortByTime"/></button>
            <button type="button" class="btn btn-outline-primary" onclick="changeSorting('numberOfUsers'); document.getElementById('paginationForm').submit()"><fmt:message
                    key="label.sortByNumberOfUsers"/></button>
            <button type="button" class="btn btn-outline-primary" onclick="changeSorting('numberOfReports'); document.getElementById('paginationForm').submit()"><fmt:message
                    key="label.sortByNumberOfReports"/></button>
        </div>
        <form action="frontControllerServlet" method="post" name="joinForm">
            <div class="inputField">
                <input type="hidden" name="id" value="" id="idParameter"/>
                <input type="hidden" name="command" value="" id="commandParameter"/>
                <div class="buttonsCont">
                    <std:if test="${sessionScope.userType == 'MODERATOR'}">
                        <button type="button" class="btn btn-primary" onclick="return createPressed()"><fmt:message
                                key="label.create"/></button>
                    </std:if>
                </div>
                <div class="multipleInputFieldsCont" id="eventsInput">
                    <div class="inputField">
                        <div class="form-outline mb-4">
                            <label for="nameField" class="form-label"><fmt:message key="input.nameField"/></label>
                            <input type="text" name="name" id="nameField" class="form-control" onchange="document.getElementById('nameValidationError').style.display = 'none'"/>
                        </div>
                        <div id="nameValidationError" class="validationError">
                            <label><fmt:message key="label.nameValidationError"/></label>
                        </div>
                        <div class="form-outline mb-4">
                            <label for="timeInput" class="form-label"><fmt:message key="input.timeField"/></label>
                            <input type="datetime-local" class="form-control" name="time" id="timeInput" onchange="document.getElementById('timeValidationError').style.display = 'none'"/>
                        </div>
                        <div id="timeValidationError" class="validationError">
                            <label><fmt:message key="label.timeValidationError"/></label>
                        </div>
                        <div class="form-outline mb-4">
                            <label for="locationField" class="form-label"><fmt:message key="input.locationField"/></label>
                            <input type="text" name="location" class="form-control" id="locationField" onchange="document.getElementById('locationValidationError').style.display = 'none'"/>
                        </div>
                        <div id="locationValidationError" class="validationError">
                            <label><fmt:message key="label.locationValidationError"/></label>
                        </div>
                    </div>
                    <input type="submit" value="<fmt:message key="label.confirm"/>" class="btn btn-primary"
                           onclick="return validateEventsInput('nameField', 'timeInput', 'locationField')"/>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
