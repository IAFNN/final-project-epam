<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myLib" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
	<title>Topics Details</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<link rel="stylesheet" type="text/css" href="styles/inputStyle.css">
	<link rel="stylesheet" type="text/css" href="styles/tabs.css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
	<script src="scripts/topicsDetails.js"></script>
	<script src="scripts/validation.js"></script>
	<fmt:setLocale value="${sessionScope.locale}"/>
	<fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
	<div>
		<form action="frontControllerServlet">
			<input type="hidden" name="command" value="GetStartPage"/>
			<input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.startPage"/>">
		</form>
	</div>
	<div>
		<label for="locale"></label>
		<form action="frontControllerServlet" method="post">
			<select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
				<option>Choose Language</option>
				<option>en</option>
				<option>ua</option>
			</select>
			<input type="hidden" name="command" value="ChangeLocale"/>
			<input type="hidden" name="currentPage" value="topicsDetails.jsp"/>
		</form>
	</div>
	<div>
		<form action="frontControllerServlet" method="post">
			<input type="hidden" name="command" value="SignOut"/>
			<input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.signOut"/>">
		</form>
	</div>
</header>
<div class="mainContentContainer" style="margin-bottom: 150px">
	<div class="multipleInputFieldsCont" style="height: 400px">
		<table class="table table-bordered border-primary">
			<myLib:constructTopicTable displayID="true" locale="${sessionScope.locale}"
			                            topics="${sessionScope.topicList}"/>
		</table>
		<div class="paginationButtons">
			<tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfTopicPages}" commandName="TopicDetails"/>
		</div>
		<std:if test="${sessionScope.userType == 'MODERATOR'}">
			<form action="frontControllerServlet" method="post">
				<div class="multipleInputFieldsCont">
					<div class="buttonsCont">
						<input type="hidden" name="command" value="CreateTopic" id="commandParameter">
						<button type="button" class="btn btn-primary" onclick="return createTopicPressed()"><fmt:message
								key="label.createTopic"/>
						</button>
					</div>
					<div class="multipleInputFieldsCont" id="topicsInput">
						<div class="inputField">
							<div class="form-outline mb-4">
								<label for="topicNameField" class="form-label"><fmt:message key="input.TopicNameField"/></label>
								<input type="text" name="topicName" class="form-control" id="topicNameField" onchange="document.getElementById('topicNameValidationError').style.display = 'none'"/>
							</div>
							<div id="topicNameValidationError" class="validationError">
								<label><fmt:message key="label.topicNameValidationError"/></label>
							</div>
							<div>
								<label for="eventIDSelect"><fmt:message key="input.eventIDField"/></label>
								<tg:constructEventSelect eventList="${sessionScope.eventList}"/>
							</div>
						</div>
						<input type="submit" class="btn btn-primary"
						       value="<fmt:message key="label.confirm"/>" onclick="return validateTopicInput('topicNameField')"/>
					</div>
				</div>
			</form>
		</std:if>
	</div>
</div>
</body>
</html>
