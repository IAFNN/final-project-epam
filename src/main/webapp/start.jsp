<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myLib" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/tabs.css" type="text/css"/>
    <link rel="stylesheet" href="styles/inputStyle.css" type="text/css"/>
    <link rel="stylesheet" href="styles/style.css" type="text/css"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <title>Main Page</title>
    <script src="scripts/changeTab.js"></script>
    <script src="scripts/validation.js"></script>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
    <div>
        <label for="locale"></label>
        <form action="frontControllerServlet" method="post">
            <select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
                <option>Choose Language</option>
                <option>en</option>
                <option>ua</option>
            </select>
            <input type="hidden" name="command" value="ChangeLocale"/>
            <input type="hidden" name="currentPage" value="start.jsp"/>
        </form>
    </div>
    <div>
        <form action="frontControllerServlet" method="post">
            <input type="hidden" name="command" value="SignOut"/>
            <input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.signOut"/>">
        </form>
    </div>
</header>
<div class="mainContentContainer" style="margin-top: 100px">
    <div id="eventsTable" class="tableContainer">
        <div class="tab">
            <div class="nav nav-tabs">
                <div class="nav-item">
                    <button class="nav-link" onclick="openTab(event, 'pastEvents')"><fmt:message
                        key="label.pastEvents"/></button>
                </div>
                <div class="nav-item">
                    <button class="nav-link" onclick="openTab(event, 'futureEvents')"><fmt:message
                        key="label.futureEvents"/></button>
                </div>
        </div>
        </div>
        <div id="pastEvents" class="tabContent">
            <table id="pastEventsTable" class="table table-bordered border-primary">
                <myLib:constructEventTable locale="${sessionScope.locale}"
                                           events="${sessionScope.pastEventList}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfPastEventsPages}" commandName="ChangePagePastEvents"/>
            </div>
            <form method="get">
                <button type="submit" class="btn btn-primary" formaction="frontControllerServlet"><fmt:message
                        key="label.eventDetails"/></button>
                <input type="hidden" name="command" value="PastEventDetails"/>
            </form>
        </div>

        <div id="futureEvents" class="tabContent">
            <table id="futureEventsTable" class="table table-bordered border-primary">
                <myLib:constructEventTable locale="${sessionScope.locale}" events="${sessionScope.futureEventList}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfFutureEventsPages}" commandName="ChangePageFutureEvents"/>
            </div>
            <form method="get">
                <button type="submit" class="btn btn-primary" formaction="frontControllerServlet"><fmt:message
                        key="label.eventDetails"/></button>
                <input type="hidden" name="command" value="FutureEventDetails"/>
            </form>
        </div>

    </div>
    <div class="tableContainer">
        <div id="reports">
            <table id="reportsTable" class="table table-bordered border-primary">
                <myLib:constructReportTable locale="${sessionScope.locale}" reports="${sessionScope.reportList}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfReportsPages}" commandName="ChangePageReports"/>
            </div>
            <form method="get">
                <button type="submit" class="btn btn-primary" formaction="frontControllerServlet"><fmt:message
                        key="label.reportDetails"/></button>
                <input type="hidden" name="command" value="ReportDetails"/>
            </form>
        </div>
    </div>
    <div class="tableContainer">
        <div id="topics">
            <table id="topicsTable" class="table table-bordered border-primary">
                <myLib:constructTopicTable locale="${sessionScope.locale}"
                                           topics="${sessionScope.topicList}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfTopicPages}" commandName="ChangePageTopics"/>
            </div>
            <form>
                <button type="submit" class="btn btn-primary"
                        formaction="frontControllerServlet"><fmt:message key="label.topicsDetails"/></button>
                <input type="hidden" name="command" value="TopicDetails"/>
            </form>
        </div>
    </div>
</div>
</body>
</html>
