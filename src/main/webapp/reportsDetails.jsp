<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myLib" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <title>Report Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="styles/inputStyle.css"/>
    <link rel="stylesheet" type="text/css" href="styles/tabs.css">
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <link rel="stylesheet" type="text/css" href="styles/reportsDetails.css"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="scripts/reportsDetails.js"></script>
    <script src="scripts/validation.js"></script>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
    <div>
        <form action="frontControllerServlet">
            <input type="hidden" name="command" value="GetStartPage"/>
            <input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.startPage"/>">
        </form>
    </div>
    <div>
        <label for="locale"></label>
        <form action="frontControllerServlet" method="post">
            <select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
                <option>Choose Language</option>
                <option>en</option>
                <option>ua</option>
            </select>
            <input type="hidden" name="command" value="ChangeLocale"/>
            <input type="hidden" name="currentPage" value="reportsDetails.jsp"/>
        </form>
    </div>
    <div>
        <form action="frontControllerServlet" method="post">
            <input type="hidden" name="command" value="SignOut"/>
            <input type="submit" class="btn btn-outline-primary" value="<fmt:message key="label.signOut"/>">
        </form>
    </div>
</header>
<div class="mainContentContainer" style="margin-bottom: 150px">
    <div class="multipleInputFieldsCont" style="height: 600px">
        <jsp:useBean id="buttons" class="java.util.HashMap"/>
        <jsp:useBean id="submits" class="java.util.HashMap"/>
        <std:if test="${sessionScope.userType == 'MODERATOR'}">
            <table id="tableModReports" class="table table-bordered border-primary">
                <std:set target="${buttons}" property="label.changeTopic" value="return changeTopicPressed(.)"/>
                <myLib:constructReportTable displayLogin="true" locale="${sessionScope.locale}"
                                            reports="${sessionScope.reportList}" buttons="${buttons}" displayId="true"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfReportsPages}" commandName="ReportDetails"/>
            </div>
        </std:if>
        <std:if test="${sessionScope.userType == 'USER'}">
            <table class="table table-bordered border-primary">
                <myLib:constructReportTable displayId="true" locale="${sessionScope.locale}"
                                            reports="${sessionScope.reportList}"/>
            </table>
            <div class="paginationButtons">
                <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfReportsPages}" commandName="ReportDetails"/>
            </div>
        </std:if>
        <std:if test="${sessionScope.userType == 'SPEAKER'}">
            <std:set target="${submits}" property="label.joinReport" value="OfferJoinReport"/>
            <div id="tableAllReports" class="multipleInputFieldsCont">
                <table id="tableAllReportsElement" class="table table-bordered border-primary">
                    <myLib:constructReportTable displayId="true" locale="${sessionScope.locale}"
                                                reports="${sessionScope.reportList}" submits="${submits}" buttons="${buttons}"/>
                </table>
                <div class="paginationButtons">
                    <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfReportsPages}" commandName="ReportDetails"/>
                </div>
            </div>
            <div id="tableMyReports" class="multipleInputFieldsCont">
                <table class="table table-bordered border-primary">
                    <myLib:constructReportTable displayId="true" locale="${sessionScope.locale}"
                                                reports="${sessionScope.myReportList}"
                                                buttons="${buttons}"/>
                </table>
                <div class="paginationButtons">
                    <tg:constructPaginationButtons numberOfPages="${sessionScope.numberOfMyReportsPages}" commandName="ReportDetails"/>
                </div>
            </div>
            <div class="form-check">
                <label for="forSpeaker" class="form-check-label"><fmt:message key="input.displayMyReports"/></label>
                <input type="checkbox" class="form-check-input" id="forSpeaker" onclick="displayForSpeaker()"/>
            </div>
        </std:if>
        <std:if test="${sessionScope.userType == 'MODERATOR' || sessionScope.userType == 'SPEAKER'}">
            <form action="frontControllerServlet" method="post">
                <div class="multipleInputFieldsCont">
                    <div class="buttonsCont">
                        <input type="hidden" name="id" value="" id="idParameter"/>
                        <input type="hidden" name="command" value="" id="commandParameter">
                        <std:if test="${sessionScope.userType == 'SPEAKER'}">
                        <button type="button" class="btn btn-primary" onclick="return offerCreateReportPressed()"><fmt:message
                                key="label.offerCreateReport"/>
                        </button>
                        </std:if>
                        <std:if test="${sessionScope.userType == 'MODERATOR'}">
                            <button type="button" class="btn btn-primary" onclick="return createReportPressed()"><fmt:message
                                    key="label.createReport"/>
                            </button>
                        </std:if>
                    </div>
                    <div class="multipleInputFieldsCont" id="topicChangeInput">
                        <div class="inputField">
                            <div>
                                <label for="topicIDSelect"><fmt:message key="input.topicIDField"/></label>
                                <tg:constructTopicSelect topicList="${sessionScope.topicList}"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="label.confirm"/>"/>
                    </div>
                    <div class="multipleInputFieldsCont" id="reportsInput">
                        <div class="inputField">
                            <div>
                                <label for="topicIDSelect"><fmt:message key="input.topicIDField"/></label>
                                <tg:constructTopicSelect topicList="${sessionScope.topicList}"/>
                            </div>
                            <div>
                                <label for="eventIDSelect"><fmt:message key="input.eventIDField"/></label>
                                <tg:constructEventSelect eventList="${sessionScope.eventList}"/>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary" value="<fmt:message key="label.confirm"/>"/>
                    </div>
                </div>
            </form>
            <form action="frontControllerServlet">
                <input type="hidden" name="command" value="OffersDetails"/>
                <input type="submit" class="btn btn-primary" value="<fmt:message key="label.goToOffers"/>"/>
            </form>
        </std:if>

    </div>
</div>
</body>
</html>
