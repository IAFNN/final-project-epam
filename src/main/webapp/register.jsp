<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="std" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tg" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="styles/inputStyle.css">
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script src="scripts/validation.js"></script>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="locale"/>
</head>
<body onload="<std:if test="${sessionScope.errorMessage != null}"><tg:displayError errorMessageAttributeName="errorMessage"/></std:if>">
<header>
    <div>
        <label for="locale"></label>
        <form action="frontControllerServlet" method="post">
            <select id="locale" class="form-select" onchange="this.form.action += '?locale=' + this.value; this.form.submit()">
                <option>Choose Language</option>
                <option>en</option>
                <option>ua</option>
            </select>
            <input type="hidden" name="command" value="ChangeLocale"/>
            <input type="hidden" name="currentPage" value="register.jsp"/>
        </form>
    </div>
</header>
<div class="mainContentContainer">
    <div class="container-fluid vh-100">
        <div class="">
            <div class="rounded d-flex justify-content-center">
                <div class="col-md-4 col-sm-12 shadow-lg p-5 bg-light">
                    <div class="p-4">
                        <form action="frontControllerServlet" method="post" name="registerForm"
                              onsubmit="return validateRegister()">
                            <div class="input-group mb-3">
                                    <span class="input-group-text bg-primary"><i
                                            class="bi bi-person-plus-fill text-white"></i></span>
                                <input type="text" class="form-control" name="name" placeholder="<fmt:message key="input.nameField"/>"
                                       onchange="document.getElementById('nameValidationError').style.display = 'none'"/>
                            </div>
                            <div id="nameValidationError" class="validationError">
                                <label><fmt:message key="label.userNameValidationError"/></label>
                            </div>
                            <div class="input-group mb-3">
                                    <span class="input-group-text bg-primary"><i
                                            class="bi bi-envelope text-white"></i></span>
                                <input type="email" class="form-control" name="login" placeholder="<fmt:message key="input.login"/>"
                                       onchange="document.getElementById('loginValidationError').style.display = 'none'">
                            </div>
                            <div id="loginValidationError" class="validationError">
                                <label><fmt:message key="label.loginValidationError"/></label>
                            </div>
                            <div class="input-group mb-3">
                                    <span class="input-group-text bg-primary"><i
                                            class="bi bi-key-fill text-white"></i></span>
                                <input type="password" class="form-control" name="password" placeholder="<fmt:message key="input.password"/>"
                                       onchange="document.getElementById('passwordValidationError').style.display = 'none'">
                            </div>
                            <div id="passwordValidationError" class="validationError">
                                <label><fmt:message key="label.passwordValidationError"/></label>
                            </div>
                            <div class="col-md-6 mb-4">
                            <select class="form-select" name="userType">
                                <option value="" disabled><fmt:message key="label.userType"/></option>
                                <option value="User"><fmt:message key="label.user"/></option>
                                <option value="Speaker"><fmt:message key="label.speaker"/></option>
                                <option value="Moderator"><fmt:message key="label.moderator"/></option>
                            </select>
                            </div>
                            <div class="d-grid col-12 mx-auto">
                                <input class="btn btn-primary" type="submit" value="<fmt:message key="label.register"/>"/>
                            </div>
                            <input type="hidden" value="Register" name="command"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
