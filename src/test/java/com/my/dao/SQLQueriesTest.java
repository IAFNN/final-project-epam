package com.my.dao;

import com.my.DAO.impl.SQLQueries;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SQLQueriesTest {

    @Test
    void simpleQueriesTest(){
        Assertions.assertEquals("select * from ?", SQLQueries.selectAllFromTable());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from ?", SQLQueries.selectAllFromTableWithCount());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from ? where ?", SQLQueries.selectAllFromTableWithCountCondition());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from ? limit ?, ?", SQLQueries.selectAllFromTableWithLimit());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from ? where ? limit ?, ?", SQLQueries.selectAllFromTableWithLimitCondition());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from events where ? order by Time ? limit ?, ?", SQLQueries.selectAllFromTableOrderByTime());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from events left outer join users_events on events.ID = users_events.EventID where ? group by ID order by count(users_events.EventID) ? limit ?, ?",
                SQLQueries.selectAllFromTableOrderByNumberOfUsers());
        Assertions.assertEquals("select SQL_CALC_FOUND_ROWS * from events left outer join events_reports on events.ID = events_reports.EventID where ? group by ID order by count(events_reports.EventID) ? limit ?, ?",
                SQLQueries.selectAllFromTablesOrderByNumberOfReports());
        Assertions.assertEquals("delete from ? where ?", SQLQueries.deleteFromTable());
    }
    @Test
    void parametrizedMethodsTest(){
        Assertions.assertEquals("select * from ? where ? and ? and ?", SQLQueries.selectFromTableWithCondition(3));
        Assertions.assertEquals("insert into ?(?, ?, ?) values (?, ?, ?)", SQLQueries.insertIntoTableWithParameters(3));
        Assertions.assertEquals("update . set .=?, .=?, .=? where . and .", SQLQueries.updateTable(3, 2));
    }
}
