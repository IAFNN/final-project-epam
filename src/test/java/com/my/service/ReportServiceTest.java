package com.my.service;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.EventDAO;
import com.my.DAO.ReportDAO;
import com.my.DAO.TopicDAO;
import com.my.DAO.impl.AbstractUserDAOImpl;
import com.my.DAO.impl.EventDAOImpl;
import com.my.DAO.impl.ReportDAOImpl;
import com.my.DAO.impl.TopicDAOImpl;
import com.my.DTO.ReportDTO;
import com.my.entity.*;
import com.my.exception.IncorrectDataException;
import com.my.listeners.ServletContextInitializationListener;
import com.my.mappers.ReportMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReportServiceTest {
    private static AbstractUserDAO userDAO;
    private static ReportDAO reportDAO;
    private static TopicDAO topicDAO;
    private static EventDAO eventDAO;
    @BeforeAll
    static void init(){
        userDAO = Mockito.mock(AbstractUserDAOImpl.class);
        reportDAO = Mockito.mock(ReportDAOImpl.class);
        topicDAO = Mockito.mock(TopicDAOImpl.class);
        eventDAO = Mockito.mock(EventDAOImpl.class);
        ReportService.init(userDAO, reportDAO, topicDAO);
        ReportMapper.init(eventDAO, reportDAO);
        UserService.init(userDAO);
    }

    @Test
    void getTest(){
        Report report = new Report(1, new Topic(1, "nehuy"),
                new AbstractUser("some speaker", "speaker1", "speaker322", UserType.SPEAKER, "dsgfdg"));
        Mockito.when(reportDAO.get(1)).thenReturn(Optional.of(report));
        Mockito.when(reportDAO.get(-1)).thenReturn(Optional.empty());
        Mockito.when(reportDAO.get(2)).thenReturn(Optional.empty());
        Mockito.when(eventDAO.getEventByReport(null)).thenReturn(Optional.empty());
        Mockito.when(eventDAO.getEventByReport(report)).thenReturn(Optional.of(new Event(3, "past event", Instant.parse("2021-01-20T02:02:05.00Z"), "Kyiv")));
        try {
            ReportDTO reportDTO = ReportService.get(1);
            Assertions.assertEquals(reportDTO, new ReportDTO(1, 1, "nehuy", "speaker1", "some speaker", 3, "past event"));
            Assertions.assertThrows(IncorrectDataException.class, () -> ReportService.get(-1));
            Assertions.assertThrows(IncorrectDataException.class, () -> ReportService.get(2));
        }catch (IncorrectDataException e){
            e.printStackTrace();
        }
    }
    @Test
    void testGetAll(){
        Report report1 = new Report(1, new Topic(1, "nehuy"),
                new AbstractUser("some speaker", "speaker1", "speaker322", UserType.SPEAKER, "dsgfdg"));
        Report report2 = new Report(0, new Topic(1, "huy"), new AbstractUser("random user", "random.user", "random1488", UserType.USER, "afssfa"));
        Mockito.when(reportDAO.getAll()).thenReturn(List.of(report1, report2));
        Mockito.when(eventDAO.getEventByReport(report1)).thenReturn(Optional.of(new Event(3, "past event", Instant.parse("2021-01-20T02:02:05.00Z"), "Kyiv")));
        Mockito.when(eventDAO.getEventByReport(report2)).thenReturn(Optional.of(new Event(2, "no event", Instant.parse("2021-01-19T02:02:05.00Z"), "Lviv")));
        Mockito.when(reportDAO.getAll(0, 2, new ArrayList<>())).thenReturn(3);
        List<ReportDTO> result = ReportService.getAll();
        Assertions.assertEquals(List.of(new ReportDTO(1, 1, "nehuy", "speaker1", "some speaker", 3, "past event"),
                new ReportDTO(0, 1, "huy", "random.user", "random user", 2, "no event")), result);
        Assertions.assertEquals(3, ReportService.getAll(0, 2, new ArrayList<>()));
    }
    @Test
    void testSave(){
        Mockito.when(userDAO.get("randommmm")).thenReturn(Optional.of(new AbstractUser("random", "randommmm", "12345sff", UserType.MODERATOR, "dsgfdg")));
        Mockito.when(topicDAO.get(1)).thenReturn(Optional.of(new Topic(1, "lololol")));
        ReportService.save("randommmm", 1);
        Mockito.verify(reportDAO).save(new Report(new Topic(1, "lololol"), new AbstractUser("random", "randommmm", "12345sff", UserType.MODERATOR, "dsgfdg")));
    }
    @Test
    void testUpdate(){
        Report report = new Report(1, new Topic(2, "random"), new AbstractUser("lollll", "notRandomName", "lolololo2323", UserType.USER, "dsgfdg"));
        Mockito.when(reportDAO.get(1)).thenReturn(Optional.of(report));
        ReportService.update(1, "randomName", 1);
        Mockito.verify(reportDAO).update(report, new String[]{"randomName", "1"});
        ReportService.update(1, null, -1);
        Mockito.verify(reportDAO).update(report,
                new String[]{null, "-1"});
    }
    @Test
    void testDelete(){
        Report report = new Report(1, new Topic(2, "random"), new AbstractUser("lollll", "notRandomName", "lolololo2323", UserType.USER, "dsgfdg"));
        Mockito.when(reportDAO.get(1)).thenReturn(Optional.of(report));
        ReportService.delete(1);
        Mockito.verify(reportDAO).delete(report);
    }
}
