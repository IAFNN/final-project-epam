package com.my.service;

import com.my.DAO.EventDAO;
import com.my.DAO.TopicDAO;
import com.my.DAO.impl.EventDAOImpl;
import com.my.DAO.impl.TopicDAOImpl;
import com.my.DTO.ReportDTO;
import com.my.DTO.TopicDTO;
import com.my.entity.*;
import com.my.exception.IncorrectDataException;
import com.my.mappers.TopicMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TopicServiceTest {
    private static EventDAO eventDAO;
    private static TopicDAO topicDAO;
    @BeforeAll
    static void init(){
        eventDAO = Mockito.mock(EventDAOImpl.class);
        topicDAO = Mockito.mock(TopicDAOImpl.class);
        TopicMapper.init(eventDAO, topicDAO);
        TopicService.init(topicDAO);
    }
    @Test
    void getTest(){
        Topic topic = new Topic(1, "hey");
        Mockito.when(topicDAO.get(1)).thenReturn(Optional.of(topic));
        Mockito.when(topicDAO.get(-1)).thenReturn(Optional.empty());
        Mockito.when(topicDAO.get(2)).thenReturn(Optional.empty());
        Mockito.when(eventDAO.getEventByTopic(null)).thenReturn(Optional.empty());
        Mockito.when(eventDAO.getEventByTopic(topic)).thenReturn(Optional.of(new Event(3, "past event", Instant.parse("2021-01-20T02:02:05.00Z"), "Kyiv")));
        try {
            TopicDTO topicDTO = TopicService.get(1);
            Assertions.assertEquals(topicDTO, new TopicDTO(1, "hey", 3, "past event"));
            Assertions.assertThrows(IncorrectDataException.class, () -> TopicService.get(-1));
            Assertions.assertThrows(IncorrectDataException.class, () -> TopicService.get(2));
        }catch (IncorrectDataException e){
            e.printStackTrace();
        }
    }
    @Test
    void testGetAll(){
        Topic topic1 = new Topic(1, "kkjjj");
        Topic topic2 = new Topic(2, "kiimimin");
        Mockito.when(topicDAO.getAll()).thenReturn(List.of(topic1, topic2));
        Mockito.when(eventDAO.getEventByTopic(topic1)).thenReturn(Optional.of(new Event(3, "past event", Instant.parse("2021-01-20T02:02:05.00Z"), "Kyiv")));
        Mockito.when(eventDAO.getEventByTopic(topic2)).thenReturn(Optional.of(new Event(2, "no event", Instant.parse("2021-01-19T02:02:05.00Z"), "Lviv")));
        Mockito.when(topicDAO.getAll(0, 2, new ArrayList<>())).thenReturn(3);
        List<TopicDTO> result = TopicService.getAll();
        Assertions.assertEquals(List.of(new TopicDTO(1, "kkjjj", 3, "past event"),
                new TopicDTO(2, "kiimimin", 2, "no event")), result);
        Assertions.assertEquals(3, TopicService.getAll(0, 2, new ArrayList<>()));
    }
    @Test
    void testSave(){
        Topic topic1 = new Topic(1, "kkjjj");
        Mockito.when(eventDAO.getEventByTopic(topic1)).thenReturn(Optional.of(new Event(3, "past event", Instant.parse("2021-01-20T02:02:05.00Z"), "Kyiv")));
        TopicDTO topicDTO = TopicService.save("kkjjj");
        Mockito.verify(topicDAO).save(new Topic(topicDTO.getId(), topicDTO.getName()));
    }
    @Test
    void testUpdate(){
        Topic topic = new Topic(1, "lol");
        Mockito.when(topicDAO.get(1)).thenReturn(Optional.of(topic));
        TopicService.update(1, "randomName");
        Mockito.verify(topicDAO).update(topic, new String[]{"randomName"});
    }
    @Test
    void testDelete(){
        Topic topic = new Topic(1, "ksfsdfsd");
        Mockito.when(topicDAO.get(1)).thenReturn(Optional.of(topic));
        TopicService.delete(1);
        Mockito.verify(topicDAO).delete(topic);
    }
}
