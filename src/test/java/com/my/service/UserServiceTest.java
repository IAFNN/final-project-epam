package com.my.service;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.impl.AbstractUserDAOImpl;
import com.my.DTO.AbstractUserDTO;
import com.my.entity.AbstractUser;
import com.my.entity.UserType;
import com.my.exception.IncorrectDataException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserServiceTest {
    private static AbstractUserDAO userDAO;
    @BeforeAll
    static void init(){
        userDAO = Mockito.mock(AbstractUserDAOImpl.class);
        UserService.init(userDAO);
    }

    @Test
    void getTest(){
        Mockito.when(userDAO.get("speaker1")).thenReturn(Optional.of(new AbstractUser("some speaker", "speaker1", "speaker322", UserType.SPEAKER, "FDSAFASD")));
        Mockito.when(userDAO.get(null)).thenReturn(Optional.empty());
        Mockito.when(userDAO.get("speaker0")).thenReturn(Optional.empty());
        try {
            AbstractUserDTO userDTO = UserService.get("speaker1");
            Assertions.assertEquals(userDTO, new AbstractUserDTO("some speaker", "speaker1", UserType.SPEAKER));
            Assertions.assertThrows(IncorrectDataException.class, () -> UserService.get(null));
            Assertions.assertThrows(IncorrectDataException.class, () -> UserService.get("speaker0"));
        }catch (IncorrectDataException e){
            e.printStackTrace();
        }
    }
    @Test
    void testGetAll(){
        Mockito.when(userDAO.getAll()).thenReturn(List.of(new AbstractUser("good admin", "admin.lol", "admin555", UserType.USER, "FDSAFASD"),
                new AbstractUser("random user", "random.user", "random1488", UserType.USER, "FSDFSAF")));
        Mockito.when(userDAO.getAll(0, 2, new ArrayList<>())).thenReturn(3);
        List<AbstractUserDTO> result = UserService.getAll();
        Assertions.assertEquals(result, List.of(new AbstractUserDTO("good admin", "admin.lol", UserType.USER),
                new AbstractUserDTO("random user", "random.user", UserType.USER)));
        Assertions.assertEquals(3, UserService.getAll(0, 2, new ArrayList<>()));
    }
    @Test
    void testSave(){
        try {
            UserService.save("randommmm", "lololololo", "abodfs24", UserType.SPEAKER, "sdgdfg");
            Mockito.verify(userDAO).save(new AbstractUser("lololololo", "randommmm", "abodfs24", UserType.SPEAKER, "sdgdfg"));
        }catch (IncorrectDataException e){
            e.printStackTrace();
        }
    }
    @Test
    void testUpdate(){
        Mockito.when(userDAO.get("random.user")).thenReturn(Optional.of(new AbstractUser("randomName","random.user", "random1488", UserType.USER, "dsgfdg")));
        UserService.update("random.user", "randomName", "random1488", "User");
        Mockito.verify(userDAO).update(new AbstractUser("randomName", "random.user", "random1488", UserType.USER, "dsgfdg"),
                new String[]{"randomName", "random1488", "User"});
        UserService.update("random.user", null, null, null);
        Mockito.verify(userDAO, Mockito.times(2)).update(new AbstractUser("randomName", "random.user", "random1488", UserType.USER, "dsgfdg"),
                new String[]{"randomName", "random1488", "User"});
    }
    @Test
    void testDelete(){
        Mockito.when(userDAO.get("random.user")).thenReturn(Optional.of(new AbstractUser("random user", "random.user",
                "random1488", UserType.USER, "dsgfdg")));
        UserService.delete("random.user");
        Mockito.verify(userDAO).delete(new AbstractUser("random user", "random.user", "random1488", UserType.USER, "dsgfdg"));
    }
    @Test
    void testGetPassword(){
        Mockito.when(userDAO.get("random.user")).thenReturn(Optional.of(new AbstractUser("random user", "random.user",
                "random1488", UserType.USER, "dsgfdg")));
        Assertions.assertEquals("random1488", UserService.getPassword("random.user"));
    }

}
