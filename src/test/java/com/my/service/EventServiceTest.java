package com.my.service;

import com.my.DAO.AbstractUserDAO;
import com.my.DAO.EventDAO;
import com.my.DAO.ReportDAO;
import com.my.DAO.TopicDAO;
import com.my.DAO.impl.AbstractUserDAOImpl;
import com.my.DAO.impl.EventDAOImpl;
import com.my.DAO.impl.ReportDAOImpl;
import com.my.DAO.impl.TopicDAOImpl;
import com.my.DTO.AbstractUserDTO;
import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.DTO.TopicDTO;
import com.my.entity.*;
import com.my.exception.IncorrectDataException;
import com.my.mappers.EventMapper;
import com.my.mappers.ReportMapper;
import com.my.mappers.TopicMapper;
import com.my.mappers.UserMapper;
import com.my.util.Utils;
import jdk.jshell.execution.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EventServiceTest {
    private static AbstractUserDAO userDAO;
    private static ReportDAO reportDAO;
    private static TopicDAO topicDAO;
    private static EventDAO eventDAO;
    @BeforeAll
    static void init(){
        userDAO = Mockito.mock(AbstractUserDAOImpl.class);
        reportDAO = Mockito.mock(ReportDAOImpl.class);
        eventDAO = Mockito.mock(EventDAOImpl.class);
        topicDAO = Mockito.mock(TopicDAOImpl.class);
        ReportService.init(userDAO, reportDAO, topicDAO);
        ReportMapper.init(eventDAO, reportDAO);
        EventMapper.init(eventDAO);
        EventService.init(userDAO, eventDAO);
        TopicMapper.init(eventDAO, topicDAO);
        UserService.init(userDAO);
        UserMapper.init(userDAO);
    }
    @Test
    void getTest(){
        Event event = new Event(1, "test", Instant.EPOCH, "here");
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(event));
        Mockito.when(eventDAO.get(-1)).thenReturn(Optional.empty());
        Mockito.when(eventDAO.get(2)).thenReturn(Optional.empty());
        Mockito.when(eventDAO.getUsersByEvent(null)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getUsersByEvent(event)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getUsersAttendedEvent(null)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getUsersAttendedEvent(event)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getReportsByEvent(null)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getReportsByEvent(event)).thenReturn(new ArrayList<>());
        try {
            EventDTO eventDTO = EventService.get(1);
            Assertions.assertEquals(eventDTO, new EventDTO(1, "test", Utils.convertInstantToString(Instant.EPOCH), "here", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
            Assertions.assertThrows(IncorrectDataException.class, () -> TopicService.get(-1));
            Assertions.assertThrows(IncorrectDataException.class, () -> TopicService.get(2));
        }catch (IncorrectDataException e){
            e.printStackTrace();
        }
    }
    @Test
    void testGetAll(){
        Event event1 = new Event(1, "kkjjj", Instant.EPOCH, "test");
        Mockito.when(eventDAO.getAll()).thenReturn(List.of(event1));
        Mockito.when(eventDAO.getUsersByEvent(event1)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getUsersAttendedEvent(event1)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getReportsByEvent(event1)).thenReturn(new ArrayList<>());
        Mockito.when(eventDAO.getAllFutureEvents(0, 2, new ArrayList<>(), null, false)).thenReturn(3);
        List<EventDTO> result = EventService.getAll();
        Assertions.assertEquals(List.of(new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>())), result);
        Assertions.assertEquals(3, EventService.getAll(0, 2, new ArrayList<>(), false));
    }
    @Test
    void testUpdate() throws IncorrectDataException {
        Event event1 = new Event(1, "kkjjj", Instant.EPOCH, "test");
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(event1));
        EventService.update(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test");
        Mockito.verify(eventDAO).update(event1, new String[]{"kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test"});
    }
    @Test
    void testDelete() throws IncorrectDataException {
        Event event1 = new Event(1, "kkjjj", Instant.EPOCH, "test");
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(event1));
        EventService.delete(1);
        Mockito.verify(eventDAO).delete(event1);
    }
    @Test
    void testGetEventsByReport() throws IncorrectDataException {
        Report report = new Report(1, new Topic(2, "random"), new AbstractUser("lollll", "notRandomName", "lolololo2323", UserType.USER, "dsgfdg"));
        Mockito.when(eventDAO.getEventByReport(report)).thenReturn(Optional.of(new Event(1, "kkjjj", Instant.EPOCH, "test")));
        Mockito.when(reportDAO.get(1)).thenReturn(Optional.of(report));
        EventDTO eventDTO = EventService.getEventByReport(new ReportDTO(report.getId(), 2, "random", "notRandomName", "lollll", 0, "test"));
        Assertions.assertEquals(new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()), eventDTO);
    }
    @Test
    void testGetEventByTopic(){
        Topic topic = new Topic(1, "kkjjj");
        Mockito.when(eventDAO.getEventByTopic(topic)).thenReturn(Optional.of(new Event(1, "kkjjj", Instant.EPOCH, "test")));
        Mockito.when(topicDAO.get(1)).thenReturn(Optional.of(topic));
        EventDTO eventDTO = EventService.getEventByTopic(new TopicDTO(1, "kkjjj", 0, "test"));
        Assertions.assertEquals(new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()), eventDTO);
    }
    @Test
    void testSetEventToTopic(){
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(new Event(1, "kkjjj", Instant.EPOCH, "test")));
        Mockito.when(topicDAO.get(1)).thenReturn(Optional.of(new Topic(1, "kkjjj")));
        EventService.setEventToTopic(new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()),
                new TopicDTO(1, "kkjjj", 0, "test"));
        Mockito.verify(eventDAO).setEventToTopic(new Event(1, "kkjjj", Instant.EPOCH, "test"), new Topic(1, "kkjjj"));
    }
    @Test
    void testGetUsersByEvent(){
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(new Event(1, "kkjjj", Instant.EPOCH, "test")));
        Mockito.when(eventDAO.getUsersByEvent(new Event(1, "kkjjj", Instant.EPOCH, "test"))).thenReturn(new ArrayList<>());
        Assertions.assertEquals(new ArrayList<>(), EventService.getUsersByEvent(new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>())));
    }
    @Test
    void testGetUsersAttendedEvent(){
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(new Event(1, "kkjjj", Instant.EPOCH, "test")));
        Mockito.when(eventDAO.getUsersAttendedEvent(new Event(1, "kkjjj", Instant.EPOCH, "test"))).thenReturn(new ArrayList<>());
        Assertions.assertEquals(new ArrayList<>(), EventService.getUsersAttendedEvent(new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>())));
    }
    @Test
    void testRegisterAttendance() throws IncorrectDataException {
        Mockito.when(eventDAO.get(1)).thenReturn(Optional.of(new Event(1, "kkjjj", Instant.EPOCH, "test")));
        Mockito.when(userDAO.get("speaker1")).thenReturn(Optional.of(new AbstractUser("some speaker", "speaker1", "speaker322", UserType.SPEAKER, "FDSAFASD")));
        EventService.registerAttendance(new AbstractUserDTO("some speaker", "speaker1", UserType.SPEAKER),
                new EventDTO(1, "kkjjj", Utils.convertInstantToString(Instant.EPOCH), "test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        Mockito.verify(eventDAO).registerAttendance(new Event(1, "kkjjj", Instant.EPOCH, "test"), new AbstractUser("some speaker", "speaker1", "speaker322", UserType.SPEAKER, "FDSAFASD"));
    }
}
