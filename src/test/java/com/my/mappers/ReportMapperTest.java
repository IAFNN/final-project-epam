package com.my.mappers;

import com.my.DAO.EventDAO;
import com.my.DAO.ReportDAO;
import com.my.DAO.impl.EventDAOImpl;
import com.my.DAO.impl.ReportDAOImpl;
import com.my.DTO.ReportDTO;
import com.my.entity.AbstractUser;
import com.my.entity.Report;
import com.my.entity.Topic;
import com.my.entity.UserType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

public class ReportMapperTest {
    private static EventDAO eventDAO;
    private static ReportDAO reportDAO;
    @BeforeAll
    static void init(){
        eventDAO = Mockito.mock(EventDAOImpl.class);
        reportDAO = Mockito.mock(ReportDAOImpl.class);
        ReportMapper.init(eventDAO, reportDAO);
    }
    @Test
    void toEntityTest(){
        Mockito.when(reportDAO.get(50)).thenReturn(Optional.of(new Report(50, new Topic(0, "lol"), new AbstractUser("lololol", "lololo423", "lololool22", UserType.USER, "aDADSSA"))));
        ReportDTO reportDTO = new ReportDTO(50, 0, "lol", "lololo423", "lololol", 0, "null");
        Assertions.assertEquals(new Report(50, new Topic(0, "lol"), new AbstractUser("lololol", "lololo423", "lololool22", UserType.USER, "aDADSSA")), ReportMapper.toEntity(reportDTO));
    }
}
