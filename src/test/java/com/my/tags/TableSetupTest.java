package com.my.tags;

import com.my.tags.event.table.*;
import com.my.tags.report.table.*;
import com.my.tags.topic.table.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TableSetupTest {
    @Test
    void EventTableSetupTest(){
        EventTable table = new ButtonsEventTableDecorator(new NumberOfReportsEventTableDecorator(new NumberOfUsersAttendedEventTableDecorator(
                new NumberOfUsersRegisteredEventTableDecorator(new LocationEventTableDecorator(new TimeEventTableDecorator(new NameEventTableDecorator(
                        new IDEventTableDecorator(new EventTableImpl())
                ))))
        )));
        Assertions.assertEquals(List.of(EventTableColumns.ID, EventTableColumns.NAME, EventTableColumns.TIME, EventTableColumns.LOCATION,
                EventTableColumns.NUMBER_OF_REGISTERED_USERS, EventTableColumns.NUMBER_OF_USERS_ATTENDED, EventTableColumns.NUMBER_OF_REPORTS, EventTableColumns.BUTTONS),
                table.setupTable());
    }
    @Test
    void ReportTableSetupTest(){
        ReportTable table = new ButtonsReportTableDecorator(new EventNameReportTableDecorator(new EventIDReportTableDecorator(
                new SpeakerNameReportTableDecorator(new SpeakerLoginReportTableDecorator(new TopicNameReportTableDecorator(new TopicIDReportTableDecorator(
                        new IDReportTableDecorator(new ReportTableImpl())
                ))))
        )));
        Assertions.assertEquals(List.of(ReportTableColumns.ID, ReportTableColumns.TOPIC_ID, ReportTableColumns.TOPIC_NAME, ReportTableColumns.SPEAKER_LOGIN,
                        ReportTableColumns.SPEAKER_NAME, ReportTableColumns.EVENT_ID, ReportTableColumns.EVENT_NAME, ReportTableColumns.BUTTONS),
                table.setupTable());
    }
    @Test
    void TopicTableSetupTest(){
        TopicTable table = new ButtonsTopicTableDecorator(new EventNameTopicTableDecorator(new EventIDTopicTableDecorator(new NameTopicTableDecorator(
                        new IDTopicTableDecorator(new TopicTableImpl())))));
        Assertions.assertEquals(List.of(TopicTableColumns.ID, TopicTableColumns.NAME, TopicTableColumns.EVENT_ID, TopicTableColumns.EVENT_NAME,
                        TopicTableColumns.BUTTONS),
                table.setupTable());
    }
}
