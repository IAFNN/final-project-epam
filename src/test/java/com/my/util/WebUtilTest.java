package com.my.util;

import com.my.DTO.AbstractUserDTO;
import com.my.DTO.EventDTO;
import com.my.DTO.ReportDTO;
import com.my.entity.UserType;
import com.my.tags.event.table.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class WebUtilTest {
    @BeforeAll
    static void init(){
        WebUtils.init("E:/IdeaProjects/FinalProject/target/FinalProject-1.0-SNAPSHOT/");
    }
    @Test
    void constructEventTable(){
        String time = Instant.now().toString();
        List<EventDTO> eventDTOList = List.of(new EventDTO(1, "lolname", Instant.EPOCH.toString(), "there",
                List.of(new AbstractUserDTO("kekw", "lols", UserType.MODERATOR), new AbstractUserDTO("lulw", "lolz", UserType.SPEAKER)), List.of(new AbstractUserDTO("lulw", "lolz", UserType.SPEAKER)),
                List.of(new ReportDTO(1, 21, "topic", "speaker22", "sepae", 12, "efefefe"))),
                new EventDTO(2, "lolnsasf", time, "here", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<tr class='headerRow'>\n<th>Name</th>\n<th>Time</th>\n<th>Location</th>\n</tr>\n");
        stringBuilder.append(String.format("<tr class=\"contentRow\">\n<td>lolname</td>\n<td>%s</td>\n<td>there</td>\n</tr>\n", Instant.EPOCH));
        stringBuilder.append(String.format("<tr class=\"contentRow\">\n<td>lolnsasf</td>\n<td>%s</td>\n<td>here</td>\n</tr>\n", time));
        EventTable eventTable = new EventTableImpl();
        eventTable = new LocationEventTableDecorator(new TimeEventTableDecorator(new NameEventTableDecorator(eventTable)));
        Assertions.assertEquals(stringBuilder.toString(), WebUtils.constructEventTable("", eventDTOList, null, null, eventTable).toString());
    }
    @Test
    void constructEventTableWithID(){
        String time = Instant.now().toString();
        List<EventDTO> eventDTOList = List.of(new EventDTO(1, "lolname", Instant.EPOCH.toString(), "there",
                        List.of(new AbstractUserDTO("kekw", "lols", UserType.MODERATOR), new AbstractUserDTO("lulw", "lolz", UserType.SPEAKER)), List.of(new AbstractUserDTO("lulw", "lolz", UserType.SPEAKER)),
                        List.of(new ReportDTO(1, 21, "topic", "speaker22", "sepae", 12, "efefefe"))),
                new EventDTO(2, "lolnsasf", time, "here", new ArrayList<>(), new ArrayList<>(), new ArrayList<>()));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<tr class='headerRow'>\n<th>ID</th>\n<th>Name</th>\n<th>Time</th>\n<th>Location</th>\n<th>Number of registered users</th>\n<th>Number of reports</th>\n</tr>\n");
        stringBuilder.append(String.format("<tr class=\"contentRow\">\n<td>1</td>\n<td>lolname</td>\n<td>%s</td>\n<td>there</td>\n<td>2</td>\n<td>1</td>\n</tr>\n", Instant.EPOCH));
        stringBuilder.append(String.format("<tr class=\"contentRow\">\n<td>2</td>\n<td>lolnsasf</td>\n<td>%s</td>\n<td>here</td>\n<td>0</td>\n<td>0</td>\n</tr>\n", time));
        EventTable eventTable = new EventTableImpl();
        eventTable = new NumberOfReportsEventTableDecorator(new NumberOfUsersRegisteredEventTableDecorator(new LocationEventTableDecorator(new TimeEventTableDecorator(
                new NameEventTableDecorator(new IDEventTableDecorator(eventTable))
        ))));
        Assertions.assertEquals(stringBuilder.toString(), WebUtils.constructEventTable("", eventDTOList, null, null, eventTable).toString());
    }
}
