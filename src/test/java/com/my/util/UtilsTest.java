package com.my.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.Instant;

public class UtilsTest {
    @Test
    void convertInstantToStringTest(){
        Assertions.assertEquals("00:00\t01.01.1970", Utils.convertInstantToString(Instant.EPOCH));
    }
    @Test
    void hashPasswordTest() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String salt = "���$�@�v��=f\u000F�؈";
        Assertions.assertEquals("��g�ꏐ��H�\u001F�G", Utils.hashPassword("test123", salt));
    }
    @Test
    void buildMessageTextTest(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Something has changed in event(Event ID: 2) you joined\n");
        stringBuilder.append("New Event name is TestEvent\n");
        stringBuilder.append("New Event date is 00:00\t01.01.1970\n");
        stringBuilder.append("New Event location is TestLocation\n");
        Assertions.assertEquals(stringBuilder.toString(), Utils.buildMessageText("2", "TestEvent", "00:00\t01.01.1970", "TestLocation"));
    }
}
